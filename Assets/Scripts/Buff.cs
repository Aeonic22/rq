﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffType { PerTile, PerUnit }

public enum BuffAnimType {
    /// <summary>
    /// unit buff, for animation that loops thru transparency
    /// </summary>
    UnitApplyMaintain,
    /// <summary>
    /// unit buff, for separate components of animation
    /// </summary>
    UnitCreateMaintainApplyEnd,
    /// <summary>
    /// for terrain buffs
    /// </summary>
    TerrainCreateMaintainApply }

public enum BuffTrigger
{
    /// <summary>
    /// Overnight
    /// </summary>
    BeginGlobalTurn,
    /// <summary>
    /// Before night
    /// </summary>
    EndTurn,
    Moved,
    EnterBattle,
    BattleBuff,
    Instant
}

/// <summary>
/// Spell creates buff over tile or over unit, 1 tile big.
/// Buff is applied on end of turn, before spell phase. 
/// That is so that new spell can be cast if one has expired.
/// </summary>
[System.Serializable]
public class Buff
{
    public IBuffMB mb;

    public SpellDefinition spell = null;
    public Unit unit;
    public TerrainTile tile;

    public LevelState ls;

    /// <summary>
    /// How long effect lasts.
    /// </summary>
    public int duration;
    /// <summary>
    /// How long has it been in effect
    /// </summary>
    public int age;

    public void Init(BuffMB mb, SpellDefinition spell, int duration, Unit unit, TerrainTile tile, LevelState ls)
    {
        this.mb = mb;
        this.spell = spell;
        //this.strength = strength;//TODO remove strength
        this.unit = unit;
        this.tile = tile;
        this.duration = duration;
        this.ls = ls;

        

        ls.buffs.Add(this);

        if (unit)
        {
            if (spell.buffTrigger == BuffTrigger.BeginGlobalTurn)
                unit.UnitBeginGlobalTurn += InitApply;
        }

        //OneTurn();
    }

    public void End()
    {
        ls.buffs.Remove(this);

        if (unit)
        {
            if (spell.buffTrigger == BuffTrigger.BeginGlobalTurn)
                unit.UnitBeginGlobalTurn -= InitApply;
        }
    }

    /// <summary>
    /// Initiates display of effect and processes its results.
    /// In game, processing is not instantaneous but has callback initiated by animation.
    /// In AI thinking, processing is instantaneous.
    /// </summary>
    public void InitApply()
    {
        /*if (spell.type == BuffType.PerUnit && unit)
        {
            AffectUnit();
        }*/
        /*else if (spell.type == BuffType.PerTile)
        {
            AffectTile();
        }*/
        if (age < duration-1)
            mb.Apply(); // and back to maintain anim
        else if (age == duration-1) // for spells that have a separate dissipate animation
        {
            mb.Apply();
            mb.End();
        }
        age++;
        if (age > duration)
        {
            MyHelpers.LogError(this, "Age greater than duration.");
        }
    }

    void ApplyToUnit()
    {
        if (spell.moraleBoost != 0) unit.morale += spell.moraleBoost;
        MyHelpers.LogColor(this, "red", spell.spellName + " over " + unit + ". Unit morale now " + unit.morale);
    }

    // no tile effects, just on units on tiles
    //void AffectTile()
    //{
    //    MyHelpers.LogColor(this, "red", spell.spellName + " over " + tile);
    //}

    /// <summary>
    /// Called at the moment the effect animation kicks in
    /// </summary>
    public void cbApplied()
    {
        if (spell.buffType == BuffType.PerUnit && unit)
        {
            ApplyToUnit();
        }
        
        /*if (spell.type == BuffType.PerTile && tile)
        {
            AffectTile();
        }*/
    }

    /// <summary>
    /// How long, what power (dissipating by x or by 50% every turn), what effect
    /// </summary>
    public string GetTooltip()
    {
        return "";
    }

}

