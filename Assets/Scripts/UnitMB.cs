﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fizzik;

/// <summary>
/// Contains one Unit and does all Unity integration (input, UI updates, animations, sprites, sounds)
/// Unit connected to tile at runtime.
/// </summary>
public class UnitMB : MonoBehaviour, IUnitMB
{
    public static UnitMBDummy unitMBDummy;

    [Header("Core object")]
    public Unit me;

    [Header("Display, temp, misc states")]
    public bool moving = false;
    public bool dying = false;
    public float moveStartTime;
    public bool needsUpdate = true;

    [Header("Connections")]
    [Tooltip("Must connect to Player instance on scene")]
    public PlayerMB playerMB; // set in editor
    private SpriteRenderer sr;
    private static GameMain gm;
    private LevelState ls;
    public GameObject unitGui;

    static UnitMB () {
        unitMBDummy = new UnitMBDummy();
    }

    /// <summary>
    /// Units are created at runtime after game and level are set up so Awake is ok to use for init.
    /// </summary>
    private void Awake()
    {
        // name set from external creation method
    }

    // Use this for initialization
    void Start () {
        sr = GetComponent<SpriteRenderer>();
        gm = FindObjectOfType<GameMain>();
        ls = gm.ls;
        MyHelpers.Log(this, " unit needs tiles");
        TerrainTile tile = ls.tiles.GetTileAtCoords(transform.position);
        if (me.mb != null) return; // already inited by CreateUnit
        me.Init(this, name, gm, ls, playerMB.me, tile);
    }

    public void FadeOutOnDeath()
    {
        StartCoroutine(FadeOutOnDeathCR());
    }

    /// <summary>
    /// Does not affect the game state, just blocks the ui until fade out is completed
    /// </summary>
    /// <returns></returns>
    private IEnumerator FadeOutOnDeathCR()
    {
        gm.gui.BlockUI(true);
        float startTime = Time.time;
        dying = true;

        while (dying)
        {
            float p = Mathf.Min(1f, (Time.time - startTime) / Defines.def.miscGameSettings.fadeOutTime);
            SetTransparency(p);
            if (p == 1f)
            {
                dying = false;
                break;
            }
            yield return null;
        }

        gm.gui.BlockUI(false);
        DestroyMe();

        yield return null;
    }

    /// <summary>
    /// Must have lastTile and targetTile set before
    /// </summary>
    public void MoveToTileAnim(TerrainTile lastTile, TerrainTile targetTile)
    {
        StartCoroutine(MoveToTileAnimCR(lastTile, targetTile));
    }

    private IEnumerator MoveToTileAnimCR(TerrainTile lastTile, TerrainTile targetTile)
    {
        moveStartTime = Time.time;
        moving = true;
        Vector2 startPos = new Vector2(lastTile.x, lastTile.y);
        Vector2 targetPos = new Vector2(targetTile.x, targetTile.y);

        float moveTime = Mathf.Lerp(Defines.def.miscGameSettings.oneTileMoveTime,
            Defines.def.miscGameSettings.oneTileMoveTimeTired, (100-me.stamina)* 0.01f);

        while (moving)
        {
            float p = Mathf.Min(1f, (Time.time - moveStartTime) / moveTime);
            Vector2 pos = Vector2.Lerp(startPos, targetPos, p);
            transform.position = gm.GetWorldCoords(pos, transform.position.z);
            needsUpdate = true;

            unitGui.transform.position = Camera.main.WorldToScreenPoint(transform.position);

            if (p >= 1f) break;
            yield return null;
        }

        moving = false;
        yield return null;
    }

    void SetTransparency(float t)
    {
        float a = 1 - t;
        if (a < Mathf.Epsilon) a = 0;
        Color c = sr.color;
        sr.color = new Color(c.r, c.g, c.b, a);
    }

    public bool IsPending() { return dying || moving; }

    public void NeedsUpdate() { needsUpdate = true; }

    public void DestroyMe()
    {
        Destroy(gameObject);
        Destroy(unitGui);
    }

    /// <summary>
    /// Animate movement, get keyboard controls, move to world coord, update sprites.
    /// </summary>
    void Update()
    {
        if (!needsUpdate) return;
        needsUpdate = false;
        if (!moving) // moving sets its own position
        {
            transform.position = gm.GetWorldCoords(me.tile.x, me.tile.y, transform.position.z);
        }

        // update sprite
        if (me.killed)
        {
            sr.enabled = false;
            return;
        }
    }

    /// <summary>
    /// Transfer click to tile. I still keep collider on Units for OnMouseEnter events.
    /// </summary>
    private void OnMouseUp()
    {
        gm.ls.TileActionUI(me.tile);
    }
    public void SetSprite(Sprite s)
    {
        sr.sprite = s;
    }
    /*private void OnMouseEnter()
    {
        gm.gui.SetUnitInfoText(me.GetInfoText());
    }
    private void OnMouseExit()
    {
        gm.gui.SetUnitInfoText(null);
    }*/
    private void OnMouseUpAsButton()
    {
        MyHelpers.Log(this, "MouseUp");
    }

    public void SetUnitGui()
    {
        HealthBarComponent sb = unitGui.transform.Find("StaminaBar").GetComponent<HealthBarComponent>();
        sb.totalHealth = 100;
        sb.currentHealth = me.stamina;

        Text textParams = unitGui.transform.Find("Params").GetComponent<Text>();
        textParams.text = me.GetStrengthActual() + "(<color=#FF5555>" + me.count + "</color>)<color=yellow>"
            + Mathf.Floor(me.food / me.count) + "</color>";

        if (gm.ls.turnOrderCurrent != null)
        {
            Text order = unitGui.transform.Find("Order").GetComponent<Text>();
            order.text = (gm.ls.turnOrderCurrent.FindIndex(x => x == me) + 1).ToString();
        }
    }


    #region hiddenInformation
    /*
    private void HideOutOfSightUnit()
    {
        if (me.player.playerSide == "EnemySide")
        {
            UnitEngagement ue = me.GetEnemyMaxEngagementToMe();

            if (ue <= UnitEngagement.Invisible)
            {
                //sr.enabled = false;
                //debug
                if (gm.fogOfWar)
                {
                    sr.enabled = false;
                }
                else
                {
                    sr.enabled = true;
                    sr.color = Defines.def.notKnownColor;
                }
            }
            else
            {
                sr.enabled = true;
                //if (visible == eUnitVisibility.known)
                //{
                //    sr.color = knownNotSeenColor;
                //}
                //else if (visible == eUnitVisibility.seen)
                //{
                //    sr.color = new Color(1, 1, 1, 1);
                //}
                sr.color = new Color(1, 1, 1, 1);

            }
        }
    }
    */
    #endregion

}



public interface IUnitMB
{
    //void SetSelectionMarker();
    //void DisableSelectionMarker();

    void FadeOutOnDeath();
    void NeedsUpdate();
    void MoveToTileAnim(TerrainTile lastTile, TerrainTile targetTile);
    void SetSprite(Sprite s);
    bool IsPending();
    void SetUnitGui();
    void DestroyMe();
}

public class UnitMBDummy : IUnitMB
{
    //void SetSelectionMarker() { }
    //void DisableSelectionMarker() { }

    public void FadeOutOnDeath() { }
    public void NeedsUpdate() { }
    public void MoveToTileAnim(TerrainTile lastTile, TerrainTile targetTile) { }
    public void SetSprite(Sprite s) { }
    public bool IsPending() { return false; }
    public void SetUnitGui() { }
    public void DestroyMe() { }
}
