﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public delegate void OnUnitBeginGlobalTurnHandler();

/// <summary>
/// Core game class that holds entire state of one army unit.
/// </summary>
/// <remarks>
/// Knows player it belongs to, has name, strength, tile it's on. Has visual representation. 
/// Can animate movement to next tile //TODO to dedicated class
/// Knows units has seen and units that have seen it.
/// Has own visibility map and memory of sightings. Potentially has own terrain reveal map.
/// Has simple AI that can decide on next move. Will be state machine.
/// </remarks>
[System.Serializable]
public class Unit {

    /// <summary>
    /// Dependency injection link to MonoBheavior
    /// </summary>
    public IUnitMB mb;

    /// <summary>
    /// To which player it belongs
    /// </summary>
    [System.NonSerialized] public Player player;       // to which player it belongs
    [HideInInspector] public string name; // hidden to avoid confusion, name same as object name
    //TODO compostion, general, items

    /// <summary>
    /// Tile the unit is on. Stays same until move finished.
    /// </summary>
    [System.NonSerialized] private TerrainTile _tile;
    public TerrainTile tile { get { return _tile; } set { _tile = value; }  }
    /// <summary>
    /// Number of men
    /// </summary>
    public int count = 100;
    /// <summary>
    /// in precentage, 100% is normal
    /// </summary>
    [Range(0,100)]
    public int morale = 100;
    /// <summary>
    /// Number of men weighed with tiredness, general, items, morale...
    /// </summary>
    // public int strength; - calculated
    /// <summary>
    /// 0-100 and above
    /// </summary>
    //[Range(0,500)]
    //public int tiredness = 0;
    [Range(0, 100)]
    public int stamina = 100;
    public void AddTiredness(int tiredness, bool ovrride=false)
    {
        if (tiredness % 20 != 0) Debug.LogError("Tiredness not in increment of 20");
        stamina = stamina - tiredness;
        stamina = Mathf.Max(stamina, 1);
        if (!ovrride) stamina = Mathf.Min(stamina, 100);
        if (stamina <= 1) skipNextTurn = true;
    }
    /// <summary>
    /// 0-100 and above. Changes immediately after move.
    /// </summary>
    public int tirednessAddedLastMove = 0;
    /// <summary>
    /// 0-100 and above. Used to display tiredness in turn order.
    /// </summary>
    public int tirednessAddedLastTurn = 0;
    //public bool TooTiredToMove() { return tiredness >= 100; }
    public bool TooTiredToMove() { return stamina <= 1; }
    /// <summary>
    /// tiredness from last move, with possible modifiers like generals, items, food
    /// </summary>
    public int initiative { get; set; } 
    /// <summary>
    /// How many consecutive days on march
    /// </summary>
    public int marchDays = 0;
    /// <summary>
    /// To detect if it can skip turn
    /// </summary>
    public bool moved = false;
    /// <summary>
    /// If chosen to skip, pinned or frozen, or if tired > 100
    /// </summary>
    public bool skipsTurn = false;
    public bool skippedLastTurn = false;
    public bool skipNextTurn = false;
    /// <summary>
    /// Food
    /// </summary>
    public int food = 2000;
    /// <summary>
    /// Mana reserves for haarp
    /// </summary>
    public int mana;
    public int manaGainLast;

    // display, temp, misc states
    //[System.NonSerialized] private TerrainTile _targetTile;
    //public TerrainTile targetTile { get { return _targetTile; } set { _targetTile = value; } }
    [System.NonSerialized] private TerrainTile _lastTile; //TODO let tile reference history of passes and let unit keep its history of moves. for palantir 
    public TerrainTile lastTile { get { return _lastTile; } set { _lastTile = value; } }

    [Header("Spells")]
    public List<SpellDefinition> spells;

    [Header("Misc states")]
    //public int currentTurn; //TODO remove this var, just leave bool finishedTurn
    public bool finishedTurn;
    public bool attacking = false;
    [HideInInspector] public bool killed = false;

    // cached
    private static GameMain gm;
    [System.NonSerialized]
    public LevelState ls;

    // events
    public event OnUnitBeginGlobalTurnHandler UnitBeginGlobalTurn;

    /// <summary>
    /// To be run after external setup. Not called Start or Awake since not MB.
    /// So units set up on level scenes are inted like this.
    /// </summary>
    public void Init (IUnitMB mb, string name, GameMain gm, LevelState ls, Player player, TerrainTile tile) {
        MyHelpers.Log(this, "Init unit " + name + " with tile " + tile.ToString());
        this.mb = mb;
        this.name = name;
        Unit.gm = gm;
        this.ls = ls;
        this.player = player;
        ls.units.Add(this);
        this.tile = tile;
        tile.AddUnit(this);
        marchDays = 0;

        UnitBeginGlobalTurn = null;
    }

    public Unit() { }

    /// <summary>
    /// For AI and misc unse only.
    /// Must call Init after this to finalize initialization.
    /// </summary>
    /// <param name="source"></param>
    public Unit (Unit source)
    {
        count = source.count;
        morale = source.morale;
        //tiredness = source.tiredness;
        stamina = source.stamina;
        tirednessAddedLastMove = source.tirednessAddedLastMove;
        tirednessAddedLastTurn = source.tirednessAddedLastTurn;
        marchDays = source.marchDays;
        moved = source.moved;
        skipsTurn = source.skipsTurn;
        skippedLastTurn = source.skippedLastTurn;
        skipNextTurn = source.skipNextTurn;
        lastTile = source.lastTile;
        finishedTurn = source.finishedTurn;
        attacking = source.attacking;
        killed = source.killed;
    }

    /// <summary>
    /// Set this unit as selected.
    /// Mark unit as entered into current turn if not yet marked.
    /// Or try to attack if tile is occupied
    /// </summary>
    /*public void SelectOrAttackMe()
    {
        MyHelpers.Log(this, "SelectOrAttackMe");
        // either I select one of my units in free selection mode
        if (ls.GetCurrentPlayer() == player)
        {
            // not used, per unit turns
            MyHelpers.Log(this, "Selected unit " + name);
            //gm.SetSelectedUnit(this);
            player.lastSelectedUnit = this;
            //mb.SetSelectionMarker();
            gm.gui.SetSelectionMarker(mb as UnitMB);
        }
        // or local player tries to attack selected enemy - just short circuit valid move test which usually wont allow movement to occupied tile
        else if (ls.currentUnit.player.playerType == PlayerType.local && ls.currentUnit.MoveIsValid(tile))
        {
            ls.currentUnit.attacking = true;
            ls.currentUnit.unitWeAttack = this;
            ls.MoveCurrentUnit(tile);
        }
    }*/
    public void Select()
    {
        // not used since we have per unit turns
    }
    /// <summary>
    /// Called once per turn
    /// </summary>
    public void BeginGlobalTurn()
    {
        //gm.selectedUnit = this;//TODO uncomment
        moved = false;
        finishedTurn = false;

        // tiredness
        int rest = Defines.def.gameplaySettings.restPerTurn;
        if (skippedLastTurn)
        {
            rest = Defines.def.gameplaySettings.restPerSkippedTurn;
            marchDays = 0;
        }
        if (tile.settlement)
        {
            if (tile.settlement.alignment == ls.GetNeutralPlayer())
                rest = (int)(Defines.def.gameplaySettings.restMultiplyerNeutralVillage * rest);
            else if (tile.settlement.alignment == player)
                rest = (int)(Defines.def.gameplaySettings.restMultiplyerFriendlyVillage * rest);
            else rest = (int)(Defines.def.gameplaySettings.restMultiplyerEnemyVillage * rest);
        }

        // supply
        Settlement supply = GetSupply();
        float gotten;
        if (supply) GetFood(supply, 1, out gotten);

        // food
        if (food < count)
        {
            // take food from village
            if (tile.settlement)
            {
                GoOutForLunch();
            }

            if (food < count)
            {
                rest *= (count - food) / count;
                MyHelpers.Log(this, ToString() + " not enough food, resting for just " + rest);
                food = 0;
            }
        }
        // either we have on begin turn or we went to lunch outside
        if (food >= count)
        {
            //MyHelpers.LogColor(this, "red", name + "food " + food + " count " + count);
            food -= count;
        }

        // again tiredness
        /*if (Defines.def.gameplaySettings.restAsMultiply)
        {
            tiredness = (int)(tiredness * (100 - rest) * 0.01f);
            if (tiredness < 5) tiredness = 0;
        }
        else tiredness = tiredness - rest;
        tiredness = Mathf.Max(0, tiredness);
        tirednessAddedLastTurn = tirednessAddedLastMove;*/

        //stamina = stamina + rest;
        //stamina = Mathf.Max(1, stamina);
        //stamina = Mathf.Min(100, stamina);
        AddTiredness(-rest);
        tirednessAddedLastTurn = tirednessAddedLastMove;

        // buffs
        if (UnitBeginGlobalTurn != null) UnitBeginGlobalTurn();

        // mana
        manaGainLast = tile.GetMaxManaGain();
        mana += manaGainLast;
    }
    public void BeginMyTurn()
    {
        ls.gui.SetSettlementControls(this, tile.settlement);
    }
    public void EndMyTurn()
    {
        ls.gui.SetSettlementControls(null, null);
        ls.gui.SetSelectedUnitInfoText(true);
    }
    /// <summary>
    /// Can we mnove or attack, is it neighbouring tile.
    /// </summary>
    public bool MoveIsValid(TerrainTile newTile, bool suppressWarning = false)
    {
        string ret = "";
        if (newTile == null)
        {
            ret = "null tile";
            goto Invalid;
        }
        if (tile == newTile)
        {
            ret = "same tile";
            goto Invalid;
        }
        if (tile && !ls.tiles.IsNeighbour(tile, newTile)) {
            ret = "not neighbour";
            goto Invalid;  // skip if initial placement on map
        }
        // can only go to occupied tile if attacking
        /*if (newTile.GetUnits() != null && newTile.GetUnits().Count > 0)
        {
            if (newTile.GetUnits().Find(x => x.player != player && x.killed != true)) return true; // we'll attack
            if (newTile.GetUnits().Count(x => x.killed != true) == 0) return true; // only dead units there
            ret = "friendly units on tile";
            goto Invalid;
        }*/
        if (newTile.GetUnits() != null && newTile.GetUnits().Find(x => x.player == player && x.killed != true))
        {
            ret = "friendly units on tile";
            goto Invalid;
        }
        return true;
        Invalid:
        if (!suppressWarning) MyHelpers.LogError(this, "MoveIsValid error (" + ret +"), unit: " + name + ", " 
            + tile + " > " + newTile);

        return false;
    }

    /// <summary>
    /// Performs all checks beforehand. Initiates move animation that runs in Update and returns.
    /// Old tile is immediately relieved of unit.
    /// //TODO notify enemy units that we move if they want to pursue.
    /// </summary>
    public void MoveStart(TerrainTile newTile)
    {
        MyHelpers.Log(this, "move start " + name + " to tile " + newTile.ToString());
        if (!MoveIsValid(newTile)) return;

        tile.RemoveUnit(this);
        lastTile = tile;
        tile = newTile;
        tile.AddUnit(this);
        marchDays++;
        
        mb.MoveToTileAnim(lastTile, newTile);

        // move consequences
        tirednessAddedLastMove = GetTirednessForOneTileMove(lastTile, tile);
        //tiredness += tirednessAddedLastMove;
        //if (tiredness > 100) skipNextTurn = true;

        //stamina = Mathf.Min(stamina, 100 - tirednessAddedLastMove); // just up tiredness to what was added this turn
        //stamina = Mathf.Max(1, stamina);
        //if (stamina <= 1) skipNextTurn = true;
        AddTiredness(tirednessAddedLastMove);

        ls.gui.DisableSelectionMarker();
        ls.gui.SetUnitInfoText(GetInfoText()); //TODO
        mb.SetUnitGui();
        moved = true;
        if (Defines.def.miscGameSettings.finishTurnOnMove) finishedTurn = true;
    }

    private int GetTirednessForOneTileMove(TerrainTile fromTile, TerrainTile toTile)
    {
        float dist = Mathf.Sqrt(Mathf.Abs(fromTile.x - toTile.x) + Mathf.Abs(fromTile.y - toTile.y));
        //if (dist > 1) dist = Defines.def.gameplaySettings.diagonalTirednessModifer;
        int ret = (int)(Defines.TirednessPerTerrain(toTile.type));
        if (dist > 1) ret += 20;
        return ret;
    }

    public void EndTurn()
    {
        skippedLastTurn = skipsTurn;
        skipsTurn = skipNextTurn;
        skipNextTurn = false;
    }
    public bool TurnFinishedOrSkipped()
    {
        if (skipsTurn)
        {
            MyHelpers.Log(this, "TURN SKIPPED for unit " + name);
            return true;
        }
        //if (gm.finishTurnOnMove || moved)
        if (Defines.def.miscGameSettings.finishTurnOnMove || moved)
        {
            MyHelpers.Log(this, "TURN FINISHED, ALREADY MOVED, for unit " + name);
            return true;
        }
        if (finishedTurn)
        {
            MyHelpers.Log(this, "TURN FINISHED for unit " + name);
            return true;
        }
        return false;
    }
    public static bool sortByTotalTiredness;
    public static int CompareInitiativeDesc(Unit u1, Unit u2)
    {
        if (!u1.killed && u2.killed) return 1;
        if (u1.killed && !u2.killed) return -1;

        if (!sortByTotalTiredness)  // sort by last turn added tiredness first
        {
            if (u1.tirednessAddedLastMove < u2.tirednessAddedLastMove) return 1;
            if (u1.tirednessAddedLastMove > u2.tirednessAddedLastMove) return -1;
        }
        //if (u1.tiredness < u2.tiredness) return 1 ;
        //if (u1.tiredness > u2.tiredness) return -1;
        if (u1.stamina > u2.stamina) return 1;
        if (u1.stamina < u2.stamina) return -1;
        return 1; //TODO last one to move is last one this turn also, or one with more kills, or one with less men, or other initiative modifiers
    }
    public static int CompareInitiativeAsc(Unit u1, Unit u2)
    {
        return CompareInitiativeDesc(u2, u1);
    }

    /// <summary>
    /// All parameters, including passive and active abilities, fortified etc.
    /// </summary>
    /// <returns></returns>
    public float GetStrengthActual()
    {
        //float ret = count * (Mathf.Max(0f, (100 - tiredness) * 0.01f)) * morale*0.01f;
        float ret = count * stamina * morale / 10000;
        ret = Mathf.Max(ret, 1);
        return Mathf.Floor(ret);
    }
    public float GetStrengthActualDefense()
    {
        float ret = GetStrengthActual();
        if (tile.GetTileType() == TerrainType.Forest || tile.GetTileType() == TerrainType.Mountain)
            ret *= 1.25f;
        return Mathf.Floor(ret);
    }
    /// <summary>
    /// Can be called for any tiles, distance is ignored.
    /// </summary>
    public float GetStrengthActualAfterMove(TerrainTile dTile)
    {
        //if (!ls.tiles.IsNeighbour(tile, dTile)) MyHelpers.LogError(this, "GetStrengthActualAfterMove error");
        //int tirednessTemp = tiredness;
        //tiredness += GetTirednessForOneTileMove(tile, dTile);
        //float ret = GetStrengthActual();
        //tiredness = tirednessTemp;
        int staminaTemp = stamina;
        stamina -= GetTirednessForOneTileMove(tile, dTile);
        float ret = GetStrengthActual();
        stamina = staminaTemp;
        return ret;
    }

    public bool GetFood(float wantedDays)
    {
        float got;
        return GetFood(tile.settlement, wantedDays, out got);
    }
    /// <summary>
    /// Takes food for x days from settlement
    /// </summary>
    /// <param name="wantedDays">How many days of food we need. Multiply by count to get food.</param>
    /// <param name="foodDays">How many food days we managed to get.</param>
    /// <returns>Whether we got as much as we required.</returns>
    public bool GetFood(Settlement s, float wantedDays, out float foodDays)
    {
        if (!s) {
            foodDays = 0;
            return false; 
        }
        // taking all food
        /*if (wantedDays * count > tile.settlement.food)
        {
            MyHelpers.Log(this, ToString() + " takes food " + tile.settlement.food);
            food += tile.settlement.food;
            foodDays = tile.settlement.food / count;
            tile.settlement.food = 0;
            ls.gui.SetSettlementControls(this, tile.settlement);
            return false;
        }
        // taking some food
        MyHelpers.Log(this, ToString() + " takes food " + wantedDays * count);
        food += wantedDays * count;
        foodDays = wantedDays;
        tile.settlement.food -= wantedDays * count;
        ls.gui.SetSettlementControls(this, tile.settlement);
        return false;*/

        // take up to a max of 10 days
        float currentDaysFood = food / count;
        int reuqestedFood = (int) wantedDays * count;
        if (currentDaysFood + wantedDays > 10)
        {
            wantedDays = 10 - currentDaysFood;
            reuqestedFood = 10 * count - food;
            MyHelpers.Log(this, name + " attempts taking more thant 10 days of food in advance.");
        }

        int takenFood = s.GetFood(this, reuqestedFood);
        food += takenFood;
        foodDays = takenFood / count;
        ls.gui.SetSettlementControls(this, s);
        if (takenFood == reuqestedFood) return true;
        return false;
    }

    /// <summary>
    /// If in settlement on begin of turn and not enough food, supplement food from village for this day
    /// </summary>
    /// <returns></returns>
    public bool GoOutForLunch()
    {
        if (food >= count) return false;
        if (!tile.settlement) return false;
        int taken = tile.settlement.GetFood(this, count - food);
        MyHelpers.Log(this, name + " goes out for lunch in " + tile.settlement);
        if (taken > 0)
        {
            food += taken;
            return true;
        }
        return false;
    }

    public Settlement GetSupply()
    {
        foreach (NeighbourTile nt in Enum.GetValues(typeof(NeighbourTile)))
        {
            TerrainTile t = ls.tiles.GetNeighbouringTile(tile, nt);
            if (t == null) continue;
            if (t.settlement && t.settlement.alignment == player) return t.settlement;
        }
            
        return null;
    }

    public bool Capture()
    {
        Settlement s = tile.settlement;
        bool captured = player == s.alignment;
        if (captured) return false;

        // morale for unit
        int moraleAdded = Defines.def.gameplaySettings.captureEnemyDelta;
        if (s.alignment == ls.GetNeutralPlayer()) moraleAdded = Defines.def.gameplaySettings.captureNeutralDelta;
        morale += moraleAdded;

        // if enemy 
        //if (s.alignment.playerName != Defines.neutralPlayerName)

        // if neutral
        s.alignment = player;
        MyHelpers.LogColor(this, "red", ToString() + " captured settlement " + s);
        ls.gui.SetSettlementControls(this, s);
        finishedTurn = true;
        return true;
    }
    /// <summary>
    /// Can be called with neighbouring tile only. Added tiredness reduced by 50%.
    /// </summary>
    /*public float GetStrengthActualAfterMoveAtEnemy(TerrainTile dTile)
    {
        if (!ls.tiles.IsNeighbour(tile, dTile))
        {
            MyHelpers.LogError(this, "GetStrengthActualAfterMove error");
        }
        int tirednessTemp = tiredness;
        tiredness += GetTirednessForMove(tile, dTile)/2;
        float ret = GetStrengthActual();
        tiredness = tirednessTemp;
        return ret;
    }*/
    /// <summary>
    /// What is known outside, morale and tiredness can be guessed but are not known, fortified or not is known
    /// </summary>
    /// <returns></returns>
    /// PROBABLY SHOULD BE BASED ON MEMORY INFO AND NOT REAL INFO therefor not called on unit
    //public float GetStrengthEst()
    //{
    //    float ret = count;
    //    if (tirednessEst) ret * = (Mathf.Max(0f, 1 - tirednessEst / 100f));
    //    if (moraleEst) ret *= (morale / 100); 
    //}

    /// <summary>
    /// Decides on next move and initiates MoveUnit.
    /// </summary>
    public void ProcessAiTurn(PlayerAI ai)
    {
        //MyHelpers.Log(player.playerAI, "ProcessAiTurn " + name);

        // ATTACK NEIGHBOURING ENEMY IF YOU CAN WIN

        // for each enemy unit in range, if we will we win, attack first enemy unit we can
        var ulist = ls.LiveEnemiesNeighbouring(player, tile).ToList();
        foreach (var u in ulist)
        {
            int winner = Battle.DetermineWinner(this, u, tile, u.tile, true);
            if (winner > 0)
            {
                MyHelpers.Log(player.playerAI, "Attack on " + u.name + " in hope of winning.");
                Attack(u); // set state
                return;
            }
            /*else if (winner == 0)
            {
                //TODO when to sacrifice
            }*/
            else
            {
                if (ai.AdjacentEnemyCountStrengthInAttack(u)[1]-GetStrengthActual() > u.GetStrengthActual()) {
                    MyHelpers.Log(player.playerAI, "Suicide attack on " + u.name + " relying on other unit to finish the job.");
                    Attack(u);
                    return;
                }
            }
        }
        //MyHelpers.Log(player.playerAI, "Not attacking");

        // WILL YOU DIE IF YOU STAY?

        bool badPosition = false;
        ulist = ls.LiveEnemiesNeighbouring(player, tile).ToList();
        foreach (var enemy in ulist)
        {
            int winner = Battle.DetermineWinner(enemy, this, enemy.tile, tile, true);
            if (winner > 1)
            {
                badPosition = true;
                break;
            }
            else if (winner == 0)
            {
                //TODO to hold or to flee
            }
        }
        if (badPosition) // move to defensive position
        {
            MyHelpers.Log(player.playerAI, "In bad position");
            foreach (NeighbourTile nt in Terrain.Directions())
            {
                if (nt == NeighbourTile.Self) continue;
                TerrainTile possibleDefPos = ls.tiles.GetNeighbouringTile(tile, nt);
                if (possibleDefPos == null) continue;
                if (possibleDefPos.LiveEnemies(player).Count() > 0) continue;
                if (IsPositionDefensible(possibleDefPos))
                {
                    MyHelpers.Log(player.playerAI, "Move to defensible position " + possibleDefPos.ToString()
                        + " from bad position " + tile.ToString());
                    MoveStart(possibleDefPos);
                    return;
                };
            }
            if (badPosition) MyHelpers.Log(player.playerAI, "Couldn't move from bad position.");
        }

        // can capture settlement?
        if (tile.settlement && Capture()) return;

        // perform move evaluation
        TerrainTile newTile = player.playerAI.SelectMove(this);
        MyHelpers.Log(player.playerAI, "Performing evaluated move.");
        if (newTile == tile) SkipTurn();
        else if (newTile.LiveEnemies(player).Count() > 0) Attack(newTile.LiveEnemy(player));
        else MoveStart(newTile);
    }
    /// <summary>
    /// Used by AI player.
    /// </summary>
    public void Attack(Unit u)
    {
        MyHelpers.Log(player.playerAI, "Attack " + u.ToString());
        attacking = true;
        MoveStart(u.tile);
    }

    /// <summary>
    /// Is there something to halt execution of turn order, like move or death animations, or attack in progress?
    /// </summary>
    /// <returns></returns>
    public bool IsPending()
    {
        return mb.IsPending();// || attacking;
    }
    /// <summary>
    /// Pathfinding, return next move toward a target tile.
    /// Currently no terrain difficulty taken into account.
    /// </summary>
    TerrainTile FindNextMoveToward(TerrainTile target)
    {
        if (tile == target)
        {
            MyHelpers.Log(this, "Pathfinding gets same tile for target 1");
            return tile;
        }

        bool left = false;
        bool right = false;
        bool top = false;
        bool bottom = false;

        if (target.x < tile.x) left = true;
        else if (target.x > tile.x) right = true;
        if (target.y > tile.y) top = true;
        else if (target.y < tile.y) bottom = true;

        if (top)
        {
            if (left) return ls.tiles.GetTopLeftTile(tile);
            else if (right) return ls.tiles.GetTopRightTile(tile);
            else ls.tiles.GetTopTile(tile);
        }
        if (bottom)
        {
            if (left) return ls.tiles.GetBottomLeftTile(tile);
            else if (right) return ls.tiles.GetBottomRightTile(tile);
            else ls.tiles.GetBottomTile(tile);
        }
        if (left) return ls.tiles.GetLeftTile(tile);
        else if (right) return ls.tiles.GetLeftTile(tile);

        MyHelpers.Log(this, "Pathfinding gets same tile for target 2");
        return tile;
    }
    /// <summary>
    /// Will the unit die if it stays on given tile.
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    bool IsPositionDefensible(TerrainTile t)
    {
        //foreach (var u in ls.units.Where(x => x.player != player
        //&& ls.tiles.GetTileDistanceSquare(t, x.tile) <= 1))
        var ulist = ls.LiveEnemiesNeighbouring(player, t).ToList();
        foreach (Unit u in ulist)
        {
            int winner = Battle.DetermineWinner(u, this, u.tile, t, true);
            if (winner >= 0) return false;
        }
        return true;
    }


    /// <summary>
    /// Mark uint as killed but leave in data structure in case we ever want to resurrect.
    /// </summary>
    public void SetKilled()
    {
        killed = true;
        count = 0;
        //tiredness = 100;
        stamina = 0;
        tile.RemoveUnit(this);
        mb.FadeOutOnDeath();
    }

    public bool SkipTurn()
    {
        if (moved || skipsTurn || finishedTurn)
        {
            MyHelpers.LogError(this, name + " attempts skip turn but already moved or skipped.");
            return false;
        }

        MyHelpers.Log(this, name + " skips turn.");
        skipsTurn = true;
        finishedTurn = true;

        return true;
    }

    public string GetInfoText()
    {
        if (count == 0) return null;
        // unit stats
        string ret = name + "\n" + count + " men, "  + GetStrengthActual() + " strong\n"
            + stamina + " fresh, morale is " + morale
            + "\nFood for " + food / count + " days\n"
            + "Marching for " + marchDays + " days\n"
            + "Bioenergy: " + mana + "\n";

        // battle estimate
        if (ls.GetCurrentPlayer().playerType == PlayerType.local && player != ls.GetCurrentPlayer())
        {
            int winner = Battle.DetermineWinner(ls.currentUnit, this, ls.currentUnit.tile, tile, predict: true);
            if (winner > 0) ret += "<color=green>You would win!</color>";
            else if (winner < 0) ret += "<color=red>You would lose!</color>";
            else ret += "<color=red>Mutual extermination!</color>";
        }

        return ret;
    }


    public static implicit operator bool(Unit u)
    {
        if (u != null) return true;
        return false;
    }

    public override string ToString()
    {
        return "U " + name;
    }

}


