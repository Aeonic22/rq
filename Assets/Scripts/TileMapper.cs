﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// For every terrain type, holds tiles for every reveal state.
/// Also holds colors for seen and unknown tiles.
/// </summary>
public class TileMapper : MonoBehaviour {

    public List<TerrainType> terrainTypes;
    public List<Sprite> terrainSprites;
    public List<Sprite> terrainSpritesMap;
    public List<Sprite> terrainSpritesSeen;
    public Color tintSeen;
    public Color tintUnknown;   // for debug
    public TileBase hatchTile;

    // cached
    private GameMain gm;

    // Use this for initialization
    void Start () {
        gm = FindObjectOfType<GameMain>();
    }

    public Sprite GetSpriteForTile(TerrainType tt, out Color tint)
    {
        tint = new Color(1, 1, 1, 1);
        int index = terrainTypes.FindIndex(x => x == tt);
        return terrainSprites[index];
    }

    #region hiddenInformation
    /* public Sprite GetSpriteForTile(TileType tt, TileRevealStatus tr, out Color tint)
    {
        tint = new Color(1, 1, 1, 1);
        int index = terrainTypes.FindIndex(x => x == tt);
        if (tr == TileRevealStatus.Unknown)
        {
            if (gm.fogOfWar)
            {
                tint = new Color32(0, 0, 0, 0);
                return terrainSprites[index];
            }
            else
            {
                tint = tintUnknown;
                return terrainSprites[index];
            }

        }
        if (tr == TileRevealStatus.OnMap) return terrainSpritesMap[index];
        //if (tr == TileRevealStatus.Seen)
        //{
        //    tint = tintSeen;
        //    return terrainSpritesSeen[index];
        //}
        if (tr == TileRevealStatus.Visited || tr == TileRevealStatus.Seen) return terrainSprites[index];
        return terrainSprites[0]; // not set, error
    }*/
    #endregion


    // Update is called once per frame
    void Update () {
		
	}
}

