﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// Randomly creates terrain with given dimensions and parameters.
/// Best to use it on enpty tilemap object.
/// </summary>
public class TerrainGenerator : MonoBehaviour {

    [Header("Terrain")]
    [Range(3, 16)]
    public int xTiles;
    [Range(3, 16)]
    public int yTiles;

    [Tooltip("-1 for random"), Range(-1,99999)]
    public int terrainSeed;
    [Tooltip("read only")]
    public int lastTerrainSeed;

    [Header("Units")]
    [Range(0, 10)] //TODO add for more players if more factions of enemy on screen
    public int playerUnits;
    [Range(0, 10)]
    public int enemyUnits;

    [Tooltip("-1 for random"), Range(-1, 99999)]
    public int unitsSeed;
    [Tooltip("read only")]
    public int lastUnitsSeed;

    // cached
    private Tilemap tmMain;
    [Header("Players")]
    public PlayerMB localPlayerMB;
    public PlayerMB enemyPlayerMB;
    [Header("Unit templates")]
    public GameObject playerUnitMB;
    public GameObject enemyUnitMB;

    // Use this for initialization
    public bool GenerateTerrain () {
        if (xTiles < 3 || yTiles < 3) return false;
        if (xTiles > 100 || yTiles > 100) return false;

        if (!InitTileMapper()) return false;
        if (!InitTilemap()) return false;

        if (terrainSeed == -1) lastTerrainSeed = Random.Range(0, 99999);
        else lastTerrainSeed = terrainSeed;
        Random.InitState(lastTerrainSeed);
        tmMain.ClearAllTiles();
        for (int i = 0; i < xTiles; i++)
        {
            for (int j = 0; j < yTiles; j++)
            {
                MyTile tile = ScriptableObject.CreateInstance<MyTile>();
                tile.Init((TerrainType) Random.Range(0, 3));
                Vector3Int pos = new Vector3Int(i, j, 0);
                tmMain.SetTile(pos, tile);
            }
        }
        tmMain.RefreshAllTiles();
        return true;
    }

    /// <summary>
    /// Uses existing tilemap dimensions to populate scene.
    /// Creates local and enemy player if not yet created.
    /// </summary>
    public bool GenerateUnits()
    {
        // prerequisites
        if (localPlayerMB == null || enemyPlayerMB == null || playerUnitMB == null || enemyUnitMB == null)
            {
            MyHelpers.LogError(this, "Must connect players and unit templates");
            return false;
        }
        Player player = localPlayerMB.me;
        Player enemy = enemyPlayerMB.me;

        GameObject unitsFolder = GameObject.Find(Defines.unitsFolderName);
        if (unitsFolder == null)
        {
            MyHelpers.LogError(this, "Must have Runtime folders in place");
            return false;
        }

        if (!InitTilemap()) return false;
        BoundsInt bi = Terrain.GetBoundsUsed(tmMain);
        bool[,] taken = new bool[bi.xMax, bi.yMax];

        // clean up
        UnitMB [] units = FindObjectsOfType<UnitMB>();
        foreach (UnitMB u in units) DestroyImmediate(u.gameObject);

        // create
        if (unitsSeed == -1) lastUnitsSeed = Random.Range(0, 99999);
        else lastUnitsSeed = unitsSeed;
        Random.InitState(lastUnitsSeed);

        int i = 0;
        while(true)
        {
            int x = Random.Range(0, bi.xMax);
            int y = Random.Range(0, bi.yMax);
            if (taken[x, y]) continue;
            taken[x, y] = true;
            Vector3 pos = new Vector3(x, y, Defines.def.miscGameSettings.zPosUnits);
            GameObject go;
            if (i < playerUnits)
            {
                go = Instantiate(playerUnitMB, pos, Quaternion.identity, unitsFolder.transform);
                go.GetComponent<UnitMB>().playerMB = localPlayerMB;
            }
            else
            {
                go = Instantiate(enemyUnitMB, pos, Quaternion.identity, unitsFolder.transform);
                go.GetComponent<UnitMB>().playerMB = enemyPlayerMB;
            }
            i++;
            if (i == playerUnits + enemyUnits) break;
        }
        return true;
    }

    private bool InitTilemap()
    {
        if (tmMain == null) tmMain = MyHelpers.FindInChildren(this.transform, Defines.tilemapMainName).gameObject.GetComponent<Tilemap>();
        if (tmMain == null)
        {
            MyHelpers.LogError(this, "Must have TilemapMain object on scene");
            return false;
        }
        return true;
    }

    private bool InitTileMapper()
    {
        if (MyTile.tMapper == null) MyTile.tMapper = FindObjectOfType<TileMapper>().GetComponent<TileMapper>();
        if (MyTile.tMapper == null)
        {
            MyHelpers.LogError(this, "Must have TileMapper object on scene");
            return false;
        }
        return true;
    }

    // Update is called once per frame
    void Update () {
		
	}
}

