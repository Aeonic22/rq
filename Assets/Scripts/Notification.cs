﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notification : MonoBehaviour
{
    //set 
    public float totalDuration;
    public float beginx;
    public float beginy;
    public float endx;
    public float endy;

    private Notification n;

    // fetch
    Text text;

    public void Awake()
    {
        text = GetComponent<Text>();
    }
    public void ShowNotification(string message)
    {
        n = Instantiate(this, transform.parent);
        n.GetComponent<Text>().enabled = true;
        StartCoroutine(n.ShowNotificationCR(message));
    }
    public IEnumerator ShowNotificationCR(string message)
    {
        print("ShowNotificationCR");
        enabled = true;
        text.text = message;
        float t = 0;
        while (t < totalDuration)
        {
            t += Time.deltaTime;
            float x = Mathf.Lerp(beginx, endx, t / totalDuration);
            float y = Mathf.Lerp(beginy, endy, t / totalDuration);
            float fade = Mathf.Lerp(1, 0, t / totalDuration);
            Vector3 pos = new Vector3(x, y, gameObject.transform.position.z);
            RectTransform rect = (RectTransform)gameObject.transform;
            rect.anchoredPosition = pos;
            Color c = text.color;
            c = new Color(c.r, c.g, c.b, fade);
            text.color = c;
            yield return null;
        }
        enabled = false;
        Destroy(gameObject);

    }

    public bool IsAnimating()
    {
        if (n != null) return true;
        return false;
    }
}
