﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Has name, side, type and AI (optional).
/// Can process turn.
/// Local player processes turn by doing nothing, just waiting, UI actions are handled elsewhere.
/// </summary>
[System.Serializable]
public class Player {

    [System.NonSerialized]
    public IPlayerMB mb;

    // params
    public PlayerType playerType;
    [System.NonSerialized] public PlayerAI playerAI;       // optional
    public string playerName;
    public string playerSide;       // set by game, ignored for now?

    // state
    [System.NonSerialized]
    public Unit lastSelectedUnit;

    // cached
    private LevelState ls;

    /// <summary>
    /// Objects created at runtime so all references are ok.
    /// </summary>
    void Awake()
    { 
    }
    void Start () {
        
    }

    public Player() { }
    /// <summary>
    /// For AI and misc use only
    /// Must run init after this
    /// </summary>
    public Player(Player source)
    {
        playerType = source.playerType;
        playerAI = source.playerAI;
        playerName = source.playerName;
    }

    public void Init(IPlayerMB mb, PlayerAI playerAI, LevelState ls)
    {
        this.mb = mb;
        this.playerAI = playerAI;
        this.ls = ls;
        if (playerAI) playerAI.Init(ls, this);
    }

    public int GetIndex()
    {
        return ls.players.FindIndex(x => x == this);
    }

    /// <summary>
    /// Basically just propagates turn number.
    /// </summary>
    /// <param name="turn"></param>
    public void BeginTurn()
    {
        if (playerAI != null) playerAI.BeginTurn();

        //foreach (Unit u in ls.units.FindAll(x => x.player == this && !x.killed))
        foreach (Unit u in ls.LiveFriendlies(this))
        {
            u.BeginGlobalTurn();
        }
    }
    /// <summary>
    /// Either AI is called or we just yield wait for local UI actions to finish.
    /// //TODO could just return false if local player, and turn manager could then rest until restarted by EndTurn
    /// </summary>
    public IEnumerator ProcessPerPlayerTurn(int turn)
    {
        /*Debug.Assert((turn - 1) == finishedTurn);
        currentTurn = turn;

        // do turn
        if (playerType == PlayerType.local)
        {
            Log(playerName);
            ls.controlToPlayer = true; // leave control to player until he ends it
            //if (lastSelectedUnit) lastSelectedUnit.SelectMe();
            while (gm.controlToPlayer == true)
                yield return new WaitForSeconds(Defines.def.yieldWaitDelay);
        }
        else if (playerType == PlayerType.AI)
        {
            gm.controlToPlayer = false;
            Log(playerName);
            yield return playerAI.ProcessTurn(turn);
        }

        // end turn
        EndTurn(turn);*/
        return null;

    }

    /// <summary>
    /// Called for specific unit of player in a game where turn order is by unit and not by player.
    /// </summary>
    /// <param name="u"></param>
    /// <returns></returns>
    public void ProcessPerUnitTurn(Unit u)
    {
        MyHelpers.Log(this, "ProcessPerUnitTurn " + this.ToString());
        if (u.player != this)
        {
            MyHelpers.LogError(this, "Player process turn error");
            return;
        }

        // do turn
        if (playerType == PlayerType.local)
        {
            MyHelpers.Log(this, playerName + ", unit " + u.name);
        }
        else if (playerType == PlayerType.AI)
        {
            MyHelpers.Log(this, playerName + ", unit " + u.name);
            //gm.controlToPlayer = false;
            playerAI.ProcessUnitTurn(u);
        }
    }

    /// <summary>
    /// Also automatically ends player turn - when all units finish.
    /// </summary>
    public bool IsLocalTurnFinished()
    {
        //var myUnits = ls.units.FindAll(x => x.player == this && !x.killed);
        foreach (Unit u in ls.LiveFriendlies(this))
        {
            if (!u.TurnFinishedOrSkipped()) return false;
            MyHelpers.Log(this, "IsTurnFinished FALSE");
        }
        //EndTurn();
        MyHelpers.Log(this, playerName + " END TURN");
        return true;
    }
    /// <summary>
    /// Advance local variables and relinquish UI control if local player.
    /// </summary>
    public void EndTurn(int turn)
    {
        MyHelpers.Log(this, playerName + " EndTurn");
        //foreach (Unit u in ls.units.FindAll(x => x.player == this && !x.killed))
        foreach (Unit u in ls.LiveFriendlies(this))
        {
            u.EndTurn();
        }
        //if (playerType == PlayerType.local) gm.controlToPlayer = false; //da li ovdje...
        if (playerAI != null) playerAI.EndTurn();
    }

    public void BattleSpell(Battle battle)
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override string ToString()
    {
        return playerName;
    }


}
