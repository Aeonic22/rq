﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcTerrainGen : MonoBehaviour {

    /// <summary>
    /// TODO: Incorporates terrain size as first 4 digits. -1 means use random.
    /// </summary>
    public int seedTerrain;
    /// <summary>
    /// Does not assume unit number or terrain type. -1 means use random.
    /// </summary>
    public int seedUnits;

    public int numFriendlyUnits = 1;
    public int numEnemyUnits;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
