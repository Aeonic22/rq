﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/*
 * Conventions:
 * gm GameMain
 * ls levelstate
 * sr SpriteRenderer
 * tmXXX tilemap
 * tt TileType
 * u Unit
 * tMapper TileMapper
 */

 //TODO game params as scriptable object, visual params as another
 // then reference SOs and you can use them as sets of settins


/// <summary>
/// AI knowledge maps: visibilityEnemy
/// </summary>
public enum KnolMapType { unitVisibility, terrainReveal, lineRenderer }

/// <summary>
/// AI knowledge maps: visibilityEnemy
/// </summary>
public enum KnolMapOps { add, subtract, mutliply, divide, max, min, avg }

/// <summary>
/// local, opponent, network, AI
/// opponent is network or AI
/// </summary>
public enum PlayerType { local, AI }

/// <summary>
/// Square, Air, Manhattan, Travel
/// </summary>
public enum DistanceType { Square, Air, Manhattan, Travel, MovesDirect }

/// <summary>
/// Unset = -1, Meadow, Forest, Mountain
/// </summary>
public enum TerrainType { Meadow, Forest, Mountain }

/// <summary>
///  Unknown, OnMap, Seen, Visited
/// </summary>
public enum TileRevealStatus { Unknown, OnMap, Seen, Visited }

/// <summary>
/// Relation between every other enemy unit and your unit.
///  Unknown, Invisible, Visible, Pursue, Battle
/// </summary>
public enum UnitEngagement { Unknown, Invisible, Visible, Pursue, Battle, Dead }

/// <summary>
/// Self, Left, Right, Top, Bottom, TopLeft, TopRight, BottomLeft, BottomRight
/// </summary>
public enum NeighbourTile { Self, Left, Right, Top, Bottom, TopLeft, TopRight, BottomLeft, BottomRight }

//public enum eUnitAlignment { template, player, ally, neutral, enemy, aggressive }

/// <summary>
/// Unit AI states - Patrol, Intercept, Evade
/// </summary>
public enum UnitAIState { Unset = -1, Hold, Defend, Patrol, Intercept, Evade, Hungry }

public class Defines : MonoBehaviour
{

    // singleton
    public static Defines def;
    public static float sqrt2 = Mathf.Sqrt(2);

    // code only, not shown in inspector
    // linked by name since not part of same prefab and I don't want to connect them on every level
    public const string tilemapMainName = "TilemapMain";
    public const string tilemapHatchName = "TilemapTest";
    public const string unitsFolderName = "Units";
    public const string tilesFolderName = "Tiles";
    public const string playersFolderName = "Players";
    public const string targetingFolderName = "Targeting";
    public const string targetingTextFolderName = "TargetingText";
    public const string goalsFolderName = "Goals";
    public const string unitGuisFolderName = "UnitGuis";
    //public const string guiControllerName = "Main Camera";
    public const string neutralPlayerName = "Neutral player";
    public const string spellParametersPanelName = "PanelSpellParameters";


    // all static [Header("Map settings")]
    /// <summary>
    /// for 2 tiles away except for corners
    /// </summary>
    [NonSerialized]
    public static float airDistTwoMinusCorner = 2.45f;
    /// <summary>
    /// for 3 away, except for 3 tiles in each corner
    /// </summary>
    [NonSerialized] public static float airDistThreeMinusBigCorner = 3.3f;
    /// <summary>
    /// how far can you see from mountain
    /// </summary>
    [NonSerialized] public static float mountainRevealTerrainDistance = airDistTwoMinusCorner;
    /// <summary>
    /// from how far do you see mountain
    /// </summary
    [NonSerialized] public static float revealMountainDistance = airDistThreeMinusBigCorner;
    /// <summary>
    /// reveal unit distance if set to 2 tiles away
    /// </summary
    [NonSerialized] public static float revealUnitsDistanceForTwoTiles = airDistTwoMinusCorner;
    /// <summary>
    /// reveal unit distance if set to 1 tile away
    /// </summary
    [NonSerialized] public static float revealUnitsDistanceForOneTile = 1.5f;
    /// <summary>
    /// num neighbouring tiles for move (including self). 5 for square, 7 for hex, 9 for square and diagonal
    /// </summary>
    [NonSerialized] public int numNeighbours = 9;

    public MiscGameSettings miscGameSettings;
    public GameplaySettings gameplaySettings;
    public LogSettings logSettings;
    public static int TirednessPerTerrain(TerrainType tt) { return def.gameplaySettings.terrainSpecs[(int)tt].tiredness;  } 
    public static int ManaPerTerrain(TerrainType tt) { return def.gameplaySettings.terrainSpecs[(int)tt].manaGained; }

    private void Awake()
    {
        if (def == null) def = this;
        else
        {
            MyHelpers.LogError(this, "defines singleton error");
            Destroy(this);
        }
    }

}
