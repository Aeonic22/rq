﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TurnOrderType { PerPlayer, PerUnit, Simultaneous }

/*
 * DEPRECATED
 */

/// <summary>
/// Knows sides, loops thru them and runs their ProcessTurn.
/// Turn finished when no action points or no more decisions to act or end turn pressed.
/// replace with v2, per unit turn initiative
/// </summary>
public class TurnManager : MonoBehaviour
{
    // setup
    public List<Player> players;
    public Image turnOrderCurrentImg;
    public Image turnOrderNextImg;
    public TurnOrderType turnOrderType;
    public float waitBetweenTurns;

    // misc
    [HideInInspector]
    public GameObject playersFolder;
    public Button btnUnitInQueue;

    // state
    [HideInInspector] public List<Unit> turnOrderCurrent;
    [HideInInspector] public List<Unit> turnOrderNext;
    int currentUnitInOrder;
    public int currentPlayerIndex;
    public int currentTurn;
    bool doRestart = false;
    bool inited = false;
    bool gameEnded = false;

    // cached
    GameMain gm;
    LevelState ls = null;

    public Player GetCurrentPlayer() { return players[currentPlayerIndex];  }
    public int GetCurrentPlayerIndex(Player player)
    {
        int c = 0;
        foreach (Player p in players)
        {
            if (p == player) return c;
            c++;
        }
        return -1;
    }
    public bool IsCurrentPlayerLocal() { return GetCurrentPlayer().playerType == PlayerType.local; }

    void Start()
    {
        playersFolder = GameObject.Find(Defines.playersFolderName);

        // instantiate as needed, gm needs them on init
        // RANDOM INIT
        /*for (int i = 0; i < numLocalPlayers; i++)
        {
            Player p = Instantiate<Player>(templateLocalPlayer, playersFolder.transform);
            p.name = "Local player " + i;
            players.Add(p);
        }
        for (int i = 0; i < numAIntPlayers; i++)
        {
            Player p = Instantiate<Player>(templateAIntPlayer, playersFolder.transform);
            p.name = "AI player " + i;
            players.Add(p);
        }*/

        turnOrderType = TurnOrderType.PerUnit;

        //LEVEL SCENE INIT
        PlayerMB[] arrPlayers = GameObject.FindObjectsOfType<PlayerMB>();
        foreach (PlayerMB player in arrPlayers)
            players.Add(player.me);

    }

    /// <summary>
    /// I guess Awake or Start didn't cut it
    /// </summary>
    void Init()
    {
        if (doRestart) Restart();
        doRestart = false;
        inited = true;
        gm = FindObjectOfType<GameMain>();
        //ls = FindObjectOfType<LevelStateMB>().me;

        if (turnOrderType == TurnOrderType.PerPlayer) StartCoroutine(ProcessTurnPerPlayer());
        else if (turnOrderType == TurnOrderType.PerUnit) StartCoroutine(ProcessTurnPerUnit());
    }

    /// <summary>
    /// 
    /// </summary>
    public void Restart()
    {
        foreach (Player p in players)
        {
            //Destroy(p.mb);
        }
    }
    /// <summary>
    /// Turn by player.
    /// </summary>
    public IEnumerator ProcessTurnPerPlayer()
    {
        while (!gameEnded)
        {
            if (currentPlayerIndex >= players.Count)
            {
                currentPlayerIndex = 0;
            }
            if (currentPlayerIndex == 0) currentTurn++;
            MyHelpers.Log(this, "TURN " + currentTurn + ", current player " + currentPlayerIndex);
            yield return players[currentPlayerIndex].ProcessPerPlayerTurn(currentTurn);
            currentPlayerIndex = (currentPlayerIndex + 1);
        }
        MyHelpers.Log(this, "TURN MANAGER END");
    }

    /// <summary>
    /// Turn by unit initiative.
    /// </summary>
    public IEnumerator ProcessTurnPerUnit()
    {
        TurnOrderSorter comp = new TurnOrderSorter(sortByTotalTiredness: true);

        currentPlayerIndex = 0;
        currentTurn = 1;
        currentUnitInOrder = 0;

        if (currentTurn == 1)
        {
            turnOrderCurrent = new List<Unit>(ls.units); // TODO set in random order per level basis for recreatability
            turnOrderNext.Clear();
        }

        while (!gameEnded)
        {
            // finish turn after all units done
            if (currentUnitInOrder == turnOrderCurrent.Count)
            {
                for (int i = 0; i < players.Count; i++)
                {
                    players[i].EndTurn(currentTurn);
                }
                currentTurn++;
                currentUnitInOrder = 0;
            }
            
            // begin new turn
            if (currentUnitInOrder == 0) 
            {

                // prepare turn order
                turnOrderNext = new List<Unit>(ls.units);
                comp.sortByTotalTiredness = true; //TODO LEon ???
                turnOrderNext.Sort(comp);

                turnOrderCurrent = new List<Unit>(ls.units);
                comp.sortByTotalTiredness = false;
                turnOrderCurrent.Sort(comp);

                // signal next turn //TODO any use?
                for (int i = 0; i < players.Count; i++)
                {
                    players[i].BeginTurn();
                }
                gm.gui.NotifyNextTurn();
                yield return new WaitForSeconds(waitBetweenTurns);
            }
            
            // process one unit
            Unit u = turnOrderCurrent[currentUnitInOrder];
            if (!u.killed)
            {
                MyHelpers.Log(this, "TURN " + currentTurn + ", current unit " + u.name);
                currentPlayerIndex = GetCurrentPlayerIndex(u.player); // always know whose player turn it is
                                                                      //MyHelpers.Log(this, "sort 1");
                turnOrderNext = new List<Unit>(ls.units);   // update next turn order
                comp.sortByTotalTiredness = true;
                turnOrderNext.Sort(comp);
                DisplayTurnOrder();

                //yield return u.player.ProcessPerUnitTurn(u);//TODO uncomment
            }
            currentUnitInOrder++; // cycle units until turn finished
        }
        MyHelpers.Log(this, "TURN MANAGER END");
    }

    /// <summary>
    /// Turn order: who added least tiredness last turn, then total tiredness. Dead units last.
    /// </summary>
    public class TurnOrderSorter : IComparer<Unit> 
    {
        public bool sortByTotalTiredness; // total for initial next turn order, recent tiredness for actual turn order
        public int inverse = -1;
        int ret;

        public TurnOrderSorter(bool sortByTotalTiredness)
        {
            this.sortByTotalTiredness = sortByTotalTiredness;
        }

        public int Compare(Unit u1, Unit u2)
        {

            //try
            //{
                if (!u1.killed && u2.killed) return (ret = 1 * inverse);
                if (u1.killed && !u2.killed) return (ret = -1 * inverse);

                if (!sortByTotalTiredness)  // sort by last turn added tiredness first
                {
                    if (u1.tirednessAddedLastMove == u2.tirednessAddedLastMove) return (ret =0);
                    if (u1.tirednessAddedLastMove < u2.tirednessAddedLastMove) return (ret = 1 * inverse);
                    if (u1.tirednessAddedLastMove > u2.tirednessAddedLastMove) return (ret = -1 * inverse);
                }
                if (u1.stamina == u2.stamina) return (ret = 0);
                if (u1.stamina > u2.stamina) return ret = 1 * inverse;
                if (u1.stamina < u2.stamina) return ret = -1 * inverse;
                return 1 * inverse; //TODO last one to move is last one this turn also, or one with more kills, or one with less men, or other initiative modifiers
            //} finally
            //{
            //    if (sortByTotalTiredness) MyHelpers.Log(this, "SORT " + u1.tiredness + ", " + u2.tiredness + ", " + ret);
            //    else MyHelpers.Log(this, "SORT " + u1.tirednessAddedLastMove + ", " + u2.tirednessAddedLastMove + ", " + ret);
            //}
        }
    }

    public void DisplayTurnOrder()
    {
        // get coords from image
        // get unit portraits
        // draw LIVING in order with some spacing
        // create and position current turn marker - sprite

        string ret = "TURN + "+ currentTurn + "\n\nCURRENT ORDER\n";
        int count = 0;
        foreach (Unit u in turnOrderCurrent)
        {
            if (!u.killed)
            {
                if (count == currentUnitInOrder) ret += ">> ";
                ret += u.name + ", tl:" + u.tirednessAddedLastTurn + "\n";
            }
            count++;
        }
        ret += "\nNEXT TURN ORDER\n";
        count = 0;
        foreach (Unit u in turnOrderNext)
        {
            if (u.killed) continue;
            if (u == turnOrderCurrent[currentUnitInOrder]) ret += ">> ";
            ret += u.name + ", ts:" + u.stamina + "\n";
        }
        gm.gui.SetGameInfoText(ret);
    }

    void Update()
    {
        if (doRestart)
        { 
            Init();
        }
        if (!inited)
        {
            Init();
        }
    }

    /// <summary>
    /// Called by lower instances, player, player AI with their text as input
    /// This adds own text and calls GameMain to display it.
    /// </summary>
    /// <param name="text"></param>
    void SetGameInfoText(string text)
    {
        string info = "Game turn " + currentTurn + "\n" + text;
        gm.gui.SetGameInfoText(info);
    }


}

/// <summary>
/// 
/// </summary>
/// <remarks>
/// Game can be cloned at any time, turn manager will be able to continue, 
/// depending on the analysis of variables in LevelState. The game does not need to 
/// explicitly set state.
/// </remarks>
public enum GameState { PlayerTurn, EnemyTurn, PreBattle, AfterBattle, Story, Magic }
/// <summary>
/// Orchestrates order of events in a game.
/// </summary>
/// <remarks>
/// Turn manager assures that proper unit is selected to move next, 
/// switches to next unit after unit has ended turn,
/// ends movement phase after all units have moved, 
/// allows all units to cast magic during night,
/// starts next turn.
/// </remarks>
public interface ITurnManager
{
}