﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellParametersPanel : MonoBehaviour {

    public bool isHovered;

    // Use this for initialization
    void Start() {

    }

    void Init()
    {
        isHovered = false;
    }

    private void OnMouseEnter()
    {
        isHovered = true;
    }

    private void OnMouseExit()
    {
        isHovered = false;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
