﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettlementMB : MonoBehaviour, ISettlementMB {

    public static SettlementMBDummy settlementMBDummy;

    [Header("Core object")]
    public Settlement me;

    [Header("Connections")]
    [Tooltip("Must connect to Player instance on scene")]
    public PlayerMB initialAligment; 
    private SpriteRenderer sr;
    private static GameMain gm;
    private LevelState ls;
    private SpriteRenderer flag;

    private bool needsUpdate;

    static SettlementMB()
    {
        settlementMBDummy = new SettlementMBDummy();
    }

    // Use this for initialization
    void Start () {
        sr = GetComponent<SpriteRenderer>();
        gm = FindObjectOfType<GameMain>();
        ls = gm.ls;
        flag = transform.Find("Flag").GetComponent<SpriteRenderer>();
        TerrainTile tile = ls.tiles.GetTileAtCoords(transform.position);
        if (me.mb != null) return; // already inited by CreateUnit
        me.Init(this, name, gm, ls, initialAligment ? initialAligment.me : gm.ls.GetNeutralPlayer(), tile);
        SetAlignment();
    }

    public void NeedsUpdate() { needsUpdate = true; }

    // Update is called once per frame
    void Update () {
		
	}

    public void DestroyMe()
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// Transfer click to tile. I still keep collider on Units for OnMouseEnter events.
    /// </summary>
    private void OnMouseUp()
    {
        ls.TileActionUI(me.tile);
    }
    public void SetSprite(Sprite s)
    {
        sr.sprite = s;
    }
    /*private void OnMouseEnter()
    {
        gm.gui.SetUnitInfoText(me.GetInfoText());
    }
    private void OnMouseExit()
    {
        gm.gui.SetUnitInfoText(null);
    }*/

    /// <summary>
    /// For setting initial aligment (param == null), used in editor to color flag.
    /// Or for changing alignment (flag) in game.
    /// </summary>
    public void SetAlignment(Player alignment = null)
    {
        if (alignment == null)
        {
            flag = transform.Find("Flag").GetComponent<SpriteRenderer>();
            flag.sprite = initialAligment.GetFlag();
        }
        else
            flag.sprite = me.alignment.mb.GetFlag();
    }
    

}

public interface ISettlementMB
{
    void NeedsUpdate();
    void SetSprite(Sprite s);
    void DestroyMe();
    void SetAlignment(Player alignment = null);
}

public class SettlementMBDummy : ISettlementMB
{
    public void NeedsUpdate() { }
    public void SetSprite(Sprite s) { }
    public void DestroyMe() { }
    public void SetAlignment(Player alignment = null) { }

}