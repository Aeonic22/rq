﻿//using System.Collections
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// Accessible by coordinates.
/// Helper functinos for neighbouting tiles.
/// </summary>
public class Terrain {

    [Header("Dimensions")]
    public int xTiles, yTiles;
    public List<TerrainTile> tiles;

    public Terrain(int x, int y)
    {
        xTiles = x;
        yTiles = y;
        tiles = new List<TerrainTile>(xTiles * yTiles);
    }

    /// <summary>
    /// Used for AI and misc only.
    /// Clones terrain, without any units and settlements. 
    /// Instead of link to MB object it is linked to dummy with empty functions.
    /// </summary>
    public Terrain(Terrain source)
    {
        xTiles = source.xTiles;
        yTiles = source.yTiles;
        tiles = new List<TerrainTile>(xTiles * yTiles);
        for (int i = 0; i < xTiles; i++)
        {
            for (int j = 0; j < yTiles; j++)
            {
                TerrainTile t = new TerrainTile(source[i, j]);
                tiles.Add(t);
            }
        }
    }

    public TerrainTile this[int i, int j] { 
        get
        {
            return tiles[i * yTiles + j];
        }
        set
        {
            tiles[i * yTiles + j] = value;
        }
    }
    public List<TerrainTile> GetAllTiles() { return tiles; }

    public TerrainTile GetTileAtCoords(Vector3 pos)
    {
        int ix = Mathf.FloorToInt(pos.x);
        int iy = Mathf.FloorToInt(pos.y);
        return this[ix, iy];
    }

    public void ClearTempData()
    {
        foreach (TerrainTile t in tiles)
        {
            t.tempManaCost = -1;
        }
    }


    #region navigationAndDistance 
    //TODO make generic neighbours in every tile for every map type instead of parent object controlling them
    public TerrainTile GetNeighbouringTile(TerrainTile t, NeighbourTile n)
    {
        if (t == null)
        {
            MyHelpers.LogError(this, "null tile");
            return null;
        }
        switch (n)
        {
            case NeighbourTile.Self: return t;
            case NeighbourTile.Left: return GetLeftTile(t);
            case NeighbourTile.Right: return GetRightTile(t);
            case NeighbourTile.Top: return GetTopTile(t);
            case NeighbourTile.Bottom: return GetBottomTile(t);
            case NeighbourTile.TopLeft: return GetTopLeftTile(t);
            case NeighbourTile.TopRight: return GetTopRightTile(t);
            case NeighbourTile.BottomLeft: return GetBottomLeftTile(t);
            case NeighbourTile.BottomRight: return GetBottomRightTile(t);

            default:
                return null;
        }
    }
    public TerrainTile GetLeftTile(TerrainTile tile)
    {
        if (tile.x < 1) return null;
        return this[tile.x - 1, tile.y];
    }
    public TerrainTile GetRightTile(TerrainTile tile)
    {
        if (tile.x > xTiles - 2) return null;
        return this[tile.x + 1, tile.y];
    }
    public TerrainTile GetTopTile(TerrainTile tile)
    {
        if (tile.y > yTiles - 2) return null;
        return this[tile.x, tile.y + 1];
    }
    public TerrainTile GetBottomTile(TerrainTile tile)
    {
        if (tile.y < 1) return null;
        return this[tile.x, tile.y - 1];
    }
    public TerrainTile GetTopLeftTile(TerrainTile tile)
    {
        if (tile.x < 1) return null;
        if (tile.y > yTiles - 2) return null;
        return this[tile.x - 1, tile.y+1];
    }
    public TerrainTile GetTopRightTile(TerrainTile tile)
    {
        if (tile.x > xTiles - 2) return null;
        if (tile.y > yTiles - 2) return null;
        return this[tile.x + 1, tile.y+1];
    }
    public TerrainTile GetBottomLeftTile(TerrainTile tile)
    {
        if (tile.x < 1) return null;
        if (tile.y < 1) return null;
        return this[tile.x - 1, tile.y-1];
    }
    public TerrainTile GetBottomRightTile(TerrainTile tile)
    {
        if (tile.x > xTiles - 2) return null;
        if (tile.y < 1) return null;
        return this[tile.x + 1, tile.y-1];
    }
    public TerrainTile GetOffsetTile(TerrainTile tile, int xoffset, int yoffset)
    {
        if (tile.x + xoffset < 0 || tile.x + xoffset >= xTiles) return null;
        if (tile.y + yoffset < 0 || tile.y + yoffset >= yTiles) return null;
        return this[tile.x + xoffset, tile.y + yoffset];
    }
    public bool IsNeighbour(TerrainTile t1, TerrainTile t2)
    {
        if (t1 == t2) MyHelpers.LogError(this, "IsNeighbour 'self' used");
        if (GetLeftTile(t1) == t2) return true;
        if (GetRightTile(t1) == t2) return true;
        if (GetTopTile(t1) == t2) return true;
        if (GetBottomTile(t1) == t2) return true;
        if (GetTopLeftTile(t1) == t2) return true;
        if (GetTopRightTile(t1) == t2) return true;
        if (GetBottomLeftTile(t1) == t2) return true;
        if (GetBottomRightTile(t1) == t2) return true;
        return false;
    }
    public List<TerrainTile> GetTilesWithinDistance(TerrainTile t1, float distance, DistanceType dt, bool includeSelf=true)//TODO replace with proper c# code
    {
        List<TerrainTile> lTiles = new List<TerrainTile>();
        for (int i = 0; i < xTiles; i++)
        {
            for (int j = 0; j < yTiles; j++)
            {
                if (this[i, j] == t1 && !includeSelf) continue;
                switch (dt)
                {
                    case DistanceType.Square:
                        if (GetTileDistanceMoves(t1, this[i, j]) <= (int)distance) lTiles.Add(this[i, j]);
                        break;
                    case DistanceType.Air:
                        if (GetTileDistanceAir(t1, this[i, j]) <= distance) lTiles.Add(this[i, j]);
                        break;
                    case DistanceType.Manhattan:
                        if (GetTileDistanceManhattan(t1, this[i, j]) <= (int)distance) lTiles.Add(this[i, j]);
                        break;
                    case DistanceType.Travel:
                        if (GetTileDistanceTravel(t1, this[i, j]) <= (int)distance) lTiles.Add(this[i, j]);
                        break;
                    case DistanceType.MovesDirect:
                        if (GetTileDistanceMovesDirect(t1, this[i, j]) <= (int)distance) lTiles.Add(this[i, j]);
                        break;
                    default:
                        Debug.Assert(false);
                        break;
                }

            }
        }
        return lTiles;
    }
    public int GetTileDistanceManhattan(TerrainTile t1, TerrainTile t2)
    {
        return (Mathf.Abs(t1.x - t2.x) + Mathf.Abs(t1.y - t2.y));
    }
    public int GetTileDistanceMoves(TerrainTile t1, TerrainTile t2)
    {
        return (Mathf.Max(Mathf.Abs(t1.x - t2.x), Mathf.Abs(t1.y - t2.y)));
    }
    public float GetTileDistanceAir(TerrainTile t1, TerrainTile t2)
    {
        return Mathf.Sqrt((t1.x - t2.x) * (t1.x - t2.x) + (t1.y - t2.y) * (t1.y - t2.y));
    }
    public int GetTileDistanceTravel(TerrainTile t1, TerrainTile t2)
    {
        return 0;
    }
    /// <summary>
    /// Tries diagnoal move (cost 1.5) and then perpendicular.
    /// </summary>
    public float GetTileDistanceMovesDirect(TerrainTile t1, TerrainTile t2)
    {
        int i = Mathf.Abs(t1.x - t2.x);
        int j = Mathf.Abs(t1.y - t2.y);
        float ret = Mathf.Min(i, j) * Defines.def.gameplaySettings.diagonalTirednessModifer; // diagonal move
        ret += Mathf.Abs(i - j); // perpendicualr move
        return ret;
    }

    public List<TerrainTile> GetDiagonalTilesInDirection(TerrainTile fromTile, NeighbourTile direction, int distance, bool includeSelf = true)
    {
        List<TerrainTile> ret = new List<TerrainTile>();
        int i = fromTile.x;
        int j = fromTile.y;
        TerrainTile t = this[i, j];
        if (includeSelf)
        {
            ret.Add(t);
        }
        if (direction == NeighbourTile.Self) return ret;
        t = GetNeighbouringTile(t, direction);
        int dist = 1;
        while (t != null && dist <= distance)
        {
            ret.Add(t);
            t = GetNeighbouringTile(t, direction);
            dist++;
        }
        return ret;
    }
    public NeighbourTile GetDirection(TerrainTile from, TerrainTile to)
    {
        if (from == to) return NeighbourTile.Self;
        int dx = to.x - from.x;
        int dy = to.y - from.y;
        if (Mathf.Abs(dx) != Mathf.Abs(dy) && dx!=0 && dy!=0) return (NeighbourTile)(-1); // only diagonal and axis directions are allowed
        if (dx > 0)
        {
            if (dy > 0) return NeighbourTile.TopRight;
            if (dy == 0) return NeighbourTile.Right;
            return NeighbourTile.BottomRight;
        }
        else if (dx == 0)
        {
            if (dy > 0) return NeighbourTile.Top;
            //if (dy == 0) return NeighbourTile.Self;
            return NeighbourTile.Bottom;
        }
        else
        {
            if (dy > 0) return NeighbourTile.TopLeft;
            if (dy == 0) return NeighbourTile.Left;
            return NeighbourTile.BottomLeft;
        }
    }
    public NeighbourTile GetOppositeDirection(NeighbourTile dir)
    {
        switch (dir)
        {
            case NeighbourTile.Self: return NeighbourTile.Self;
            case NeighbourTile.TopLeft: return NeighbourTile.BottomRight;
            case NeighbourTile.Left: return NeighbourTile.Right;
            case NeighbourTile.BottomLeft: return NeighbourTile.TopRight;
            case NeighbourTile.Top: return NeighbourTile.Bottom;
            case NeighbourTile.Bottom: return NeighbourTile.Top;
            case NeighbourTile.TopRight: return NeighbourTile.BottomLeft;
            case NeighbourTile.Right: return NeighbourTile.Left;
            case NeighbourTile.BottomRight: return NeighbourTile.TopLeft;
        }
        return (NeighbourTile)(-1);
    }
    #endregion

    /// <summary>
    /// Get real bounds of tilemap, including only non null tiles.
    /// This was written since I don't know how to compact the tilemap to actual bounds.
    /// </summary>
    /// <returns></returns>
    public static BoundsInt GetBoundsUsed(Tilemap tilemap)
    {
        int xMax = 0, xMin = 0, yMax = 0, yMin = 0;
        bool inited = false;
        BoundsInt bi = tilemap.cellBounds;
        for (int i = bi.xMin; i < bi.xMax; i++)
            for (int j = bi.yMin; j < bi.yMax; j++)
            {
                Vector3Int pos = new Vector3Int(i, j, 0);
                TileBase tb = tilemap.GetTile(pos);
                if (tb != null)
                {
                    if (!inited)
                    {
                        inited = true;
                        xMax = pos.x;
                        xMin = pos.x;
                        yMax = pos.y;
                        yMin = pos.y;
                    }
                    else
                    {
                        xMax = Mathf.Max(pos.x, xMax);
                        xMin = Mathf.Min(pos.x, xMin);
                        yMax = Mathf.Max(pos.y, yMax);
                        yMin = Mathf.Min(pos.y, yMin);
                    }
                }
            }
        BoundsInt biReal = new BoundsInt(xMin, yMin, 0, xMax - xMin + 1, yMax - yMin + 1, 0);
        return biReal;
    }

    /// <summary>
    /// Get a rectangle of tiles, ordered so 2 for loops can be run on it like on normal map (untested)
    /// //TODO return class with rect coords also, and maybe with ability to run function on tile
    /// </summary>
    public List<TerrainTile> GetRectTiles(int left, int bottom, int width, int height) {

        int right = Mathf.Min(left + width, xTiles);
        int top = Mathf.Min(bottom + height, yTiles);
        List<TerrainTile> ret = new List<TerrainTile>();
        for (int i = left; i < right; i++)
        {
            for (int j = bottom; j < top; j++)
            {
                ret.Add(this[i,j]);
            }
        }
        return ret;
    }

    public static System.Array Directions()
    {
        return System.Enum.GetValues(typeof(NeighbourTile));
    }

    // helpers
    TerrainTile RandomTile()
    {
        int i = Random.Range(0, xTiles - 1);
        int j = Random.Range(0, yTiles - 1);
        return this[i, j];
    }
    TerrainTile RandomTileExcept(TerrainTile t)
    {
        int i, j;
        do
        {
            i = Random.Range(0, xTiles - 1);
            j = Random.Range(0, yTiles - 1);
        } while (i == t.x && j == t.y);
        return this[i, j];
    }
    TerrainTile RandomFreeTileExcept(TerrainTile t)
    {
        int i, j;
        do
        {
            i = Random.Range(0, xTiles - 1);
            j = Random.Range(0, yTiles - 1);
        } while (this[i, j].GetUnits().Count > 0 || i == t.x && j == t.y);
        return this[i, j];
    }

    public TerrainTile GetNextTileTowards(TerrainTile from, TerrainTile to)
    {
        int xoffset = 0;
        int yoffset = 0;
        if (from.x > to.x) xoffset = -1;
        if (from.x < to.x) xoffset = 1;
        if (from.y > to.y) yoffset = -1;
        if (from.y < to.y) yoffset = 1;

        return this[from.x + xoffset, from.y + yoffset];
    }

}
