﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// Has coordinates, representation, can't move and holds units. 
/// Reveal status provided by player knol map.
/// </summary>
[System.Serializable]
public class TerrainTile {

    // link to mb
    /// <summary>
    /// Dependency injection link to MonoBheavior
    /// </summary>
    public ITerrainTileMB mb;

    // globals
    //public static int[] tirednessPerType = null; // = { 10, 30, 60 }; // starting with meadow. //TODO make it per level, take values from some object on level, if present
    //public static int[] maxGainedManaPerType = null;
    //public static int[] gainedManaModifierPerSettType = null;
    public LevelState ls;
    public int x;
    public int y;
    public TerrainType type;

    // units on tile
    [NonSerialized] public List<Unit> units;  //TODO set to null for finished minmax snapshots to save space
    [NonSerialized] public Settlement settlement; 

    // temp data
    public int tempManaCost;

    public TerrainTile()
    {
        units = new List<Unit>();
        settlement = null;
    }
    /// <summary>
    /// For AI and misc use only. Must call Init to finish initialization.
    /// </summary>
    public TerrainTile(TerrainTile source)
    {
        x = source.x;
        y = source.y;
        type = source.type;
        units = new List<Unit>();
        mb = TerrainTileMB.terrainTileMBDummy as ITerrainTileMB;
    }

    public void Init(LevelState ls)
    {
        this.ls = ls;
    }

    public List<Buff> GetBuffs()
    {
        return ls.buffs.FindAll(x => x.tile == this);
    }

    public int GetMoveTiredness(NeighbourTile direction)
    {
        int ret = 0;
        if (direction != NeighbourTile.Self)
        {
            ret = Defines.TirednessPerTerrain(type);
        }
        return 1;
    }

    public void AddUnit(Unit u)
    {
        if (units.Contains(u))
        {
            MyHelpers.LogError(this, "unit already contained"); //TODO remove
            return;
        }
        units.Add(u);
    }

    public void RemoveUnit(Unit u)
    {
        if (units.Contains(u))
        {
            units.Remove(u);
        }
        else MyHelpers.LogError(this, "unit to be removed not on tile "); //TODO remove
    }

    public List<Unit> GetUnits() { return units; }

    public void SetSettlement(Settlement s)
    {
        if (settlement)
        {
            MyHelpers.LogError(this, "settlement already exists");
            return;
        }
        settlement = s;
    }

    public IEnumerable<Unit> LiveEnemies(Player p)
    {
        return units.Where(x => !x.killed && x.player.playerName != p.playerName);
    }
    public IEnumerable<Unit> LiveFriendlies(Player p)
    {
        return units.Where(x => !x.killed && x.player.playerName == p.playerName);
    }
    public Unit LiveEnemy(Player p)
    {
        return units.FirstOrDefault(x => !x.killed && x.player.playerName != p.playerName);
    }
    public Unit LiveFriendly(Player p)
    {
        return units.FirstOrDefault(x => !x.killed && x.player.playerName == p.playerName);
    }
    public bool AnyLiveUnits()
    {
        return units.FindAll(x => !x.killed).Count > 0;
    }
    public Unit GetUnit()
    {
        return units.Find(x => !x.killed);
    }

    public void ClearTile()
    {
        units.Clear();
    }

       /// <summary>
    /// Used only for init so far. Could be used to burn forset or flood valley etc.
    /// </summary>
    public void SetTileType(TerrainType tt)
    {
        type = tt;
        mb.NeedsUpdate();
    }
    public TerrainType GetTileType() { return type; }

    /// <summary>
    /// Only forest and mountain hide nemy units diagonally.
    /// </summary>
    /// <returns></returns>
    public bool BlocksView()
    {
        if (x == Int32.MinValue) return false;
        if (type == TerrainType.Forest || type == TerrainType.Mountain) return true;
        return false;
    }

    /// <summary>
    /// Takes into account terrain and settlement type
    /// </summary>
    /// <returns></returns>
    public int GetMaxManaGain()
    {
        //int res = maxGainedManaPerType[(int)type];
        int res = Defines.ManaPerTerrain(type);
        if (settlement)
        {
            //res *= gainedManaModifierPerSettType[(int)settlement.settlementType];
            res = (int) (res * Defines.def.gameplaySettings.settlementSpecs[(int)settlement.settlementType].manaMultiplier);
        }
        return res;
    }

    public string GetInfoText()
    {
        string ret;
        int tiredness = Defines.TirednessPerTerrain(type);
        ret = System.Enum.GetName(typeof(TerrainType), type) + "\n" +
            "Tiredness per move:\n" + tiredness + "%, (" + Mathf.Round(tiredness * Defines.sqrt2) + "% diag.)\n" +
            ((type == TerrainType.Forest) ? "(Units in forest are concealed from view from other tiles)\n" : "");
        ret += "Max mana gain per turn: " + GetMaxManaGain();

        /*Unit u = gm.selectedUnit;
        if (u != null && u.MoveIsValid(this, true)) {
            ret += "\nMove cost:\n";
            float dist = Mathf.Sqrt(Mathf.Abs(u.tile.x - this.x) + Mathf.Abs(u.tile.y - this.y));
            dist = Mathf.Round(dist * tirednessPerType[(int)type]);
            ret +=  + dist + "% tiredness\n";
            if (this.type == TileType.Forest) ret += "(You cannot see if there are enemy units in this forest unless you send recon)\n";
        }*/

        return ret;
    }

    public static implicit operator bool(TerrainTile t)
    {
        if (t != null) return true;
        return false;
    }

    public override string ToString()
    {
        return "T " + x + "," + y;
    }

    #region hiddenInformation
    /*
    // game state
    /// <summary>
    /// Reveal map set by some unit or player for all tiles (static).
    /// It is set on beggining of unit or player turn so that all references to tile.revealMap[] provide data for that player
    /// </summary>
    public static KnolMapComparable<TileRevealStatus> revealMap;
    /// <summary>
    /// Which of the existing per player reveal maps is used for local player, i.e. to display map on screen.
    /// Set once on beggining of level.
    /// /// </summary>
    public static KnolMapComparable<TileRevealStatus> revealMapOfLocalPlayer;

    /// <summary>
    /// Used only for init so far. Could be used to burn forset or flood valley etc.
    /// </summary>
    public void SetTileType(TileType tt, TileRevealStatus trs)
    {
        type = tt;
        SetRevealStatus(trs);
    }
    public TileType GetTileType() { return type; }

    /// <summary>
    /// In relation to local player. Can't go back to map, seen, if visited etc. Only to more reveal. Maybe if terrain changes...
    /// </summary>
    public void SetRevealStatus(TileRevealStatus trs)
    {
        if (revealMapOfLocalPlayer == revealMap)
        {
            //if (trs > revealMap[this]) needsUpdate = true;
            //if (!gm.fogOfWar) needsUpdate = true;
            needsUpdate = true; //TEMP
        }
        //if (trs > revealMap[this]) needsUpdate = true; // only change sprite from player viewpoint of terrain, not enemy viewpoint
        if (trs > revealMap[this]) revealMap[this] = trs;
    }
    /// <summary>
    /// In relation to local player.
    /// </summary>
    public TileRevealStatus GetRevealStatus() { return revealMap[this]; }

        
    private void SetHatchOnVisibleTiles()
    {
        // add hatch for unvisited visible tiles
        if (revealMap[this] == TileRevealStatus.Seen)
        {
            tileMapHatch.SetTile(new Vector3Int(x, y, 0), tileMapper.hatchTile);
            //MyHelpers.MyHelpers.Log(this, "tile " + x + "," + y + " is hatched, " + revealMap[this]);
        }
        else
        {
            tileMapHatch.SetTile(new Vector3Int(x, y, 0), null);
            //MyHelpers.MyHelpers.Log(this, "tile " + x + "," + y + " NOT, " + revealMap[this]);
        }
    }

    public string GetInfoText()
    {
        // beginning of method
        string ret;
        if (revealMap[this] == TileRevealStatus.Unknown)  {
            return "You know nothing of this area yet.";
        };
        ret = System.Enum.GetName(typeof(TileType), type) + "\n" +
            "Tiredness per move:\n" + tirednessPerType[(int)type] + "%, (" + Mathf.Round(tirednessPerType[(int)type]*Defines.sqrt2)+ "% diag.)\n" +
            ((type == TileType.Forest) ? "(Units in forest are concealed from view from other tiles)\n" : "") +
            ((revealMap[this] == TileRevealStatus.Visited) ? "You have visited this tile.\n" : "");

    }

    */
    #endregion
}
