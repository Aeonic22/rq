﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SettlementSpecs
{
    public SettlementType settlementType;
    [Tooltip("Percent gained")]
    public float manaMultiplier;
}

[System.Serializable]
public class TerrainSpecs
{
    public TerrainType terrainType;
    [Tooltip("Tiredness gained on enter")]
    public int tiredness;
    [Tooltip("Per turn, per unit")]
    public int manaGained;
}

[CreateAssetMenu(fileName = "GameplaySettings", menuName = "ScriptableObjects/GamplaySettings", order = 1)]
public class GameplaySettings : ScriptableObject {

    [Header("Tiredness and rest")]
    /// <summary>
    /// Maximum amount of tiredness per battle. Incurred when enemy of equal strength.
    /// </summary>
    public int tiredBattle = 100;
    /// <summary>
    /// Fraction of tiredness eliminated per turn, 0-100.
    /// </summary>
    public int tiredHunger = 40;
    /// <summary>
    /// Fraction of tiredness eliminated per turn, 0-100.
    /// </summary>/// 
    public int restPerTurn = 20;
    /// <summary>
    /// Fraction of tiredness eliminated per skipped turn, 0-100.
    /// </summary>
    public int restPerSkippedTurn = 50;
    /// <summary>
    /// How much less effective rest is in eneny village
    /// </summary>
    public float restMultiplyerEnemyVillage;
    /// <summary>
    /// How much more effective rest is in netural village
    /// </summary>
    public float restMultiplyerNeutralVillage;
    /// <summary>
    /// How much more effective rest is in friendly village
    /// </summary>
    public float restMultiplyerFriendlyVillage;
    public bool restAsMultiply = true;

    [Header("Morale")]
    /// <summary>
    /// How much morale a winner gets after battle, 0-100.
    /// </summary>
    public int battleMoraleDelta = 20;
    /// <summary>
    /// How much morale for capturing a neutral settelement
    /// </summary>
    public int captureNeutralDelta = 20;
    /// <summary>
    /// How much morale for capturing an enemy settelement
    /// </summary>
    public int captureEnemyDelta = 40;

    [Header("Spells")]


    [Header("Diagonal")]
    public float diagonalTirednessModifer = 1.5f;

    [Header("Terrain")]
    public List<TerrainSpecs> terrainSpecs;
    public List<SettlementSpecs> settlementSpecs;

    // Use this for initialization
    void OnEnable() {
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
