﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Size same as game map, represents one piece of knowledge
/// Accessible by coordinates or tile.
/// </summary>
/// <typeparam name="T">any type of knowledge, primitive or enum, or any objact in fact</typeparam>
public class KnolMap<T>
{
    public KnolMapType mapType;
    protected T[,] map;
    protected int divx, divy;

    public KnolMap(int divisionsx, int divisionsy, KnolMapType type)
    {
        divx = divisionsx;
        divy = divisionsy;
        mapType = type;
        map = new T[divx, divy];
    }
    public T this[int i, int j]
    {
        get
        {
            return map[i, j];
        }
        set
        {
            map[i, j] = value;
        }
    }
    public T this[TerrainTile t]
    {
        get
        {
            return map[t.x, t.y];
        }
        set
        {
            map[t.x, t.y] = value;
        }
    }

    public void SetAllTiles(T t)
    {
        for (int i = 0; i < divx; i++)
            for (int j = 0; j < divy; j++)
                map[i, j] = t;
    }
    /*public void SetAllTilesToDefault() // something not working
    {
        Array.Clear(map, 0, divisions * divisions);
    }*/
    public T[,] GetAllTiles() { return map; } // probably won't need
    public int Count(T value)
    {
        int count = 0;
        for (int i = 0; i < divx; i++)
            for (int j = 0; j < divy; j++)
                if (map[i, j].Equals(value)) count++;
        return count;
    }

    /// <summary>
    /// Copy constructor
    /// </summary>
    public KnolMap(KnolMap<T> pmap) {
        divx = pmap.divx;
        divy = pmap.divy;
        mapType = pmap.mapType;
        map = new T[divx, divy];

        for (int i = 0; i < divx; i++)
        {
            for (int j = 0; j < divy; j++)
            {
                map[i, j] = pmap[i, j];
            }
        }
    }
}

/// <summary>
/// Size same as game map, represents one piece of knowledge
/// Accessible by coordinates or tile.
/// In addition, this map can absorb other maps into itself (max, min, avg...)
/// </summary>
/// <typeparam name="T">any comparable type of knowledge, primitive or enum</typeparam>
public class KnolMapComparable<T> : KnolMap<T> where T : System.IComparable
{
    public KnolMapComparable(int divx, int divy, KnolMapType type) : base(divx, divy, type) { }
    
    public void UpdateMap(KnolMap<T> pmap, KnolMapOps op)
    {
        for (int i = 0; i < divx; i++)
        {
            for (int j = 0; j < divy; j++)
            {
                switch (op)
                {
                    case KnolMapOps.max:
                        map[i, j] = Max(map[i, j], pmap[i, j]);
                        break;
                    default:
                        map[i, j] = pmap[i, j];
                        break;
                }
            }
        }
    }

    public T Max(T t1, T t2)
    {
        return t1.CompareTo(t2) > 0 ? t1 : t2;
    }
}

/// <summary>
/// One memory related to enemy unit. Where, when, heading
/// </summary>
public class MemoryItemUnit //TODO replace with struct?
{
    public int unitId;  // -1 for unknown
    //public Vector2Int positionSeen;
    public TerrainTile tileSeen;
    public int turnSeen;
    public string sourceOfInfo;
    public NeighbourTile heading;
    //TODO unit seen - from ai's list of units known
    //TODO seen or known army composition

    /// <summary>
    /// Constructo.
    /// </summary>
    /// <param name="id">-1 for unknown. id's not working yet.</param>
    /// <param name="soi">who told us</param>
    /// <param name="hd">default is Self, meaning contact with unit was lost while it was holding position or we moved away from it</param>
    public MemoryItemUnit(int id, TerrainTile tileSeen, int turnSeen, string soi, NeighbourTile hd = NeighbourTile.Self)
    {
        unitId = id;
        this.tileSeen = tileSeen;
        this.turnSeen = turnSeen;
        sourceOfInfo = soi;
        heading = hd;
    }
}
/// <summary>
/// All sightings and rumors abour enemy units so far that a single entity knows about (unit or player).
/// </summary>
public class ListMemoryItemUnit
{
    static TurnManager tm = (TurnManager)GameObject.FindObjectOfType(typeof(TurnManager));

    List<MemoryItemUnit> miList;

    public ListMemoryItemUnit()
    {
        miList = new List<MemoryItemUnit>();
    }
    public void Add(MemoryItemUnit mi)
    {
        // since it will be added chronologically, no sort is needed
        miList.Add(mi);
    }
    /// <summary>
    /// Get last sighting of a specified unit
    /// </summary>
    public MemoryItemUnit GetLastFor(int unitId)
    {
        return miList.FindLast(x => x.unitId == unitId);
    }
    /// <summary>
    /// Get last sighting of every known unit.
    /// </summary>
    public List<MemoryItemUnit> GetLastForEveryUnit()
    {
        List<MemoryItemUnit> ret = new List<MemoryItemUnit>();
        foreach (MemoryItemUnit mi in miList)
        {
            int index = ret.FindLastIndex(x => x.unitId == mi.unitId);
            if (index < 0) // first entry
            {
                ret.Add(mi);
            }
            else
            {
                ret[index] = mi;
            }
        }
        return ret;
    }
    /// <summary>
    /// Erase but last memory of every unit.
    /// </summary>
    public void LeaveJustLast()
    {
        //TODO
        MyHelpers.LogError(this, "not impl");
    }
    /// <summary>
    /// Forget older memories.
    /// </summary>
    /// <param name="numTurns">1 turn means leave memories of last turn, erase others. 0 is current turn</param>
    public void ForgetNumTurnsBack(int numTurns = 10)
    {
        foreach (MemoryItemUnit mi in miList)
        {
            if (tm.currentTurn - mi.turnSeen > 10)
                miList.Remove(mi);
        }
    }
}



// get unit values 
public class Battle
{
    // all data for battle
    public Unit attacker, defender;
    public TerrainTile fromTile, toTile;
    public bool predict;
    public bool ambush;
    public bool attackerIsPlayer;
    public bool singleLocalPlayer;

    public static int defenderWins = -1;
    public static int attackerWins = 1;
    public static int bothLose = 0;

    public static int playerWins = 1;
    public static int opponentWins = -1;

    public float attackerStrength;
    public float defenderStrength;

    public BattleResults br;

    public Battle(Unit attacker, Unit defender, TerrainTile fromTile, TerrainTile toTile, bool predict = false, bool ambush = false)
    {
        this.attacker = attacker;
        this.defender = defender;
        this.fromTile = fromTile;
        this.toTile = toTile;
        this.ambush = ambush;
        this.predict = predict;
        attackerIsPlayer = attacker.player.playerType == PlayerType.local;
        singleLocalPlayer = attacker.player.playerType == PlayerType.local && defender.player.playerType != PlayerType.local
            || attacker.player.playerType != PlayerType.local && defender.player.playerType == PlayerType.local;
    }

    /// <summary>
    /// Determine winner of battle with all parameters, magic etc...
    /// </summary>
    /// <returns>-1 if defender wins, 0 if both lose, 1 if attacker wins</returns>
    public BattleResults DetermineResult()
    {
        br = new BattleResults { battle = this };

        // winner //TODO random winner if same strength?
        if (predict) attackerStrength = attacker.GetStrengthActualAfterMove(toTile);
        else attackerStrength = attacker.GetStrengthActual();
        defenderStrength = defender.GetStrengthActual();
        if (attackerStrength > defenderStrength) br.winner = attackerWins;
        else if (attackerStrength < defenderStrength) br.winner = defenderWins;
        else br.winner = bothLose;

        //TODO for hoteseat, don't write you win but player wins, always positive picture
        int winnerTemp = br.winner;
        if (!attackerIsPlayer) winnerTemp *= -1;
        if (singleLocalPlayer)
        {
            if (winnerTemp == 1) br.singleLocalPlayerWins = true;
            else br.singleLocalPlayerWins = false;
        }

        float tired;

        // casualties
        if (br.winner == attackerWins)
        {
            br.defenderCasualties = defender.count;
            br.attackerCasualties = (int) (attacker.count * defenderStrength / attackerStrength);
            tired = defenderStrength / attackerStrength;
        }
        else if (br.winner == defenderWins)
        {
            br.attackerCasualties = attacker.count;
            br.defenderCasualties = (int) (defender.count * attackerStrength / defenderStrength);
            tired = attackerStrength / defenderStrength;
        }
        else
        {
            br.defenderCasualties = defender.count;
            br.attackerCasualties = attacker.count;
            tired = 1;
        }

        // stats
        int t = (int)Mathf.Lerp(0f, Defines.def.gameplaySettings.tiredBattle, tired);
        br.winnerTirednessDelta = (t / 20 + 1) * 20; // snap to increments of 20
        br.winnerMoraleDelta = Defines.def.gameplaySettings.battleMoraleDelta;

        return br;
    }

    // HELPERS 

    /// <summary>
    /// 1 for attacker win, -1 for defender win
    /// </summary>
    public static int DetermineWinner(Unit attacker, Unit defender, TerrainTile fromTile, TerrainTile toTile, bool predict = false, bool ambush = false)
    {
        Battle battle = new Battle(attacker, defender, fromTile, toTile, predict, ambush);
        BattleResults br = battle.DetermineResult();
        return br.winner;
    }

    public bool WinnerIsPlayer()
    {
        if (br.winner > 0 && attackerIsPlayer) return true;
        if (br.winner < 0 && !attackerIsPlayer) return true;
        return false;
    }

    /// <summary>
    /// Called after battle has been resolved. Kills units, notifies...
    /// </summary>
    public void Cleanup()
    {
        if (attacker.count < br.attackerCasualties || defender.count < br.defenderCasualties)
        {
            MyHelpers.Log(this, "Unit count after battle less than 0.");
            int breakHere = 0;
        }

        if (br.winner > 0)
        { // attacker win
            defender.SetKilled();
            attacker.count -= br.attackerCasualties;
            //attacker.tiredness += br.winnerTirednessDelta;
            attacker.AddTiredness(br.winnerTirednessDelta);
            attacker.mb.SetUnitGui();
        }
        else if (br.winner == 0)
        {
            defender.SetKilled();
            attacker.SetKilled();
            attacker.count = defender.count = 0;
        }
        else // def win
        {
            attacker.SetKilled();
            defender.count -= br.defenderCasualties;
            //defender.tiredness += br.winnerTirednessDelta;
            defender.AddTiredness(br.winnerTirednessDelta);
            defender.mb.SetUnitGui();
        }

        // wrap up
        attacker.attacking = false;
        br.resolved = true;

        MyHelpers.Log(this, "Battle resolved at " + toTile.ToString());
    }

}

/// <summary>
/// Contained within battle class.
/// </summary>
public struct BattleResults
{
    public Battle battle;
    public bool resolved;
    /// <summary>
    /// -1 for defeneder, 1 for attacker, 0 for mutual destruction
    /// </summary>
    public int winner;
    public bool singleLocalPlayerWins;
    /// <summary>
    /// did the defeated surrender and stopped further battle
    /// </summary>
    public bool surrender;
    public int attackerCasualties, defenderCasualties; // rest of defeated troops are disbanded
    public int winnerTirednessDelta, winnerMoraleDelta;

    public Unit GetWinner() {
        if (winner > 0) return battle.attacker;
        else if (winner < 0) return battle.defender;
        else return null;
    }

    public override string ToString()
    {
        string ret = "";
        if (singleLocalPlayerWins) ret = "YOU WIN!\n";
        else ret = "YOU LOSE!\n";

        //TODO surrender

        string strAttackerCasualties = attackerCasualties + ((attackerCasualties == battle.attacker.count) ? " (Annihilated)\n" : "\n");
        string strDefenderCasualties = defenderCasualties + ((defenderCasualties == battle.defender.count) ? " (Annihilated)\n" : "\n");
        ret += "Your casualties: " + (battle.attackerIsPlayer ? strAttackerCasualties : strDefenderCasualties);
        ret += "Enemy casualties: " + (battle.attackerIsPlayer ? strDefenderCasualties : strAttackerCasualties) +  "\n";

        return ret;
    }
}



static class MyHelpers
{
    public static void Start()
    {

    }

    public static Transform FindInChildren(Transform parent, string name)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform c = parent.GetChild(i);
            if (c.name == name) return c;
            Transform result = FindInChildren(c, name);
            if (result != null) return result;
        }
        return null;
    }

    private static string GetLogPrefix(object source)
    {
        string prefix = "<color=red>must set prefix: </color>";
        if (source == null) // for always displayed messages
        {
            return " ";
        }
        if (source.GetType() == typeof(GameMain))
        {
            if (Defines.def.logSettings.mono) prefix = "<color=gray>-gm:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(LevelState))
        {
            if (Defines.def.logSettings.levelState) prefix = "<color=gray>Player:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(Player))
        {
            if (Defines.def.logSettings.ai) prefix = "<color=gray>Player:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(PlayerAI))
        {
            if (Defines.def.logSettings.ai) prefix = "<color=purple>AI:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(PlayerMB))
        {
            if (Defines.def.logSettings.mono) prefix = "<color=gray>PlayerMB:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(TerrainTile))
        {
            if (Defines.def.logSettings.terrain) prefix = "<color=#008800ff>tTile:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(TerrainTileMB))
        {
            if (Defines.def.logSettings.mono) prefix = "<color=#008800ff>tTileMB:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(Unit))
        {
            if (Defines.def.logSettings.units) prefix = "<color=blue>Unit:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(UnitMB))
        {
            if (Defines.def.logSettings.mono) prefix = "<color=blue>UnitMB:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(Terrain)) {
            if (Defines.def.logSettings.terrain) prefix = "<color=#008800ff>Terrain:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(TerrainGenerator)) {
            if (Defines.def.logSettings.terrain) prefix = "<color=#008800ff>TerrGen:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(Settlement)) {
            if (Defines.def.logSettings.units) prefix = "<color=#008800ff>Sett:</color> ";
            else prefix = "";
        }

        else if (source.GetType() == typeof(GUIController)) {
            if (Defines.def.logSettings.gui) prefix = "<color=yellow>GUICtrl:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(GUIControllerBattle))
        {
            if (Defines.def.logSettings.battle && Defines.def.logSettings.gui) prefix = "<color=yellow>GUIBattle:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(Battle))
        {
            if (Defines.def.logSettings.battle) prefix = "<color=orange>Battle:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(TurnOrderSorter))
        {
            if (Defines.def.logSettings.mono && Defines.def.logSettings.spells) prefix = "<color=yellow>TurnOrder:</color> ";
            else prefix = "";
        }

        else if (source.GetType() == typeof(BuffMB))
        {
            if (Defines.def.logSettings.mono && Defines.def.logSettings.spells) prefix = "<color=orange>Buff:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(Buff)) {
            if (Defines.def.logSettings.spells) prefix = "<color=orange>Buff:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(SpellMB))
        {
            if (Defines.def.logSettings.mono && Defines.def.logSettings.spells) prefix = "<color=orange>Buff:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(Spell))
        {
            if (Defines.def.logSettings.spells) prefix = "<color=orange>Buff:</color> ";
            else prefix = "";
        }
        else if (source.GetType() == typeof(SpellTargetTile))
        {
            if (Defines.def.logSettings.mono && Defines.def.logSettings.spells) prefix = "<color=orange>Buff:</color> ";
            else prefix = "";
        }


        return prefix;
    }
    public static void Log(object source, object message)
    {
        string prefix = GetLogPrefix(source);
        if (prefix.Length == 0) return;
        Debug.Log(prefix + (string)message);
    }
    public static void LogColor(object source, string color, object message)
    {
        string prefix = GetLogPrefix(source);
        if (prefix.Length == 0) return;
        Debug.Log(prefix + "<color=" + color + ">" + (string)message + "</color>");
    }
    public static void LogError(object source, object message)
    {
        string prefix = GetLogPrefix(source);
        if (prefix.Length == 0) return;
        Debug.LogError(prefix + (string)message);
    }

}
