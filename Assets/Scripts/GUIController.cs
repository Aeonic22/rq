﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GUIController : MonoBehaviour, IGUIController {
    //TODO init with board size and remove gm ref

    public static GUIControllerDummy gUIControllerDummy;

    // camera position
    [HideInInspector] public Vector3 cameraOffset;

    // UI state
    //public bool playerSelected;
    bool fogOfWar = false; // when units are moving block the UI
    public UnitMB selectedUnit;
    
    public Notification uiNextTurnText;
    public Notification uiBriefNotification;
    //HI public GameObject lineRendererVisibility;
    public GameObject unitSelectedMark;

    // cached
    private GameMain gm;
    public Text textUnitInfo;
    public Text textSelectedUnitInfo;
    public Text textTileInfo;
    public Text textGameInfo;
    public Text textSettlementInfo;
    public Text textFow;
    public GameObject panelBlockUI;
    public Text textAlignment;
    public GameObject unitGuiPrefab;
    public GameObject unitsGuiFolder;

    public Text textPlayerTurn;
    public GameObject panelGameOver;

    [Header("Controls")]
    public GameObject controlsSettlement;
    public Button buttonCapture;
    public Button buttonFood1;
    public Button buttonFood10;



    [Header("UI edges")]
    public RectTransform topPanel;
    public RectTransform bottomPanel;
    public RectTransform leftPanel;
    public RectTransform rightPanel;

    private float pixelToWorldRatio;

    private float uwTotal = 1920;
    private float uwLeft;
    private float uwMiddle;
    private float uwRight;

    private float wTotal;
    private float wLeft;
    private float wLeftPadding;
    private float wMiddle;
    private float wRight;
    private float wRightPadding;

    private float uhTotal = 1080;
    private float uhTop;
    private float uhMiddle;
    private float uhBottom;

    private float hTotal;
    private float hTopPadding;
    private float hTop;
    private float hMiddle;
    private float hBottom;
    private float hBottomPadding; // =hTopPadding by default

    static GUIController()
    {
        gUIControllerDummy = new GUIControllerDummy();
    }

    // Use this for initialization
    void Start() {
        gm = FindObjectOfType<GameMain>();
        unitsGuiFolder = GameObject.Find(Defines.unitGuisFolderName);

        if (fogOfWar) textFow.text = "Fog of War is ON";
        else textFow.text = "Fog of War is OFF";

        BlockUI(false);

        panelGameOver.SetActive(false);
        //unitSelectedMark.transform.localScale = new Vector3(tileScale, tileScale, 1f);

        uhTop = topPanel.sizeDelta.y;
        uhBottom = bottomPanel.sizeDelta.y;
        float uhLevel = uhTotal - uhTop - uhBottom;

        uwLeft = leftPanel.sizeDelta.x;
        uwRight = rightPanel.sizeDelta.x;
        float uwLevel = uwTotal - uwLeft - uwRight;
        float uiLevelAspect = uwLevel / uhLevel;
        float levelAspect = gm.xTiles / (float) gm.yTiles;

        if (uiLevelAspect > levelAspect)
        {
            SetCameraPositionForTallLevels();
        }
        else
        {
            SetCameraPositionForWideLevels();
        }
        //uiNextTurnText.enabled = false;
        //uiBriefNotification.enabled = false;
        uiNextTurnText.GetComponent<Text>().enabled = false;
        uiBriefNotification.GetComponent<Text>().enabled = false;

        // add unit guis on all units that don't have one
        // take all units, for their mb, if unit gui is null, instance one
        // correctly position unit gui in x,y,z
        foreach (UnitMB unit in FindObjectsOfType<UnitMB>())
        {
            if (!unit.unitGui)
            {
                unit.unitGui = Instantiate(unitGuiPrefab, 
                    Camera.main.WorldToScreenPoint(unit.transform.position), 
                    Quaternion.identity, unitsGuiFolder.transform);
            }
            else // make sure position & scale is ok
            {
                unit.unitGui.transform.position = Camera.main.WorldToScreenPoint(unit.transform.position);
                
            }
            unit.unitGui.transform.localScale = new Vector3(0.01f / pixelToWorldRatio, 0.01f / pixelToWorldRatio, 1.0f);
            unit.SetUnitGui();
        }
    }

    // Update is called once per frame
    void Update() {
    }

    private void SetCameraPositionForTallLevels()
    {
        uhMiddle = uhTotal - uhTop - uhBottom;
        hMiddle = gm.yTiles;
        pixelToWorldRatio = hMiddle / uhMiddle;
        
        hTop = uhBottom * pixelToWorldRatio;
        hBottom = uhBottom * pixelToWorldRatio;
        hTotal = uhTotal * pixelToWorldRatio;

        wTotal = uwTotal * pixelToWorldRatio;
        wLeft = uwLeft * pixelToWorldRatio;
        wRight = uwRight * pixelToWorldRatio;
        wMiddle = gm.xTiles;
        wLeftPadding = wRightPadding = (wTotal - wLeft - wRight - wMiddle) / 2;

        Camera.main.orthographicSize = hTotal / 2;
        Camera.main.transform.position = new Vector3(
            wTotal / 2 - wLeft - wLeftPadding,
            hTotal / 2 - hBottom,
            Defines.def.miscGameSettings.zPosCamera);
    }

    private void SetCameraPositionForWideLevels()
    {
        uwMiddle = uwTotal - uwLeft - uwRight;

        wMiddle = gm.xTiles;
        pixelToWorldRatio = wMiddle / uwMiddle;

        wTotal = uwTotal * pixelToWorldRatio;
        wLeft = uwLeft * pixelToWorldRatio;
        wRight = wTotal - wLeft - wMiddle;

        hTotal = uhTotal * pixelToWorldRatio;
        hBottom = uhBottom * pixelToWorldRatio;
        hTop = uhTop * pixelToWorldRatio;
        hMiddle = gm.yTiles;
        hBottomPadding = hTopPadding = (hTotal - hMiddle - hTop - hBottom) / 2;

        Camera.main.orthographicSize = hTotal / 2;
        Camera.main.transform.position = new Vector3(
            wTotal / 2 - wLeft,
            hTotal / 2 - hBottom - hBottomPadding,
            Defines.def.miscGameSettings.zPosCamera);
    }

    public void NotifyNextTurn()
    {
        uiNextTurnText.ShowNotification("Next turn");
    }
    public void BriefNotify(string message)
    {
        uiBriefNotification.ShowNotification(message);
    }
    public void NotifyGameWon()
    {
        panelGameOver.SetActive(true);
        string s = "";
        if (gm.ls.killerPlayer.playerType == PlayerType.AI // AI alwas has elimination victory option
            || gm.lset.eliminationVictory && gm.ls.killerPlayer != null)
        {
            s = "Level complete by elimination!\n"
                + gm.ls.killerPlayer.playerName + " has won the game!";
        }
        else if (gm.lset.conquestVictory && gm.ls.conqueringPlayer != gm.ls.GetNeutralPlayer())
        {
            s = "Level complete by conquest!\n"
                + gm.ls.conqueringPlayer.playerName + " has won the game!";
        }
        else
        {
            s = "It's a tie!\n";
        }
        panelGameOver.transform.Find("Text").GetComponent<Text>().text = s;
    }
    public void SetActivePlayer(Player p)
    {
        Color c = (p.mb as PlayerMB).GetPlayerColor();
        textPlayerTurn.color = c;
        textPlayerTurn.text = p.playerName + "'s turn";
    }
    public bool IsAnimating()
    {
        //if (uiNextTurnText.enabled) return true;
        if (uiNextTurnText.IsAnimating()) return true;
        return false;
    }

    public void SetUnitInfoText(string text)
    {
        if (text == null) textUnitInfo.text = "hover over unit to get info";
        else textUnitInfo.text = text;
    }
    public void SetSelectedUnitInfoText(bool empty = false)
    {
        Unit u = gm.ls.currentUnit;
        if (u == null || empty == true) textSelectedUnitInfo.text = "No unit selected.";
        textSelectedUnitInfo.text = u.GetInfoText() + "\n\n" + u.tile.GetInfoText()
                        + (u.tile.settlement != null ? u.tile.settlement.GetInfoText() : "");
    }
    public void SetTileInfoText(string text)
    {
        if (text == null) textTileInfo.text = "hover over tile to get info";
        else textTileInfo.text = text;
    }
    public void SetGameInfoText(string text)
    {
        textGameInfo.text = text;
    }
    public void SetSettlementInfoText(string text)
    {
        if (text == null) textSettlementInfo.text = "hover over settlement tile to get info";
        else textSettlementInfo.text = text;
    }
    public void SetSelectionMarker(Vector2 pos)
    {
        MyHelpers.Log(this, "SetSelectionMarker " + pos.ToString());
        unitSelectedMark.transform.position = new Vector3(pos.x, pos.y, unitSelectedMark.transform.position.z);
        unitSelectedMark.SetActive(true);
    }
    public void SetSelectionMarker(UnitMB unit)
    {
        MyHelpers.Log(this, "SetSelectionMarker " + unit.ToString());
        unitSelectedMark.transform.position =
            new Vector3(unit.transform.position.x, unit.transform.position.y, unitSelectedMark.transform.position.z);
        unitSelectedMark.SetActive(true);
    }
    public void DisableSelectionMarker()
    {
        unitSelectedMark.SetActive(false);
    }

    public bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        /*if (results.Count > 0 )
        {
            int bre = 1;
        }*/
        return results.Count > 0;
    }

    public void BlockUI(bool onoff)
    {
        panelBlockUI.SetActive(onoff);
    }

    public void SetSettlementControls(Unit u, Settlement s)
    {
        // all 
        if (u == null || s == null) {
            controlsSettlement.SetActive(false);
            return;
        }
        controlsSettlement.SetActive(true);
        MyHelpers.Log(this, "SetSettlementControls" + u + ", " + s);

        // capture
        bool captured = u.player == s.alignment;
        buttonCapture.onClick.RemoveAllListeners();
        if (!captured)
        {
            buttonCapture.gameObject.SetActive(true);
            buttonCapture.onClick.AddListener(delegate { u.Capture(); });
        }
        else buttonCapture.gameObject.SetActive(false);

        // food 
        buttonFood1.onClick.RemoveAllListeners();
        buttonFood10.onClick.RemoveAllListeners();
        if (s.food <= 0)
        {
            buttonFood1.gameObject.transform.Find("Text").GetComponent<Text>().color = new Color(1, 1, 1, 0.1f);
            buttonFood1.enabled = false;
        }
        else
        {
            buttonFood1.gameObject.transform.Find("Text").GetComponent<Text>().color = new Color(1, 1, 1, 1f);
            buttonFood1.enabled = true;
            buttonFood1.onClick.AddListener(delegate { u.GetFood(1); });
        }

        if (s.food <= 0)
        {
            buttonFood10.gameObject.transform.Find("Text").GetComponent<Text>().color = new Color(1, 1, 1, 0.1f);
            buttonFood10.enabled = false;
        }
        else
        {
            buttonFood10.gameObject.transform.Find("Text").GetComponent<Text>().color = new Color(1, 1, 1, 1f);
            buttonFood10.enabled = true;
            buttonFood10.onClick.AddListener(delegate { u.GetFood(10); });
        }

    }

    /// <summary>
    /// Produces and displays text on UI
    /// </summary>
    public void UpdateLevelAlignment()
    {
        string s = "Level alignment";
        if (gm.ls.settlements.Count == 0)
        {
            s += "\nNo settlements on level";
        }
        else
        {
            for (int i = 0; i < gm.ls.players.Count; i++)
            {
                Player p = gm.ls.players[i];
                if (p == gm.ls.GetNeutralPlayer()) continue;
                s += "\n<color=" + p.mb.GetPlayerColorAsHex() + ">" 
                    + p.playerName + " - " + gm.ls.levelAlignment[i] + "%</color>";
            }
        }
        MyHelpers.Log(this, s);
        textAlignment.text = s;
    }

}

public interface IGUIController
{
    void NotifyNextTurn();
    void BriefNotify(string message);
    void SetActivePlayer(Player p);
    void SetUnitInfoText(string text);
    void SetSelectedUnitInfoText(bool empty = false);
    void SetTileInfoText(string text);
    void SetGameInfoText(string text);
    void SetSettlementInfoText(string text);
    void SetSelectionMarker(Vector2 pos);
    void SetSelectionMarker(UnitMB unit);
    void DisableSelectionMarker();
    bool IsPointerOverUIObject();
    void BlockUI(bool onoff);
    bool IsAnimating();
    void SetSettlementControls(Unit u, Settlement s);
    void UpdateLevelAlignment();
    void NotifyGameWon();
}


public class GUIControllerDummy : IGUIController
{
    public void NotifyNextTurn() { }
    public void BriefNotify(string message) { }
    public void SetActivePlayer(Player p) { }
    public void SetUnitInfoText(string text) { }
    public void SetSelectedUnitInfoText(bool empty = false) { }
    public void SetTileInfoText(string text) { }
    public void SetGameInfoText(string text) { }
    public void SetSettlementInfoText(string text) { }
    public void SetSelectionMarker(Vector2 pos) { }
    public void SetSelectionMarker(UnitMB unit) { }
    public void DisableSelectionMarker() { }
    public bool IsPointerOverUIObject() { return false; }
    public void BlockUI(bool onoff) { }
    public bool IsAnimating() { return false; }
    public void SetSettlementControls(Unit u, Settlement s) { }
    public void UpdateLevelAlignment() { }
    public void NotifyGameWon() { }

}
