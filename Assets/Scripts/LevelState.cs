﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Holds links to all core game objects such as terrain, units, players, turn manager...
/// Can clone itself and play the game just by API calls.
/// Has a LevelStateMB for custom display in inspector?
/// </summary>
/// <remarks>
/// After every StartCoroutine there is return. After the coroutine is done, it will change LevelState
/// In AI game, method is just called while depth > x, and depth is a parameter of LevelState
        /*
         * If in battle
         *      If resolved, wait for ok
         *      If prebattle, select AI spell, wait for player spell and OK
         *      If resolved and OK, show debrief
         * If in town
         *      ...
         * If animation
         *      continue
         * If unit is selected 
         *      has moves, allow to move
         *      spent moves or skipped turn, proceed to next unit
         *      last unit, proceed to magic phase
         * If magic phase
         *      select AI spells
         *      wait for player spells and OK
         *      display spells
         *      move to next turn
         * 
         * ILI
         * 
         * everything has a GameState, on every change a new GameState object is created and assigned
         * GameState initiates AI players (all for minmax) and waits for main player by looking at variables
         * each game state has in its interface method to determine whether it should run, its sort order
         *  and can determine when it is finished or can initiate other state.
         *//// 
/// </remarks>
public class LevelState : ILevelState, IUnitContainer
{

    // terrain
    [NonSerialized] public Terrain tiles;

    // units
    [NonSerialized] public List<Unit> units;
    [NonSerialized] public List<Settlement> settlements;

    //other objects
    [NonSerialized] public List<Buff> buffs;
    public IGUIController gui;
    public IGUIControllerBattle guiBattle;

    // turn management
    public int currentTurn;
    public TurnOrderType turnOrderType;
    public List<Player> players;
    [NonSerialized] public List<Unit> turnOrderCurrent;
    [NonSerialized] public List<Unit> turnOrderNext;
    int currentUnitIndex;
    private bool currentUnitStartedTurn;
    // clicked unit or current unit in per unit turns
    public Unit currentUnit
    {
        get { return turnOrderCurrent[currentUnitIndex]; }
    }
    public int currentPlayerIndex;
    public bool processWait;
    public Battle battle;
    TurnOrderSorter comp;
    /// <summary>
    /// 0-100, 100 being human player
    /// </summary>
    public int[] levelAlignment;

    // minmax
    public int minmaxDepth;
    public bool gameWon;
    public Player conqueringPlayer;
    public Player killerPlayer;

    public static GameMain gm;
    public SpellDevelopment spellDev;

    /// <summary>
    /// Get every reference from creator
    /// </summary>
    public void Start(Terrain tiles, List<Unit> units, List<Player> players,
           GameMain gm, SpellDevelopment spellDev, IGUIController gui, IGUIControllerBattle guiBattle)
    {
        this.tiles = tiles;
        this.units = new List<Unit>();
        this.settlements = new List<Settlement>();
        this.buffs = new List<Buff>();
        LevelState.gm = gm;
        this.spellDev = spellDev;
        this.gui = gui;
        this.guiBattle = guiBattle;
        this.players = players;

        turnOrderType = TurnOrderType.PerUnit;
        currentTurn = 1;
        comp = new TurnOrderSorter();
        // even if level has no neutral settlements, they can become neutral and we need neutral player
        if (players.Find(x => x.playerName == Defines.neutralPlayerName) == null) {
            CreateNeutralPlayer();
        }
        gameWon = false;

        levelAlignment = new int[players.Count];
        gui.SetSettlementControls(null, null);
    }

    /// <summary>
    /// 50% from settlements. 50% from armies?
    /// </summary>
    /// <returns></returns>
    public void UpdateLeveleAlignment()
    {
        if (settlements.Count == 0)
        {
            gui.UpdateLevelAlignment(); return;
        }
        levelAlignment = new int[players.Count];
        int folks = 0;
        foreach (Settlement s in settlements)
        {
            levelAlignment[s.alignment.GetIndex()] += s.countFolks;
            folks += s.countFolks;
        }
        
        // calc align. neutral settlement is 50/50 between players.
        int neutralAlign = levelAlignment[GetNeutralPlayer().GetIndex()]/(players.Count-1);
        int maxLevelAlignment = 0;
        for (int i = 0; i < players.Count; i++) {
            if (players[i] == GetNeutralPlayer())
            {
                levelAlignment[i] = 0;
                continue;
            }
            levelAlignment[i] = (levelAlignment[i] + neutralAlign) * 100 / folks;
            if (levelAlignment[i] == maxLevelAlignment)
            {
                conqueringPlayer = GetNeutralPlayer();
            }
            if (levelAlignment[i] > maxLevelAlignment)
            {
                maxLevelAlignment = levelAlignment[i];
                conqueringPlayer = players[i];
            }
        }

        gui.UpdateLevelAlignment();

        // is game won?
        if (gm.lset.conquestVictory && maxLevelAlignment == 100 
            && AnyLiveFriendlies(conqueringPlayer) || !AnyLiveUnits())
        {
            gameWon = true;
            gui.NotifyGameWon();
        }
    }

    /// <summary>
    /// Must be in every game
    ///TODO add flag creation, create from prefab
    /// </summary>
    void CreateNeutralPlayer()
    {
        Player neutralPlayer = new Player();
        neutralPlayer.Init(PlayerMB.playerMBDummy, null, this);
        neutralPlayer.playerType = PlayerType.AI;
        neutralPlayer.playerName = Defines.neutralPlayerName;
        players.Add(neutralPlayer);
    }

    public Player GetNeutralPlayer()
    {
        return players.Find(x => x.playerName == Defines.neutralPlayerName);
    }

    /// <summary>
    /// Assumes 2 sides game
    /// </summary>
    public Player GetOtherPlayer(Player p)
    {
        return players.Find(x => x.playerName != Defines.neutralPlayerName && x.playerName != p.playerName);
    }

    //TODO other game board manipulation functions such as changing terrain etc.

    public LevelState() { }
    /// <summary>
    /// Creates a copy of all game state, units, tiles, with no links to any monobehavior objects.
    /// On that state functions can be called to play the game further.
    /// </summary>
    /// <returns></returns>
    public LevelState(LevelState source)
    {
        // clone self

        // clone players, ai (ai is not cloned for now since REST)
        players = new List<Player>();
        PlayerAI ai = null; // only one RESTful instance so far, //TOOD make static somewhere
        foreach (Player p in source.players)
        {
            if (p.playerAI != null)
            {
                ai = p.playerAI;
                break;
            }
        }
        foreach (Player p in source.players)
        {
            Player player = new Player(p);
            player.Init(PlayerMB.playerMBDummy as IPlayerMB, p.playerAI, this);
            player.playerType = PlayerType.AI; 
            if (player.playerAI == null)
            {
                MyHelpers.Log(this, "AI is null");
                player.playerAI = ai;
            }
            players.Add(player);
        }

        // clone tiles
        tiles = new Terrain(source.tiles);

        // clone units and settlements, connect to tiles (in Init)
        units = new List<Unit>();
        foreach (Unit u in source.units)
        {
            
            Unit unit = new Unit(u);
            unit.Init(UnitMB.unitMBDummy, u.name, gm, this,
                GetPlayerByName(u.player.playerName), tiles[u.tile.x, u.tile.y]);
        }

        turnOrderCurrent = new List<Unit>();
        foreach (Unit u in source.turnOrderCurrent)
        {
            turnOrderCurrent.Add(GetUnitByName(u.name));
        }
        turnOrderNext = new List<Unit>();
        foreach (Unit u in source.turnOrderNext)
        {
            turnOrderNext.Add(GetUnitByName(u.name));
        }

        gui = GUIController.gUIControllerDummy;
        guiBattle = GUIControllerBattle.gUIControllerBattleDummy;
        turnOrderType = source.turnOrderType;
        currentTurn = source.currentTurn;
        comp = new TurnOrderSorter();
        currentUnitIndex = source.currentUnitIndex;
        currentUnitStartedTurn = source.currentUnitStartedTurn;
        currentPlayerIndex = source.currentPlayerIndex;
        if (source.currentUnitStartedTurn && source.GetCurrentPlayer().playerType == PlayerType.local)
        {
            // if we copy state where local player turn has started, when AI takes over in minmax
            // it has to have not started turn to act.
            currentUnitStartedTurn = false;
        }

        if (battle != null)
        {
            //TODO
        }
        minmaxDepth = source.minmaxDepth;
    }

    public void Destroy()
    {

    }

    public void CleanUpDeadUnitsFromTurnOrder()
    {
        int backwards = 0;
        for (int i = turnOrderCurrent.Count-1; i >= 0; i-- )
        {
            if (turnOrderCurrent[i].killed)
            {
                turnOrderNext.Remove(turnOrderCurrent[i]); // calced after every move
                turnOrderCurrent.RemoveAt(i);
                if (i <= currentUnitIndex)
                    backwards++;
                
            }
        }
        currentUnitIndex -= backwards;
    }

    //public Player GetCurrentPlayer() { return players[currentPlayerIndex]; }


    //TODO add events to method so logic can be triggered
    /// <summary>
    /// Called every frame, detects where in turn are we and advances game when needed.
    /// </summary>
    /// <returns>True if it can be called again this frame.</returns
    public bool ProceedWithTurn()
    {
        if (gameWon) return false;
        if (this != gm.ls)
        {
            int a = 1;
        }
        // is in battle screen
        if (battle != null)
        {
            if (!battle.br.resolved) return false;
            battle = null;
        }
        // any animation running
        if (AnyUnitAnimating()) return false;
        if (gui.IsAnimating()) return false;
        if (!gm.processedBuffs && gm.processingBuffs) return false;
        // turn finished or before first turn?
        if (turnOrderCurrent == null || currentUnitIndex == turnOrderCurrent.Count)
        {   
            // end turn (if it's not before first turn)
            if (turnOrderCurrent != null)
            {
                for (int i = 0; i < players.Count; i++)
                {
                    players[i].EndTurn(currentTurn);
                }
                currentTurn++;
                currentUnitIndex = 0;
            }

            // begin new turn
            MyHelpers.LogColor(null, "white", "---------------- Turn " + currentTurn + " ----------------");


            // process buffs before showing units
            //gm.processedBuffs = false;
            //gm.StartCoroutine(gm.ProcessBuffs());

            // signal next turn
            if (currentTurn > 1)
            {
                for (int i = 0; i < players.Count; i++)
                {
                    players[i].BeginTurn();
                }
            }
            for (int i = 0; i < settlements.Count; i++)
            {
                settlements[i].BeginTurn();
            }

            // prepare turn order
            turnOrderCurrent = new List<Unit>(units);
            comp.sortNextTurn = false;
            turnOrderCurrent.Sort(comp);

            turnOrderNext = new List<Unit>(units);
            comp.sortNextTurn = true;
            turnOrderNext.Sort(comp);

            UpdateLeveleAlignment();
            currentUnitIndex = 0;
            gui.NotifyNextTurn();
            for (int i = 0; i < units.Count; i++)
            {
                if (units[i].killed) continue;
                units[i].mb.SetUnitGui();
            }
            return false;
        }


        // any movement left for unit?
        Unit u = turnOrderCurrent[currentUnitIndex];
        if (u.skipsTurn)
        {
            if (u.tile.settlement == null) u.finishedTurn = true;
        }
        if (u.attacking) // has the unit attacked by this move
        {
            battle = new Battle(u, u.tile.LiveEnemy(u.player), u.lastTile, u.tile, predict: false);
            guiBattle.StartBattle(battle);
            return false;
        }
        if (u.finishedTurn){
            currentUnitStartedTurn = false;
            // next unit turn
            if (!processWait)
            {
                u.EndMyTurn();
                CleanUpDeadUnitsFromTurnOrder();
                currentUnitIndex++;
                currentUnitStartedTurn = false;

                if (!AnyLiveEnemies())
                {
                    if (AnyLiveAIUnits() || gm.lset.eliminationVictory)
                    {
                        gameWon = true;
                        gui.NotifyGameWon();
                        return false;
                    }
                }

                return true;
            }
            else
            // this unit has waited
            {
                processWait = false;
                currentUnit.finishedTurn = false;
                if (currentUnitIndex == turnOrderCurrent.Count - 1) // end turn if last in row
                {
                    return true;
                }
                turnOrderCurrent.Insert(currentUnitIndex + 2, currentUnit);
                turnOrderCurrent.RemoveAt(currentUnitIndex);
                DisplayTurnOrder();
                return false;
            }
        }
        else
        {
            // actual unit
            if (!u.killed)
            {
                if (currentUnitStartedTurn)
                {
                    if (GetCurrentPlayer().playerType == PlayerType.local) KeyboardControl();
                    return false;
                }
                else // start unit turn - can be called mutiple times per turn if unit waits
                // 1/turn processing must go into BegunTurn() 
                {
                    MyHelpers.Log(this, "Turn " + currentTurn + ", current unit " + u.name);
                    currentPlayerIndex = CalcAndGetCurrentPlayerIndex(u.player);
                    currentUnitStartedTurn = true;
                    if (GetCurrentPlayer().playerType == PlayerType.local)
                        gui.SetSelectionMarker(u.mb as UnitMB);
                    gui.SetActivePlayer(u.player);
                    u.BeginMyTurn();

                    turnOrderNext = new List<Unit>(units);   // update next turn order
                    comp.sortNextTurn = true;
                    turnOrderNext.Sort(comp);
                    DisplayTurnOrder();

                    u.player.ProcessPerUnitTurn(u);
                    gui.SetSelectedUnitInfoText();
                }
            }
            else u.finishedTurn = true; // dead units finish turn immediately
            return false; // for human player, just wait for turn to finish
        }

    }

    /// <summary>
    /// Is any unit in process of moving or dying right now?
    /// </summary>
    /// <returns></returns>
    public bool AnyUnitAnimating()
    {
        foreach (Unit u in units)
        {
            if (u.IsPending()) return true;
        }
        //TODO is notification animating return false
        return false;
    }

    public bool AnyBuffAnimating()
    {
        foreach (Buff b in buffs)
        {
            if (b.mb.IsPlaying()) return true;
        }
        return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="newTile"></param>
    /*public void MoveCurrentUnit(TerrainTile newTile)
    {
        if (currentUnit == null) return;
        currentUnit.MoveStart(newTile);
    }*/

    /// <summary>
    /// Tile clicked. Either we move our unit, or we attack enemy, or click tile too far away.
    /// </summary>
    /// <param name="t"></param>
    public void TileActionUI(TerrainTile t)
    {
        MyHelpers.Log(this, "TileAction");
        if (t == null) return;

        if (currentUnit == null)
        {
            MyHelpers.LogError(this, "no selected unit");
            return;
        }

        if (gui.IsAnimating()) return;

        // did we click enemy?
        //Unit enemy = t.GetUnits().Find(x => x.player.playerType != PlayerType.local); //not good for hotset
        Unit enemy = t.GetUnits().Find(x => x.player.playerName != GetCurrentPlayer().playerName);
        if (enemy)
        {
            currentUnit.attacking = true;
        }

        // try to move
        MyHelpers.Log(this, "TileAction about to move unit");
        if (currentUnit == null) return;
        if (currentUnit.TooTiredToMove())
        {
            gui.BriefNotify("Unit too tired to move.");
            return;
        };
        currentUnit.MoveStart(t);
    }


    /// <summary>
    /// Main casting spell. Targeted tiles must be known in advance using SpellTargeter
    /// CastSpell not in unit because maybe global spells could be cast (from orbit)
    /// manaCost is per turn
    /// //TODO check if unit has spells
    /// </summary>
    public void CastSpell(Unit caster, SpellDefinition spell, List<TerrainTile> targetTiles, int duration, int manaCost)
    {
        if (caster.mana < manaCost)
        {
            MyHelpers.LogError(this, "Insufficient mana error");
            return;
        }
        MyHelpers.Log(this, "Casting spell " + spell.spellName + ", " + duration + " turns, " + manaCost + " mana");
        caster.mana -= manaCost;
        List<Buff> newBuffs = gm.CastSpell(caster, spell, targetTiles, duration, this);
        if (newBuffs.Count == 0)
        {
            MyHelpers.LogError(this, "0 buffs");
            // AI working. init here manually
        }
        //TODO subtract contradictory spells
        //buffs.AddRange(newBuffs); // all buffs already added to list at creation
        gui.SetSelectedUnitInfoText();
    }

    
    /*public List<Unit> GetUnitsFromTiles(List<TerrainTile> someTiles)
    {
        List<TerrainTile> ret = new List<TerrainTile>();
        foreach (TerrainTile t in someTiles) ret.Add(t);
        return GetUnitsFromTiles(ret);
    }*/

    public List<Unit> GetUnitsFromTiles(List<TerrainTile> targetTiles)
    {
        List<Unit> ret = new List<Unit>();
        foreach (TerrainTile t in targetTiles) if (t.AnyLiveUnits()) ret.Add(t.GetUnit());
        return ret;
    }

    /// <summary>
    /// WASD
    /// //TODO if more than one unit, only selected unit gets keyboard control. MOVE TO PLAYER CLASS.
    /// </summary>
    private void KeyboardControl()
    {

        if (Input.GetKeyDown(KeyCode.Keypad4) || Input.GetKeyDown(KeyCode.A))
        {
            MyHelpers.Log(this, "left pressed");
            TileActionUI(tiles.GetLeftTile(currentUnit.tile));
        }
        else if (Input.GetKeyDown(KeyCode.Keypad6) || Input.GetKeyDown(KeyCode.D))
        {
            MyHelpers.Log(this, "right pressed");
            TileActionUI(tiles.GetRightTile(currentUnit.tile));
        }
        else if (Input.GetKeyDown(KeyCode.Keypad8) || Input.GetKeyDown(KeyCode.W))
        {
            MyHelpers.Log(this, "up pressed");
            TileActionUI(tiles.GetTopTile(currentUnit.tile));
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.X))
        {
            MyHelpers.Log(this, "down pressed");
            TileActionUI(tiles.GetBottomTile(currentUnit.tile));
        }
        // diagonal
        if (Input.GetKeyDown(KeyCode.Keypad7) || Input.GetKeyDown(KeyCode.Q))
        {
            MyHelpers.Log(this, "top left pressed");
            TileActionUI(tiles.GetTopLeftTile(currentUnit.tile));
        }
        else if (Input.GetKeyDown(KeyCode.Keypad9) || Input.GetKeyDown(KeyCode.E))
        {
            MyHelpers.Log(this, "top right pressed");
            TileActionUI(tiles.GetTopRightTile(currentUnit.tile));
        }
        if (Input.GetKeyDown(KeyCode.Keypad1) || Input.GetKeyDown(KeyCode.Y))
        {
            MyHelpers.Log(this, "bottom left pressed");
            TileActionUI(tiles.GetBottomLeftTile(currentUnit.tile));
        }
        else if (Input.GetKeyDown(KeyCode.Keypad3) || Input.GetKeyDown(KeyCode.C))
        {
            MyHelpers.Log(this, "bottom right pressed");
            TileActionUI(tiles.GetBottomRightTile(currentUnit.tile));
        }

    }

    /// <summary>
    ///TODO all unit params
    /// </summary>
    /// <param name="name"></param>
    /// <param name="player"></param>
    /// <param name="tile"></param>
    public void CreateUnit(string name, Player player, TerrainTile tile)
    {
        MyHelpers.Log(this, "Creating unit " + name);
        Unit u = gm.CreateUnit(name, player, tile, this);
        if (u == null) {
            u = new Unit(); // for AI unit will not be created on scene
            u.Init(UnitMB.unitMBDummy, name, gm, this, player, tile);
        }
    }
    public Unit GetUnitByName(string name)
    {
        Unit uFound = null;
        foreach (Unit u in units)
        {
            if (u.name == name)
            {
                if (uFound != null)
                {
                    MyHelpers.LogError(this, "Duplicate unit name found.");
                    return null;
                }
                uFound = u;
            }
        }
        return uFound;
    }

    public Player GetCurrentPlayer() { return players[currentPlayerIndex]; }
    public int CalcAndGetCurrentPlayerIndex(Player player)
    {
        int c = 0;
        foreach (Player p in players)
        {
            if (p == player)
            {
                currentPlayerIndex = c;
                return c;
            }
            c++;
        }
        MyHelpers.Log(this, "current player error");
        return -1;
    }
    public bool IsCurrentPlayerLocal() { return GetCurrentPlayer().playerType == PlayerType.local; }
    public Player GetPlayerByName(string name)
    {
        Player pFound = null;
        foreach (Player p in players)
        {
            if (p.playerName == name)
            {
                if (pFound != null)
                {
                    MyHelpers.LogError(this, "Duplicate player name found.");
                    return null;
                }
                pFound = p;
            }
        }
        return pFound;
    }

    public void DisplayTurnOrder()
    {
        // get coords from image
        // get unit portraits
        // draw LIVING in order with some spacing
        // create and position current turn marker - sprite

        string ret = "TURN + " + currentTurn + "\n\nCURRENT ORDER\n";
        int count = 0;
        foreach (Unit u in turnOrderCurrent)
        {
            if (!u.killed)
            {
                if (count == currentUnitIndex) ret += ">> ";
                ret += "<color=" + u.player.mb.GetPlayerColorAsHex() + ">"
                    + u.name + ", tl:" + u.tirednessAddedLastTurn + "</color>\n";
            }
            count++;
        }
        ret += "\nNEXT TURN ORDER\n";
        count = 0;
        foreach (Unit u in turnOrderNext)
        {
            if (u.killed) continue;
            if (u == turnOrderCurrent[currentUnitIndex]) ret += ">> ";
            ret += "<color=" + u.player.mb.GetPlayerColorAsHex() + ">"
                + u.name + ", tl:" + u.stamina + "</color>\n";
        }
        gui.SetGameInfoText(ret);
    }

    #region IUnitContainer

    public bool AnyLiveUnits()
    {
        return (units.FindAll(x => !x.killed).Count > 0);
    }

    /// <summary>
    /// Also detects who is the winning player if there is only one playre standing.
    /// Neutral player is ignored.
    /// </summary>
    public bool AnyLiveEnemies()
    {
        int livePlayers = 0;
        foreach (Player p in players)
        {
            if (p == GetNeutralPlayer()) continue;
            //if (units.Count(x => !x.killed && x.player.playerName == p.playerName) > 0) livePlayers++;
            if (LiveFriendlies(p).Count() > 0) livePlayers++;
        }
        if (livePlayers > 1) return true;
        if (livePlayers == 0) killerPlayer = null;
        else killerPlayer = units.Find(x => !x.killed && x.player != gm.ls.GetNeutralPlayer()).player;
        return false;
    }
    public IEnumerable<Unit> LiveEnemies(Player p)
    {
        return units.Where(x => !x.killed && x.player != p && x.player != GetNeutralPlayer());
    }
    public IEnumerable<Unit> LiveFriendlies(Player p)
    {
        return units.Where(x => !x.killed && x.player == p);
    }

    public bool AnyLiveEnemies(Player p)
    {
        Unit u = units.Find(x => !x.killed && x.player != p && x.player != GetNeutralPlayer());
        if (u == null) return false;
        return true;
    }
    public bool AnyLiveFriendlies(Player p)
    {
        Unit u = units.Find(x => !x.killed && x.player == p);
        if (u == null) return false;
        return true;
    }

    public bool AnyLiveAIUnits()
    {
        Unit u = units.Find(x => !x.killed && x.player.playerType == PlayerType.AI);
        if (u == null) return false;
        return true;
    }

    public IEnumerable<Unit> LiveEnemiesNeighbouring(Player p, TerrainTile t)
    {
        return units.Where(x => !x.killed && x.player != p
        && tiles.GetTileDistanceMoves(t, x.tile) == 1);
    }
    public IEnumerable<Unit> LiveFriendliesNeighbouring(Player p, TerrainTile t)
    {
        return units.Where(x => !x.killed && x.player == p 
        && tiles.GetTileDistanceMoves(t, x.tile) == 1);
    }

    #endregion
}

public class TurnOrderSorter : IComparer<Unit>
{
    public bool sortNextTurn;
    public int inverse = 1;
    int ret;

    /// <summary>
    /// If u1 goes first, return -1
    /// </summary>
    public int Compare(Unit u1, Unit u2)
    {
        if (u1 == u2) return 0;

        if (!u1.killed && u2.killed) return (ret = -1 * inverse);
        if (u1.killed && !u2.killed) return (ret = 1 * inverse);

        if (sortNextTurn)
        {

            return 0;
        }

        // if more tired, has less initiative
        if (u1.tirednessAddedLastMove == u2.tirednessAddedLastMove) ret = 0;
        else if (u1.tirednessAddedLastMove < u2.tirednessAddedLastMove) ret = -1;
        else if (u1.tirednessAddedLastMove > u2.tirednessAddedLastMove) ret = 1;

        if (ret == 0)
        {
            // larger units have less initiative
            if (u1.count == u2.count) ret = 0;
            else if (u1.count < u2.count) ret = -1;
            else if (u1.count > u2.count) ret = 1;
        }

        if (ret == 0)
        {
            // higher morale is more initiative
            if (u1.morale == u2.morale) ret = 0;
            else if (u1.morale > u2.morale) ret = -1;
            else if (u1.morale < u2.morale) ret = 1;
        }

        if (ret == 0)
        {
            // less food means lighter load and more motivation to move
            if (u1.food == u2.food) ret = 0;
            else if (u1.food < u2.food) ret = -1;
            else if (u1.food > u2.food) ret = 1;
        }

        if (ret == 0)
        {
            MyHelpers.LogError(this, "Same values, cannot properly sort turn order");

        }


        return ret * inverse; //TODO last one to move is last one this turn also, or one with more kills, or one with less men, or other initiative modifiers
                            //} finally
                            //{
                            //    if (sortByTotalTiredness) MyHelpers.Log(this, "SORT " + u1.tiredness + ", " + u2.tiredness + ", " + ret);
                            //    else MyHelpers.Log(this, "SORT " + u1.tirednessAddedLastMove + ", " + u2.tirednessAddedLastMove + ", " + ret);
                            //}
    }
}

public interface ILevelState
{
    void Destroy();

    // state manipulation
    void CreateUnit(string name, Player player, TerrainTile tile);

    // 
    bool ProceedWithTurn();
    bool AnyUnitAnimating();

    //void MoveCurrentUnit(TerrainTile newTile);
    void TileActionUI(TerrainTile t);
}

public interface IUnitContainer
{
    bool AnyLiveEnemies();
    IEnumerable<Unit> LiveEnemies(Player p);
    IEnumerable<Unit> LiveFriendlies(Player p);

    IEnumerable<Unit> LiveEnemiesNeighbouring(Player p, TerrainTile t);
    IEnumerable<Unit> LiveFriendliesNeighbouring(Player p, TerrainTile t);
}

