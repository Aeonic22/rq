﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LogSettings", menuName = "ScriptableObjects/LogSettings", order = 1)]
public class LogSettings : ScriptableObject {

    [Tooltip("GameMain and other MBs")]
    public bool mono = true;
    [Tooltip("LevelState processing")]
    public bool levelState = true;
    [Tooltip("Units and settlements")]
    public bool units = true;
    [Tooltip("Terrain")]
    public bool terrain = true;
    [Tooltip("GUI")]
    public bool gui = true;
    [Tooltip("Battle")]
    public bool battle = true;
    [Tooltip("AI")]
    public bool ai = true;
    public bool spells = true;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
