﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used by SpellMB to show appropriate hilights and icons when targeting spells.
/// </summary>
public class SpellTargetTile : MonoBehaviour {

    /// <summary>
    /// Whether pulsating right now
    /// </summary>
    public bool animate;

    public bool valid;
    public bool outOfRange;
    public bool targeted;
    public bool selected;
    public Sprite spValid;
    public Sprite spTarget;
    public Sprite spOutOfRange;

    public GameObject costText;

    /// <summary>
    /// Required energy to cast to this location, used for display only
    /// </summary>
    private int manaCost;
    public void SetManaCost(int cost) { manaCost = cost; }
    /// <summary>
    /// Same as tile position
    /// </summary
    //public int x, y;
    public TerrainTile tile;

    [System.NonSerialized]
    public SpriteRenderer sr;

    public static SpellMB spell;

	// Use this for initialization
	public void Init () {
        sr = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetValid()
    {
        valid = true;
        sr.sprite = spValid;
        
    }

    public void SetOutOfRange()
    {
        outOfRange = true;
        sr.sprite = spOutOfRange;
    }

    public void SetTarget()
    {
        targeted = true;
        sr.sprite = spTarget;
    }

    public void SetSelected()
    {
        selected = true;
    }

    public void ResetToValid()
    {
        if (valid)
        {
            selected = targeted = false;
            sr.sprite = spValid;
        }
        else
        {
            // do nothing
        }
    }

}
