﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour, ITestMeat {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int Count { get; set; }
    public string Name { get; set; }
}

public interface ITestMeat
{
    int Count { get; set; }
    string Name { get; set; }
}
