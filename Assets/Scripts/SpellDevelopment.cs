﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpellLevel
{
    public SpellId spell;
    /// <summary>
    /// Taken from enum
    /// </summary>
    public string name;
    /// <summary>
    /// 1-4
    /// </summary>
    public int level;
}

[CreateAssetMenu(fileName = "SpellDevelopment", menuName = "ScriptableObjects/SpellDevelopment", order = 1)]
public class SpellDevelopment : ScriptableObject {

    public int maxHaarpRange;
    public int maxHaarpWall;
    public int maxHaarpChargePerTurn;
    public List<SpellLevel> spells;

    // Use this for initialization - init with no active spells
    public void Init () {
		if (spells == null || spells.Count == 0)
        {
            if (spells != null) spells.Clear();
            foreach (SpellId id in System.Enum.GetValues(typeof(SpellId)))
            {
                spells.Add(new SpellLevel {
                    spell = id,
                    name = System.Enum.GetName(typeof(SpellId), id),
                    level = 0 });
            }
        }
	}
}
