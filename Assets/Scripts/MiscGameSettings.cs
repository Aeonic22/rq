﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MiscGameSettings", menuName = "ScriptableObjects/MiscGameSettings", order = 1)]
public class MiscGameSettings : ScriptableObject {

    [Header("game flow")]
    /// <summary>
    /// How long is the move from tile to tile. It is multiplied for difficult terrain proportional to tiredness.
    /// </summary>
    public float oneTileMoveTime = 0.15f;
    /// <summary>
    /// How long is the move from tile to tile. It is multiplied for difficult terrain proportional to tiredness.
    /// </summary>
    public float oneTileMoveTimeTired = 1f;
    /// <summary>
    /// Fade out on death.
    /// </summary>
    public float fadeOutTime = 1f;
    /// <summary>
    /// For yield function to avoid every Update.
    /// </summary>
    public float yieldWaitDelay = 0.1f;
    /// <summary>
    /// After unit is moved, next unit automatically selected
    /// </summary>
    public bool finishTurnOnMove = true;

    [Header("z positions, coords")]
    public float zPosCamera = -10f;
    public float zPosTargeting = -6.5f;
    public float zPosBuffs = -6f;
    public float zPosUnits = -5f;
    public float zPosSettlements = -4f;
    public float zPosTerrainMain = 0f;

    [Header("Text")]
    public string notEnoughEnergyForRangeAtThisStrength = "Reduce strength to be able to cast this far.";
    public string notEnoughEnergyForRange = "You don't have enough energy to cast this far.";
    public string spellHealDesc = "Invigorates and heals target unit";


    // Use this for initialization
    //void OnEnable () {
    //    MyHelpers.Log(this, "MiscGameSettings OnEnable");
    //}

    // Update is called once per frame
    void Update () {
		
	}
}
