﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Is RESTful, it takes LevelState from Unit so it has no state of its own for now.
/// Just a wrapper for Unit AI for now.
/// </summary>
public class PlayerAI : MonoBehaviour, IAIBasic {

    // cached
    [System.NonSerialized]
    public static GameMain gm;
    private Terrain tiles;
    private LevelState ls;
    private Player player;

    List<AspectEvaluation> aspectEvaluationFunctions;
    Array neighbourTiles;
    int numDirections;



    private void Awake()
    {
        //player = GetComponent<Player>();
    }
    // Use this for initialization
    void Start () {
        //player = GetComponent<Player>();
        gm = FindObjectOfType<GameMain>();
        aspectEvaluationFunctions = new List<AspectEvaluation>();
        aspectEvaluationFunctions.Add(AttackPotential);
        aspectEvaluationFunctions.Add(SettlementConquerPotential);
        neighbourTiles = Enum.GetValues(typeof(NeighbourTile));
        numDirections = neighbourTiles.Length;
    }

    public void Init(LevelState ls, Player player)
    {
        tiles = ls.tiles;
        this.ls = ls;
        this.player = player;
    }

    public void BeginTurn()
    {

    }

    /*public IEnumerator ProcessTurn()
    {
        MyHelpers.Log(this, "player " + player.playerName + " BEGIN TURN");
        var units = ls.units.FindAll(x => x.player.playerSide == player.playerSide && !x.killed);
        //while (true) yield return null;
        bool temp = false;
        foreach (Unit u in units)
        {
            while (ls.AnyUnitAnimating())   // wait for current move to end
            {
                if (temp == false)
                {
                    MyHelpers.LogError(this, "some movement still left before AI has been called.");
                    temp = true;
                }
                yield return new WaitForSeconds(Defines.def.miscGameSettings.yieldWaitDelay);
            }
            u.ProcessAiTurn();  // then process next unit       
            temp = true;
        }
        while (ls.AnyUnitAnimating()) // to finish move of last unit and allow it to switch to playerSelected
            yield return new WaitForSeconds(Defines.def.miscGameSettings.yieldWaitDelay);
        MyHelpers.Log(this, "player " + player.playerName + " END TURN");
        yield return null;

        //TODO
        // create shared visible map
        // do global planning
        // if units come together, share information - maybe not here
    }*/

    public void ProcessUnitTurn(Unit u)
    {
        MyHelpers.Log(this, "ProcessUnitTurn");
        //var units = ls.units.FindAll(x => x.player.playerSide == player.playerSide && !x.killed);
        //while (true) yield return null;
        //bool moveErr = false;

        /*while (gm.IsMoving())   // wait for current move to end - should not be happening
        {
            moveErr = true;
            yield return new WaitForSeconds(Defines.def.yieldWaitDelay);
        }
        if (moveErr) MyHelpers.LogError(this, "some movement still left before AI has been called.");*/

        u.ProcessAiTurn(this);  // then process next unit  
        /*TerrainTile nt = SelectMove(u);
        if (nt == u.tile) u.SkipTurn();
        else if (nt.LiveEnemy(u.player)) u.Attack(nt.LiveEnemy(u.player));
        else u.MoveStart(nt);*/

        //while (gm.IsMoving()) // to finish move of last unit and allow it to switch to playerSelected
        //while (gm.AnyUnitPending())
        //    yield return new WaitForSeconds(Defines.def.yieldWaitDelay);
        MyHelpers.Log(this, "unit " + u.name + " END TURN");
    }

    public void EndTurn()
    {
    }

    // Update is called once per frame
    void Update () {
		
	}

    /// <summary>
    /// Staying put is also a move. Skips occupied tiles.
    /// </summary>
    public TerrainTile SelectMove(Unit u)
    {
        float[] moveValues = new float[numDirections];
        float max = 0;
        int maxMove = 0;

        foreach (NeighbourTile move in neighbourTiles)
        {
            int moveInt = (int)move;
            if (move != NeighbourTile.Self && tiles.GetNeighbouringTile(u.tile, move) && tiles.GetNeighbouringTile(u.tile, move).AnyLiveUnits()) // skip adjacent occupied tiles
            {
                moveValues[moveInt] = 0;
                continue;
            }

            moveValues[moveInt] = EvaluateMove(u, move);
            if (moveValues[moveInt] > max)
            {
                max = moveValues[moveInt];
                maxMove = moveInt;
            }
        }

        TerrainTile nt = tiles.GetNeighbouringTile(u.tile, (NeighbourTile)maxMove);
        MyHelpers.Log(this, "___Move to " + nt.ToString() + " selected___");
        return nt;
    }

    /// <summary>
    /// Evaluate move across all criteria and return a single -1 to 1 value.
    /// </summary>
    /// <returns></returns>
    public float EvaluateMove(Unit u, NeighbourTile move)
    {
        if (tiles.GetNeighbouringTile(u.tile, move) == null) return 0;

        int maxValueTotal = 0;
        float valueTotal = 0;

        foreach(AspectEvaluation aef in aspectEvaluationFunctions)
        {
            aspectResults ar = aef.Invoke(u, move);
            //maxValueTotal += ar.maxValue;
            //valueTotal += ar.value;
            valueTotal += ar.value / (float) ar.maxValue;
        }

        //float ret = valueTotal /(float) maxValueTotal;
        float ret = valueTotal / aspectEvaluationFunctions.Count;
        //MyHelpers.Log(this, "Move to " + move.ToString() + " evaluated as " + ret);
        return ret;
    }

    /// <summary>
    /// Potential for killing enemy units, takes into account count, strength, proximity, chance of win
    /// </summary>
    public aspectResults AttackPotential(Unit u, NeighbourTile move)
    {
        TerrainTile newTile = tiles.GetNeighbouringTile(u.tile, move);
        aspectResults ar;
        ar.name = "AttackPotential";
        ar.maxValue = 5000;
        ar.value = 0;
        foreach (Unit enemy in ls.LiveEnemies(u.player))
        {
            int dist = (int)( (ls.tiles.GetTileDistanceMoves(newTile, enemy.tile) + ls.tiles.GetTileDistanceManhattan(newTile, enemy.tile))/2f ) + 1;
            ar.value += (int) (enemy.count * ChanceOfWinDistIgnored(u, enemy) / Mathf.Pow(dist, 2f) );
            /*if (dist == 1) // for combining attacks
            {
                int[] acs = AdjacentEnemyCountStrength(enemy);
                ar.value += acs[0] - u.count;
            }*/
        }
        if (ar.value > ar.maxValue) MyHelpers.LogError(this, "AttackPotential max value too small");
        return ar;
    }
    /*public aspectResults SelfDefensePotential(Unit u, NeighbourTile move)
    {
        maxValue = 100;
        return 1;
    }*/
    public aspectResults SettlementConquerPotential(Unit u, NeighbourTile move)
    {
        TerrainTile newTile = tiles.GetNeighbouringTile(u.tile, move);
        aspectResults ar;
        ar.name = "SettlementConquerPotential";
        ar.maxValue = 0;
        ar.value = 0;
        if (ls.settlements.Count == 0)
        {
            ar.maxValue = 1;
            return ar;
        }
        foreach (Settlement s in ls.settlements)
        {
            ar.maxValue += s.countFolks;
            if (s.alignment == player) continue;
            //int dist = ls.tiles.GetTileDistanceMoves(newTile, s.tile)+1;
            int dist = (int)((ls.tiles.GetTileDistanceMoves(newTile, s.tile) + ls.tiles.GetTileDistanceManhattan(newTile, s.tile)) / 2f) + 1;
            ar.value += s.countFolks / dist;
        }
        //MyHelpers.Log(this, "SettlementConquerPotential valued at " + ar.value);
        return ar;
    }
    /*public aspectResults SettlementDefensePotential(Unit u, NeighbourTile move)
    {
        maxValue = 100;
        return 1;
    }
    public aspectResults RestockPotential(Unit u, NeighbourTile move)
    {
        maxValue = 100;
        return 1;
    }
    public aspectResults HealPotential(Unit u, NeighbourTile move)
    {
        maxValue = 100;
        return 1;
    }
    public aspectResults GoalPotential(Unit u, NeighbourTile move)
    {
        maxValue = 0;
        return 0;
    }*/

    /// <summary>
    /// Equal strength return 0.5
    /// Anybody's strength approaching 0 returns 0 or 1
    /// 2x stronger enemy means 0.25 chance of win
    /// 10x stronger enemy means 0.05 chance of win
    /// 2x stronger us means 0.75 chance to win
    /// </summary>
    /// <returns></returns>
    public float ChanceOfWinDistIgnored(Unit us, Unit them)
    {
        if (us.GetStrengthActual() > float.Epsilon && them.GetStrengthActual() < float.Epsilon) return 1;
        if (us.GetStrengthActual() < float.Epsilon && them.GetStrengthActual() > float.Epsilon) return 0;

        float chance = 0.5f;
        float ratio = Mathf.Sqrt(us.GetStrengthActual() / them.GetStrengthActual());
        if (ratio < 1) chance /= ratio;
        else if (ratio > 1) chance = 1 - 0.5f / ratio;
        return chance;
    }

    /// <summary>
    /// Get strength of all enemy surrounding unit - excluding self tile of course.
    /// </summary>
    public int[] AdjacentEnemyCountStrengthInAttack(Unit u)
    {
        int count = 0;
        float strength = 0;
        foreach (NeighbourTile nt in neighbourTiles)
        {
            TerrainTile t = tiles.GetNeighbouringTile(u.tile, nt);
            if (!t) continue;
            Unit enemy = t.LiveEnemy(u.player);
            if (!enemy) continue;
            count += enemy.count;
            strength += enemy.GetStrengthActualAfterMove(u.tile);
        }
        int[] ret = new int[2] { count, (int)strength };
        return ret;
    }

    void CombinedMoveEvaluation()
    {
        // units in order of initiative
        Unit[] units = new Unit[3];
        NeighbourTile[][] nt = new NeighbourTile[3][];
        nt[0] = new NeighbourTile[2] { NeighbourTile.Bottom, NeighbourTile.Top };
        nt[1] = new NeighbourTile[2] { NeighbourTile.Left, NeighbourTile.Right };
        nt[2] = new NeighbourTile[2] { NeighbourTile.TopLeft, NeighbourTile.TopRight };

        int numUnits = 3;
        int last = numUnits - 1;
        int[] counter = new int[numUnits];
        int[] bestIndex = new int[numUnits];
        float bestValue = 0;
        int total = nt[0].Length * nt[1].Length * nt[2].Length;
        for (int i = 0; i < total; i++)
        {
            // evaluate board 
            //TODO I need board copy and function to advance gameplay by specified move
            // then I perform all moves and analyse board

            // better move?
            float ret = 0;
            if (ret > bestValue)
            {
                bestValue = ret;
                counter.CopyTo(bestIndex, 0);
            }

            // advance indexes
            counter[last]++;
            int current = last;
            while (counter[current] == nt[current].Length)
            {
                counter[current] = 0;
                current--;
                if (current >= 0)
                    counter[current]++;
            }
        }
    }



}

delegate aspectResults AspectEvaluation(Unit u, NeighbourTile move);

public struct aspectResults
{
    public string name;
    public int value;
    public int maxValue;
}
public interface IAIBasic
{
    TerrainTile SelectMove(Unit u);
    float EvaluateMove(Unit u, NeighbourTile move);

    //aspectResults AttackPotential(Unit u, NeighbourTile move);
    //aspectResults SelfDefensePotential(Unit u, NeighbourTile move);
    aspectResults SettlementConquerPotential(Unit u, NeighbourTile move);
    //aspectResults SettlementDefensePotentia(Unit u, NeighbourTile move);
    //aspectResults RestockPotential(Unit u, NeighbourTile move);
    //aspectResults HealPotential(Unit u, NeighbourTile move);
    //aspectResults GoalPotential(Unit u, NeighbourTile move);
    //int SupplyOthersPotential(Unit u, NeighbourTile move);
    //int MagicPotential(Unit u, NeighbourTile move);

}


