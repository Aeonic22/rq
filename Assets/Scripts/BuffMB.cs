﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum BuffMBAnims { Maintain = 0, Create, Affect, Dissipate }
/// <summary>
/// Takes care of displaying buff
/// </summary>
public class BuffMB : MonoBehaviour, IBuffMB
{

    public Buff me;
    public Sprite sprite {
        get { return GetComponent<SpriteRenderer>().sprite; }
        set { GetComponent<SpriteRenderer>().sprite = value; }
    }

    private Animator animator;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    /// <summary>
    /// Either does state transition anim or just runs maintenance animation
    /// </summary>
    void Update()
    {
    }


    /// <summary>
    /// When any animation playing except maintenance animation. Or if animation of those states is triggered and pending.
    /// </summary>
    public bool IsPlaying()
    {
        // maintain animation does not stop game execution 
        //animator.HasState(0, )
        if (animator == null) return false; // because it can destroy itself
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Maintain"))
        {
            // maybe some pending triggers?
            foreach (AnimatorControllerParameter acp in animator.parameters)
            {
                if (acp.type != AnimatorControllerParameterType.Trigger) continue;
                if (animator.GetBool(acp.name) == true)
                {
                    //MyHelpers.Log(this, "Buff playing, trigger " + acp.name);
                    return true;
                }
            }
            return false;
        }
        //MyHelpers.Log(this, "Buff playing, state not Maintain");
        return true;
    }

    /// <summary>
    /// Run create animation
    /// </summary>
    public void Create()
    {
        MyHelpers.Log(this, me.spell.spellName + " over " + me.unit + " create animation.");
        
    }
    /// <summary>
    /// Run affect animation
    /// </summary>
    public void Apply()
    {
        animator.SetTrigger("apply");
        MyHelpers.Log(this, me.spell.spellName + " over " + me.unit + " affect animation.");
    }
    /// <summary>
    /// Run affect animation
    /// </summary>
    public void ApplyAndEnd()
    {
        animator.SetTrigger("applyAndEnd");
        MyHelpers.Log(this, me.spell.spellName + " over " + me.unit + " affect and end animation.");
    }
    /// <summary>
    /// Run dissipate animation and selfdesctuct
    /// </summary>
    public void End()
    {
        MyHelpers.Log(this, me.spell.spellName + " over " + me.unit + " dissipate animation.");
        animator.SetTrigger("end");
    }

    public void ApplyBuffFromAnim()
    {
        me.cbApplied();
    }

}

public interface IBuffMB
{
    bool IsPlaying();
    void Create();
    /// <summary>
    /// animation of affecting target and return to maintain spell
    /// </summary>
    void Apply();
    /// <summary>
    /// animation of affecting target terminates spell
    /// </summary>
    void ApplyAndEnd(); 
    void End();
}

public class BuffMBDummy
{
    public Buff me;
    public bool IsPlaying() { return false; }
    public void Create() { }
    /// <summary>
    /// does not wait for any animations
    /// </summary>
    public void Apply() { me.cbApplied(); }
    public void ApplyAndEnd() { me.cbApplied(); }
    public void End() { }
}

