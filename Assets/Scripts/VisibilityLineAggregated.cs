﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to be replaced by tilemap covering the area of any shape, capable of showing visibility for all friendly units
/// Currently implemented with one line renderer per tile seen.
/// </summary>
public class VisibilityLineAggregated : MonoBehaviour
{

    // state
    public bool needsUpdate = true;
    //public Vector2Int coords = new Vector2Int(0, 0); // tile coords
    //public Vector2 pos = new Vector2(0f, 0f);    // position of lower left corner of self tile
    public float tileSize = 1;
    //private bool[] visibility;  // for each neighbour and self, is other (player) army visible there

    // objects
    public GameObject lineRendFolder;
    public LineRenderer lineRenderer;   // template for one tile
    //private List<LineRenderer> renderers;   // one for each neighbour
    private KnolMap<LineRenderer> renderers;
    private GameMain gm;

    // cached
    public KnolMapComparable<UnitEngagement> unitVisibilityMap; // from player

    // misc
    bool inited = false;

    // Use this for initialization
    void Awake()
    {
        Init();
    }

    // create rectangle, make instances for all tiles
    // creation must follow enum Neighbour
    public void Init()
    {
        if (inited) return;
        MyHelpers.Log(this, "VisibilityLineAggregated init " + gameObject.name);
        inited = true;
        gm = FindObjectOfType<GameMain>();
        float len = gm.tileLen;
        float z = transform.position.z;

        //visibility = null;
        renderers = new KnolMap<LineRenderer>(gm.xTiles, gm.yTiles, KnolMapType.lineRenderer);

        lineRendFolder = new GameObject();
        lineRendFolder.transform.position = new Vector3(0, 0, 0);
        lineRendFolder.name = "LineRenderers";

        // instantiate template, link all to parent 
        LineRenderer lr = Instantiate(lineRenderer, new Vector3(0, 0, 0), Quaternion.identity, lineRendFolder.transform);
        lr.positionCount = 5;
        lr.SetPosition(0, new Vector3(0f, 0f, 0));
        lr.SetPosition(1, new Vector3(len, 0f, 0));
        lr.SetPosition(2, new Vector3(len, len, 0));
        lr.SetPosition(3, new Vector3(0f, len, 0));
        lr.SetPosition(4, new Vector3(0f, 0f, 0));
        lr.name = "delete";
        lr.enabled = false;

        for (int i = 0; i < gm.xTiles; i++)
        {
            for (int j = 0; j < gm.yTiles; j++)
            {
                Vector3 wpos = gm.GetWorldCoords(i, j);
                renderers[i,j] = Instantiate(lr, wpos, Quaternion.identity, lineRendFolder.transform);
                renderers[i, j].name = "LR " + i + ", " + j;
            }
        }

        lineRendFolder.transform.position = new Vector3(0, 0, -2);

        Destroy(lr);

        needsUpdate = true;
    }

    // just add all neighbours that you want visible to params
    public void SetVisibilityAll(bool visible)
    {
        for (int i = 0; i < gm.xTiles; i++)
        {
            for (int j = 0; j < gm.yTiles; j++)
            {
                renderers[i, j].enabled = visible;
            }
        }
        needsUpdate = true;
    }

    // just add all neighbours that you want visible to params
    public void SetVisibility(KnolMapComparable<UnitEngagement> ueMap)
    {
        if (!inited) Init();
        unitVisibilityMap = ueMap;

        for (int i = 0; i < gm.xTiles; i++)
        {
            for (int j = 0; j < gm.yTiles; j++)
            {
                renderers[i, j].enabled = ueMap[i,j] >= UnitEngagement.Visible;
            }
        }
        needsUpdate = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!needsUpdate) return;
        needsUpdate = false;

        int count = 0;
        for (int i = 0; i < gm.xTiles; i++)
        {
            for (int j = 0; j < gm.yTiles; j++)
            {
                bool enable = unitVisibilityMap[i, j] >= UnitEngagement.Visible;
                renderers[i, j].enabled = enable;
                if (enable) count++;
            }
        }

        //MyHelpers.MyHelpers.Log(this, "VisibilityLineAggregated update, visible tiles " + count);
    }

    private void OnDestroy()
    {
        if (renderers != null)
            for (int i = 0; i < gm.xTiles; i++)
            {
                for (int j = 0; j < gm.yTiles; j++)
                {
                    Destroy(renderers[i, j]);
                }
            }
    }
}
