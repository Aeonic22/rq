﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

/// <summary>
/// Unit can select its tile
/// </summary>
[CustomEditor(typeof(UnitMB))]
public class UnitMBEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        UnitMB unitMb = (UnitMB)target;
        if (GUILayout.Button("Select tile"))
        {
            UnityEditor.Selection.activeGameObject = (unitMb.me.tile.mb as TerrainTileMB).gameObject;
        }

        if (GUILayout.Button("Snap to tile"))
        {
            Vector3 snapPos = new Vector3(Mathf.Floor(unitMb.transform.position.x), 
                Mathf.Floor(unitMb.transform.position.y), FindObjectOfType<Defines>().miscGameSettings.zPosUnits);
            unitMb.transform.position = snapPos;
            //TODO friendly on one side, enemy on other, city to middle etc.
            //TODO layout manager
        }
    }
}

/// <summary>
/// Settlement can select its tile
/// </summary>
[CustomEditor(typeof(SettlementMB))]
public class SettlementMBEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SettlementMB settMb = (SettlementMB)target;
        if (GUILayout.Button("Select tile"))
        {
            UnityEditor.Selection.activeGameObject = (settMb.me.tile.mb as TerrainTileMB).gameObject;
        }

        if (GUILayout.Button("Snap to tile"))
        {
            Vector3 snapPos = new Vector3(Mathf.Floor(settMb.transform.position.x),
                Mathf.Floor(settMb.transform.position.y), FindObjectOfType<Defines>().miscGameSettings.zPosSettlements);
            settMb.transform.position = snapPos;
            //TODO friendly on one side, enemy on other, city to middle etc.
            //TODO layout manager
            settMb.SetAlignment();
        }
    }
}

/// <summary>
/// Tile lists unit names and can select them
/// </summary>
[CustomEditor(typeof(TerrainTileMB))]
public class TerrainTileMBEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TerrainTileMB myScript = (TerrainTileMB)target;
        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("Units on tile:");
        foreach (Unit u in myScript.me.units)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(u.name);
            if (GUILayout.Button("Select unit"))
            {
                UnityEditor.Selection.activeGameObject = (u.mb as UnitMB).gameObject;
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }
}


[CustomEditor(typeof(TerrainGenerator))]
public class TerrainGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TerrainGenerator tgen = (TerrainGenerator)target;
        if (GUILayout.Button("Generate terrain (will delete current terrain)"))
        {
            tgen.GenerateTerrain();
            EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
        }
        if (GUILayout.Button("Generate units (will delete current units)"))
        {
            tgen.GenerateUnits();
            EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
        }
    }
}


/// <summary>
/// Spell dev initializes itself with spell list
/// </summary>
[CustomEditor(typeof(SpellDevelopment))]
public class SpellDevelopmentEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SpellDevelopment spellDev = (SpellDevelopment)target;
        if (GUILayout.Button("Initialize"))
        {
            spellDev.Init();
        }
    }
}
