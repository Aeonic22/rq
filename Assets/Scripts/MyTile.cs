﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// Used for programatically generating Tilemap
/// </summary>
public class MyTile : Tile
{
    public static TileMapper tMapper;

    // cannot FindObjectsOfType in scriptable object constructor

    public void Init(TerrainType tt)
    {
        switch (tt)
        {
            case TerrainType.Meadow:
                sprite = tMapper.terrainSprites[1];
                break;
            case TerrainType.Forest:
                sprite = tMapper.terrainSprites[2];
                break;
            case TerrainType.Mountain:
                sprite = tMapper.terrainSprites[3];
                break;
        }
    }

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        tileData.sprite = sprite;
        //tileData.color = this.color;
        //tileData.transform = this.transform;
        //tileData.gameobject = this.gameobject;
        //tileData.flags = (int)this.flags;
    }

    // later maybe when painting rule tiles
    //public virtual void RefreshTile(Vector3Int position, ITilemap tilemap);
}