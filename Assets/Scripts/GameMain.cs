﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.Tilemaps;

public delegate void OnTileEnterHandler(TerrainTile tile);
public delegate void OnTileExitHandler(TerrainTile tile);
public delegate void OnTileClickHandler(TerrainTile tile);

public class GameMain 
    : MonoBehaviour
{

    private static GameMain instance;

    // terrain
    private Vector3 startPos = new Vector3(0, 0, 0); // for positioning other tiles in scene
    [HideInInspector] public float tileLen = 1f;  // in world units
    [HideInInspector] public float tileScale = 1f; // how much to scale tile (which is deafult 1 unit) to get to tileLen, default is 1 for 10x10 map
    private GameObject tilesFolder;
    [SerializeField] GameObject tileTemplateSprite = null;
    private Tilemap tmMain;
    /// <summary>
    /// Stored in Terrain
    /// </summary>
    public int xTiles { get { return ls.tiles.xTiles; } }
    /// <summary>
    /// Stored in Terrain
    /// </summary>
    public int yTiles { get { return ls.tiles.yTiles; } }

    // units
    private GameObject unitsFolder;
    [SerializeField] GameObject unitTemplateSprite = null;

    [SerializeField] GameObject buffTemplateSprite = null;

    // cached
    public IGUIController gui;
    public IGUIControllerBattle guiBattle;
    public LevelState ls;
    public LevelSettings lset;
    public SpellMB spellmb;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            ls = new LevelState();
        }
        else
        {
            MyHelpers.LogError(this, "gamemain singleton error");
            Destroy(this);
        }
    }

    // Use this for initialization
    // create sprites div x div on specified coordinates
    void Start()
    {
        tilesFolder = GameObject.Find(Defines.tilesFolderName);
        unitsFolder = GameObject.Find(Defines.unitsFolderName);
        tmMain = GameObject.Find(Defines.tilemapMainName).GetComponent<Tilemap>();
        gui = FindObjectOfType<GUIController>() as IGUIController;
        guiBattle = FindObjectOfType<GUIControllerBattle>() as IGUIControllerBattle;
        spellmb = FindObjectOfType<SpellMB>();
        lset = FindObjectOfType<LevelSettings>();

        MyHelpers.Start();

        Terrain tiles = InitTerrain();

        List<Player> players = new List<Player>();
        PlayerMB[] arrPlayers = FindObjectsOfType<PlayerMB>();
        foreach (PlayerMB pmb in arrPlayers) players.Add(pmb.me);

        ls.Start(tiles, null, players, this, lset.spellDev, gui, guiBattle);

        TileEntered = null;
        TileExited = null; 
        TileClicked = null;

    }


    // don't call in start because the start won't be called on instantiated tiles until this f has finished
    private Terrain InitTerrain(bool isRestart = false)
    {
        BoundsInt bi = Terrain.GetBoundsUsed(tmMain);
        if (bi.min != new Vector3Int(0, 0, 0))
        {
            MyHelpers.LogError(this, "Tilemap must start from 0,0,0");
            return null;
        }
        int xTiles = bi.xMax - bi.xMin;
        int yTiles = bi.yMax - bi.yMin;
        MyHelpers.Log(this, " gm creates tiles");
        Terrain tiles = new Terrain(xTiles, yTiles);

        return InitTerrainFromTilemap(tiles);
    }

    Terrain InitTerrainFromTilemap(Terrain tiles)
    {
        float tile_z = tileTemplateSprite.transform.position.z;

        for (int i = 0; i < tiles.xTiles; i++)
        {
            for (int j = 0; j < tiles.yTiles; j++)
            {
                Vector3 pos = GetWorldCoords(i, j, tile_z);
                //GameObject go = Instantiate(tileTemplateSprite, pos, Quaternion.identity);
                GameObject go = Instantiate(tileTemplateSprite, pos, Quaternion.identity, tilesFolder.transform);
                go.name = "Tile " + i + ", " + j;
                go.transform.localScale = new Vector3(tileScale, tileScale, 1);
                TerrainTileMB tt = go.GetComponent<TerrainTileMB>();
                tt.me.x = i;
                tt.me.y = j;
                {
                    Sprite sp = tmMain.GetSprite(new Vector3Int(i,j,0));
                    switch (sp.name)
                    {
                        case "TileForest":
                            tt.me.SetTileType(TerrainType.Forest);
                            break;
                        case "TileMountain":
                            tt.me.SetTileType(TerrainType.Mountain);
                            break;
                        case "TileMeadow":
                            tt.me.SetTileType(TerrainType.Meadow);
                            break;
                        default:
                            tt.me.SetTileType(TerrainType.Meadow);
                            MyHelpers.LogError(this, "Terrain type unset");
                            break;
                    }
                }
                tiles.GetAllTiles().Add(tt.me);
            }
        }
        return tiles;
    }

    public event OnTileEnterHandler TileEntered;
    public event OnTileExitHandler TileExited;
    public event OnTileClickHandler TileClicked;

    /// <summary>
    /// Centralized event broadcast, tiles report here
    /// </summary>
    public void OnTileEnter(TerrainTile tile)
    {
        if (TileEntered!= null) TileEntered(tile);
    }

    /// <summary>
    /// Centralized event broadcast, tiles report here
    /// </summary>
    public void OnTileExit(TerrainTile tile)
    {
        if (TileExited != null) TileExited(tile);
    }

    /// <summary>
    /// Centralized event broadcast, tiles report here
    /// </summary>
    public void OnTileClick(TerrainTile tile)
    {
        if (TileClicked != null) TileClicked(tile);
    }

    /// <summary>
    /// Delete units. Delete unit refs from tiles. Recreate units, reveal data, everything.
    /// </summary>
    public void Restart()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }

    public void SkipTurn()
    {
        ls.currentUnit.SkipTurn();
    }

    public void Wait()
    {
        ls.currentUnit.finishedTurn = true;
        ls.processWait = true;
    }


    /// <summary>
    /// Create unit of name, for player, on tile, with given sprite
    /// Core unit class is passed back to LevelState.
    /// //TODO if ls.minmax, return null
    /// </summary>
    public Unit CreateUnit(string name, Player player, TerrainTile tile, LevelState ls, Sprite sp = null)
    {
        MyHelpers.Log(this, "Creating unit " + name);
        Vector3 wpos = GetWorldCoords(tile.x, tile.y, Defines.def.miscGameSettings.zPosUnits);// z in defines
        GameObject go = Instantiate(unitTemplateSprite, wpos, Quaternion.identity, unitsFolder.transform);
        go.name = name;
        go.transform.localScale = new Vector3(tileScale, tileScale, 1);
        UnitMB unitmb = go.GetComponent<UnitMB>();
        if (sp) unitmb.SetSprite(sp);
        unitmb.playerMB = player.mb as PlayerMB;
        unitmb.me.Init(unitmb, name, this, ls, player, tile);
        return unitmb.me;
    }


    private SpellDefinition currentSpell;
    /// <summary>
    /// When spell icon is hovered over in UI. Needs int because enum cannot be set from GUI trigger functions.
    /// </summary>
    public void SetCurrentSpellUI(int spellId)
    {
        currentSpell = spellmb.spells.Find(x => x.spellId == (SpellId)spellId);

        GameObject newParent = spellmb.spellIcons.Find(x => x.spell == (SpellId) spellId).icon;
        //Vector2 offset = spellDurationPanel.transform.position - spellDurationPanel.transform.parent.position;
        //spellDurationPanel.transform.parent = newParent.transform;
        //spellDurationPanel.transform.position = offset;
        spellmb.spellDurationPanel.transform.SetParent(newParent.transform, worldPositionStays:false);
        spellmb.spellDurationPanel.SetActive(true);
    }

    /// <summary>
    /// When spell duration is clicked in the GUI
    /// </summary>
    public void SelectSpellDurationUI(int duration)
    {
        //if (ls.currentUnit.spells.Count == 0) return;
        spellmb.BeginTargeting(ls.currentUnit, currentSpell, duration);
        spellmb.DisplayValidTiles();
    }

    /// <summary>
    /// When spell is cast from UI - when target tile clicked
    /// </summary>
    public void CastSpellUI()
    {
        MyHelpers.Log(this, "Spell cast initiated UI");
        //if (ls.currentUnit.spells.Count == 0) return;
        var target = spellmb.me.GetTargetedTiles(spellmb.GetSelecetedTile());
        ls.CastSpell(ls.currentUnit, currentSpell, target, spellmb.me.duration, spellmb.selectedTile.tile.tempManaCost);
    }

    /// <summary>
    /// Creates Buff and BuffMB objects for specific spell at specific targets.
    /// Is not used for AI calculations.
    /// //TODO ignore for AI ls
    /// </summary>
    public List<Buff> CastSpell(Unit caster, SpellDefinition spell, List<TerrainTile> targetTiles, int duration, LevelState ls)
    {
        // create buffmb at target coords, connect buff, unit, ls, apply buff
        MyHelpers.Log(this, "Casting spell " + spell.spellName);
        List<Buff> newBuffs = new List<Buff>();
        if (spell.buffType == BuffType.PerUnit)
        {
            //List<Unit> targetUnits = ls.GetUnitsFromTiles(targetTiles);
            //reach (Unit target in targetUnits)
            foreach(TerrainTile t in targetTiles)
            {
                Unit target = t.GetUnit();
                if (target == null) continue;
                Vector3 wpos = GetWorldCoords(target.tile.x, target.tile.y, Defines.def.miscGameSettings.zPosBuffs);
                GameObject go = Instantiate(buffTemplateSprite, wpos, Quaternion.identity, (target.mb as UnitMB).transform);

                go.name = spell.name + " buff";
                go.transform.localScale = new Vector3(tileScale, tileScale, 1);
                BuffMB buffmb = go.GetComponent<BuffMB>();
                if (spell.buffSprite) buffmb.sprite = spell.buffSprite;
                Animator aoc = go.GetComponent<Animator>();
                aoc.runtimeAnimatorController = spell.animatorOverrideController;

                buffmb.me.Init(buffmb, spell, duration, target, null, ls);
                newBuffs.Add(buffmb.me);
            }

        }
        else if (spell.buffType == BuffType.PerTile)
        {
            foreach (TerrainTile target in targetTiles)
            {
                //TerrainTile target = t.tile;
                Vector3 wpos = GetWorldCoords(target.x, target.y, Defines.def.miscGameSettings.zPosBuffs);
                GameObject go = Instantiate(buffTemplateSprite, wpos, Quaternion.identity, (target.mb as TerrainTileMB).transform);

                // same as above
                go.name = spell.name + " buff";
                go.transform.localScale = new Vector3(tileScale, tileScale, 1);
                BuffMB buffmb = go.GetComponent<BuffMB>();
                if (spell.buffSprite) buffmb.sprite = spell.buffSprite;
                Animator aoc = go.GetComponent<Animator>();
                aoc.runtimeAnimatorController = spell.animatorOverrideController;

                buffmb.me.Init(buffmb, spell, duration, null, target, ls);
                newBuffs.Add(buffmb.me);
            }
        }
        else
        {
            MyHelpers.LogError(this, "Casting spell error, neither tile nor unit");
            return null;
        }
        return newBuffs;
    }

    public bool processedBuffs = false;
    public bool processingBuffs = false;
    public IEnumerator ProcessBuffs()
    {
        processingBuffs = true;

        foreach (Buff b in ls.buffs)
        {
            b.InitApply();
            while (b.mb.IsPlaying()) yield return null;
        }

        for (int i = ls.buffs.Count - 1; i >= 0; i--)
        {
            Buff b = ls.buffs[i];
            if (b.age >= b.duration)
            {
                //b.mb.End(); // already played that anim
                b.End();
            }
        }

        processingBuffs = false;
        processedBuffs = true;
        yield break;
    }



    /// <summary>
    /// get coords of bottom left point of tile
    /// </summary>
    public Vector3 GetWorldCoords(int i, int j)
    {
        return startPos + new Vector3(i * tileLen, j * tileLen, 0);
    }
    /// <summary>
    /// get coordinates of point in world coordinates
    /// </summary>
    public Vector3 GetWorldCoords(Vector2 pos)
    {
        return startPos + new Vector3(pos.x * tileLen, pos.y * tileLen, 0);
    }
    /// <summary>
    /// get coordinates of point in world coordinates
    /// </summary>
    public Vector3 GetWorldCoords(Vector2Int pos)
    {
        return startPos + new Vector3(pos.x * tileLen, pos.y * tileLen, 0);
    }
    /// <summary>
    /// get coords of bottom left point of tile, keep z coord
    /// </summary>
    public Vector3 GetWorldCoords(int i, int j, float z)
    {
        return startPos + new Vector3(i * tileLen, j * tileLen, z);
    }
    /// <summary>
    /// get coordinates of point in world coordinates, keep z coord
    /// </summary>
    public Vector3 GetWorldCoords(Vector2 pos, float z)
    {
        return startPos + new Vector3(pos.x * tileLen, pos.y * tileLen, z);
    }
    /// <summary>
    /// get coordinates of point in world coordinates, keep z coord
    /// </summary>
    public Vector3 GetWorldCoords(Vector2Int pos, float z)
    {
        return startPos + new Vector3(pos.x * tileLen, pos.y * tileLen, z);
    }



    // Update is called once per frame
    void Update()
    {
        while (ls.ProceedWithTurn())
        {

        }
    }

    private LevelState lsCopy;
    public void TestCopyState()
    {
        lsCopy = new LevelState(ls);
    }
    public void TestAdvanceGame()
    {
        if (lsCopy == null) return;
        lsCopy.ProceedWithTurn();
    }
        

}
