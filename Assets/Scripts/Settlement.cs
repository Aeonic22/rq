﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SettlementType { Village, Town, Castle }

[System.Serializable]
public class Settlement {

    public ISettlementMB mb;

    [System.NonSerialized] private TerrainTile _tile;
    public TerrainTile tile { get { return _tile; } set { _tile = value; } }

    [Header("Folks")]
    [HideInInspector] public string name;
    public SettlementType settlementType;
    private Player _alignment;
    public Player alignment
    {
        get { return _alignment; }
        set
        {
            _alignment = value;
            mb.SetAlignment(_alignment);
            //ls.UpdateLeveleAlignment(); // updated begin turn
        }
    }
    public Player lastPlayerToTakeFood;
    public int countRecruitableFolks;
    public int countNonRecruitableFolks;
    public int countFolks { get { return countNonRecruitableFolks + countRecruitableFolks; } }
    // recruit and return folks

    [Header("Food")]
    public int food;
    public float foodProdPerInhabitant;
    public int foodProducedLastTurn;
    public int foodConsumedLastTurn;
    public int foodDeltaLestTurn { get { return foodProducedLastTurn - foodConsumedLastTurn; } }
    public float starving;

    // items, tavern

    // cached
    private static GameMain gm;
    private LevelState ls;

    public void Init(ISettlementMB mb, string name, GameMain gm, LevelState ls, Player alignment, TerrainTile tile)
    {
        MyHelpers.Log(this, "Init unit " + name + " with tile " + tile.ToString());
        this.mb = mb;
        this.name = name;
        Settlement.gm = gm;
        this.ls = ls;
        this.alignment = alignment;
        ls.settlements.Add(this);
        this.tile = tile;
        tile.SetSettlement(this);
        lastPlayerToTakeFood = ls.GetPlayerByName(Defines.neutralPlayerName);
    }

    /// <summary>
    /// They first comsume food, then produce so it can be allowed to starve them.
    /// </summary>
    public void BeginTurn()
    {
        //eat
        foodConsumedLastTurn = Mathf.Min(countFolks, food);
        food -= foodConsumedLastTurn;

        starving = 0;
        if (foodConsumedLastTurn < countFolks) starving = 1 - foodConsumedLastTurn / countFolks;
        if (starving > 0.01)
        {
            //TODO lose alignment toward neutral, then other side from that that took your food last turn
            if (alignment != ls.GetNeutralPlayer()) {
                if (lastPlayerToTakeFood == alignment) alignment = ls.GetNeutralPlayer();
            }
            else
            {
                alignment = ls.GetOtherPlayer(lastPlayerToTakeFood);
            }
        }

        // produce
        foodProducedLastTurn = (int) foodProdPerInhabitant * countFolks;
        food += foodProducedLastTurn;

        MyHelpers.Log(this, "Settlement " + name + " consumed / produced food: " + foodConsumedLastTurn + "/" + foodProducedLastTurn);
    }

    /// <summary>
    /// Unit can take some or all food from the village.
    /// </summary>
    /// <param name="u">Unit that takes food</param>
    /// <param name="amountFood">How much food is required</param>
    /// <returns>How much food is taken</returns>
    public int GetFood(Unit u, int amountFood)
    {
        if (food == 0) return 0;
        lastPlayerToTakeFood = u.player;
        // taking all food from village
        if (amountFood > food)
        {
            MyHelpers.Log(this, u.name + " takes ALL " + food + " food from " + name);
            int ret = food;
            food = 0;
            return ret;
        }
        // taking some food from village
        MyHelpers.Log(this, u.name + " takes " + amountFood + " food from " + name);
        food -= amountFood;
        return amountFood;
    }

    public float GetManaGainMultiplier()
    {
        //return TerrainTile.gainedManaModifierPerSettType[(int)settlementType];
        return Defines.def.gameplaySettings.settlementSpecs[(int)settlementType].manaMultiplier;
    }

    public static implicit operator bool(Settlement s)
    {
        if (s != null) return true;
        return false;
    }

    public override string ToString()
    {
        return "S " + name;
    }

    public string GetInfoText()
    {
        string s = "<color=" + alignment.mb.GetPlayerColorAsHex() + ">" + name
            + "\n(" + alignment.playerName + ")</color>\n"
            + countFolks + " inhabitants\n" + food + " food\n"
            + foodConsumedLastTurn + "/"+ foodProducedLastTurn + " consumed/produced last turn\n"
            + (foodConsumedLastTurn < countFolks ?
                "<color=red>STARVING " + (int)(starving * 100) + "%\n</color>"
                :
                "")
                + "Mana gain modifier: " + GetManaGainMultiplier()*100 + "%";
        return s;
    }


}
