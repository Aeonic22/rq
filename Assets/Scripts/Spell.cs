﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

/// <summary>
/// Can provide valid targets in range and determine all targets that spell will hit and at what mana cost. 
/// (Can determine strength on every tile - NO, all tiles the same) and total cost of spell.
/// </summary>
[Serializable]
public class Spell
{
    [NonSerialized] public Unit caster;
    [NonSerialized] public SpellDefinition spellDef;
    [NonSerialized] public int duration;

    [NonSerialized] public TerrainTile selectedTile;
    /// <summary>
    /// Calculated once per spell and duration selected
    /// </summary>
    //[NonSerialized] public List<SpellTargetInfo> validSti;
    /// <summary>
    /// Calculated once per spell and duration selected
    /// </summary>
    [NonSerialized] public List<TerrainTile> validTiles;

    [NonSerialized] public LevelState ls;

    /// <summary>
    /// Each time used selects spell and duration
    /// </summary>
    public void Init(Unit caster, SpellDefinition spellDef, int duration)
    {
//        validSti = null;
        validTiles = null;
        this.caster = caster;
        this.spellDef = spellDef;
        this.duration = duration;
        ls = caster.ls;
        GetValidTiles();
    }

    /// <summary>
    /// Returns all tiles that are in reach of magic, regardless whether we have enough mana to reach
    /// </summary>
    public List<TerrainTile> GetValidTiles()
    {
        if (validTiles != null) return validTiles;
        //if (validSti != null) return validSti;

        //validSti = new List<SpellTargetInfo>();
        validTiles = new List<TerrainTile>();

        if (spellDef.spread == SpellSpreadType.Self)
        {
            validTiles.Add(caster.tile);
        }
        else if (spellDef.spread == SpellSpreadType.Distance)
        {
            validTiles = caster.ls.tiles.GetTilesWithinDistance(caster.tile, ls.spellDev.maxHaarpRange, DistanceType.MovesDirect, true);
        }
        else if (spellDef.spread == SpellSpreadType.Ray)
        {
            foreach (NeighbourTile dir in System.Enum.GetValues(typeof(NeighbourTile)))
            {
                if (dir == NeighbourTile.Self) continue;
                else validTiles.AddRange(caster.ls.tiles.GetDiagonalTilesInDirection(caster.tile, dir, ls.spellDev.maxHaarpRange, false));
            }
        }
        else if (spellDef.spread == SpellSpreadType.Line)
        {
            foreach (NeighbourTile dir in System.Enum.GetValues(typeof(NeighbourTile)))
            {
                if (dir == NeighbourTile.Self) validTiles.Add(caster.tile);
                else validTiles.AddRange(caster.ls.tiles.GetDiagonalTilesInDirection(caster.tile, dir, ls.spellDev.maxHaarpRange, false));
            }
        }
        else if (spellDef.spread == SpellSpreadType.Wall)
        {
            foreach (NeighbourTile dir in System.Enum.GetValues(typeof(NeighbourTile)))
            {
                if (dir == NeighbourTile.Self) continue;
                else validTiles.AddRange(caster.ls.tiles.GetDiagonalTilesInDirection(caster.tile, dir, ls.spellDev.maxHaarpRange, false));
            }
        }
        // calc cost
        int baseCost = spellDef.manaCost * duration;
        foreach (TerrainTile t in validTiles)
        {
            int dist = (int)caster.ls.tiles.GetTileDistanceMovesDirect(caster.tile, t);
            t.tempManaCost = baseCost + dist;

        }
            //foreach (TerrainTile t in validTiles)
            //
            //    validSti.Add(new SpellTargetInfo { tile = t, manaCost = spellDef.manaCost * duration + (int) (ls.tiles.GetTileDistanceMovesDirect(caster.tile, t)*2) });
            //}
            return validTiles;
    }
    /// <summary>
    /// In range is a subgroup of valid tiles that we have enough mana for
    /// </summary>
    public List<TerrainTile> GetInRangeTiles()
    {
        int maxRange = caster.mana - spellDef.manaCost * duration;
        return validTiles.Where(x => ls.tiles.GetTileDistanceMovesDirect(caster.tile, x) <= maxRange).ToList();
    }
    public int GetManaCost(SpellTargetTile stt)
    {
        return (int)(duration * spellDef.manaCost + ls.tiles.GetTileDistanceMovesDirect(stt.tile, caster.tile));
    }
    /// <summary>
    /// Subgroup of in range tiles, plus maybe some nearby tiles to the one selected.
    /// </summary>
    public List<TerrainTile> GetTargetedTiles(TerrainTile selectedTile)
    {
        this.selectedTile = selectedTile;
        List<TerrainTile> ret = new List<TerrainTile>();
        if (spellDef.spread == SpellSpreadType.Self || spellDef.spread == SpellSpreadType.Distance)
        {
            ret.Add(selectedTile);
        }
        else if (spellDef.spread == SpellSpreadType.Ray)
        {
            NeighbourTile dir = ls.tiles.GetDirection(caster.tile, selectedTile);
            dir = ls.tiles.GetOppositeDirection(dir);
            for (int i = 0; i < 3; i++)
            {
                ret.Add(selectedTile);
                selectedTile = ls.tiles.GetNeighbouringTile(selectedTile, dir);
                if (selectedTile == null) break;
            }
        }
        else if (spellDef.spread == SpellSpreadType.Wall)
        {
            NeighbourTile dir = ls.tiles.GetDirection(caster.tile, selectedTile);
            dir = ls.tiles.GetOppositeDirection(dir);
            for (int i = 0; i < ls.spellDev.maxHaarpWall; i++)
            {
                ret.Add(selectedTile);
                selectedTile = ls.tiles.GetNeighbouringTile(selectedTile, dir);
                if (selectedTile == null) break;
            }
        }
        return ret;
    }


}