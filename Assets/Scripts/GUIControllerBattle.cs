﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIControllerBattle : MonoBehaviour, IGUIControllerBattle {

    public static GUIControllerBattleDummy gUIControllerBattleDummy;

    // battle ui
    public GameObject battlePanel;
    public Text battleText;
    public Button battleButton;
    public Text battleButtonText;

    private GameMain gm;

    static GUIControllerBattle()
    {
        gUIControllerBattleDummy = new GUIControllerBattleDummy();
    }

    // Use this for initialization
    void Start () {
        battlePanel.SetActive(false);
        gm = FindObjectOfType<GameMain>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    BattlePhase battlePhase = BattlePhase.off;
    public enum BattlePhase { off, estimate, resolution, results };
    public void SignalProcessBattle(BattlePhase phase)
    {
        battlePhase = phase;
    }
    public void StartBattle(Battle battle)
    {
        StartCoroutine(ProcessBattleCR(battle));
    }
    public IEnumerator ProcessBattleCR(Battle battle)
    {
        yield return null; // let the caller of this finish his frame...

        gm.gui.BlockUI(true);

        // begin estimate phase
        MyHelpers.Log(this, "Battle begun at " + battle.toTile.ToString());
        battlePhase = BattlePhase.estimate;
        battle.DetermineResult();
        int win = battle.br.winner;

        battlePanel.SetActive(true);
        string desc;
            /* TEMP
@"Battle result estimate:

";
        if (battle.br.playerWins) desc += "You will win!";
        else desc += "Enemy will win!";
        battleText.text = desc;
        battleButton.onClick.RemoveAllListeners();
        battleButton.onClick.AddListener(delegate { SignalProcessBattle(BattlePhase.results); });
        battleButtonText.text = "Commence battle!";
        while (battlePhase == BattlePhase.estimate) // wait till user confirms
            yield return null;*/
        battlePhase = BattlePhase.results;

        // user confirmed estimate, show results
        MyHelpers.Log(this, "Battle results at " + battle.toTile.ToString());
        if (battle.singleLocalPlayer)
        {
            desc = "BATTLE RESULTS: " + (battle.WinnerIsPlayer() ? "YOU WIN!" : "YOU LOSE!"); 
        }
        else desc = "BATTLE RESULTS: " + (battle.br.winner > 0 ? "ATTACKER WINS!" :
            (battle.br.winner < 0 ? "DEFENDER WINS!" : "BOTH LOSE!"));

        string winnerStats = "";
        if (battle.br.winner != 0)
        {
            winnerStats = "\nLost stamina: " + battle.br.winnerTirednessDelta + " for a total of "
                + (battle.br.GetWinner().stamina - battle.br.winnerTirednessDelta).ToString();
            winnerStats += "\nGained morale: " + battle.br.winnerMoraleDelta + " for a total of "
                + (battle.br.GetWinner().morale + battle.br.winnerMoraleDelta).ToString();
        }

        Unit u = battle.br.GetWinner();
        if (u) desc += "\n    " + u.name + " of " + u.player.playerName + " wins!\n";
        desc += "\nAttacker's stats (" + battle.attacker.name + ")";
        desc += "\nCount: " + battle.attacker.count;
        desc += "\nStamina: " + battle.attacker.stamina;
        desc += "\nTOTAL STRENGTH: " + battle.attackerStrength;
        desc += "\nTOTAL CASUALTIES: " + battle.br.attackerCasualties + " of " + battle.attacker.count;
        if (battle.br.winner > 0) desc += winnerStats;

        desc += "\n\nDefender's stats (" + battle.defender.name + ")";
        desc += "\nCount: " + battle.defender.count;
        desc += "\nStamina: " + battle.defender.stamina;
        desc += "\nTOTAL STRENGTH: " + battle.defenderStrength;
        desc += "\nTOTAL CASUALTIES: " + battle.br.defenderCasualties + " of " + battle.defender.count;
        if (battle.br.winner < 0) desc += winnerStats;

        battleText.text = desc;
        battleButton.onClick.RemoveAllListeners();
        battleButton.onClick.AddListener(delegate { SignalProcessBattle(BattlePhase.off); });
        battleButtonText.text = "Close";
        while (battlePhase == BattlePhase.results) // wait till user confirms
            yield return null;

        // close dialog, process results
        battlePanel.SetActive(false);
        gm.gui.BlockUI(false);

        battle.Cleanup();
    }

}

public interface IGUIControllerBattle
{
    void StartBattle(Battle battle);
}

/// <summary>
/// Used in AI and misc.
/// </summary>
public class GUIControllerBattleDummy : IGUIControllerBattle
{
    public void StartBattle(Battle battle)
    {
        battle.attacker.player.BattleSpell(battle);
        battle.defender.player.BattleSpell(battle);
        battle.DetermineResult();
        battle.Cleanup();
    }
}