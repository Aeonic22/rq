﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpellSpreadType {
    /// <summary>
    /// Spell targets only own tile
    /// </summary>
    Self,
    /// <summary>
    /// Spell targets any tile or unit within distance
    /// </summary>
    Distance,
    /// <summary>
    /// Spell is cast from own tile into straight line of some duration
    /// </summary>
    Ray,
    /// <summary>
    /// Spell is cast in unlimited line thru own tile
    /// </summary>
    Line,
    /// <summary>
    /// Or line segment or certain duration and distance from own tile. Can be centered on own tile too.
    /// </summary>
    Wall } // SingleTile, Ray, Line, CenteredLineSegment, Adjacent
public enum SpellId { Invigorate, Dampen, Harmonize, Disturb, InvigoratingWind, HarmonizingWind, EnhancePlantMetabolism }
public enum SpellStackType {
    /// <summary>
    /// Add all buffs together and add to default value. Opposites nullify. Used for addition type buffs.
    /// </summary>
    Add,
    /// <summary>
    /// Get average value of all buff effects and multiply default value with that. Suitable for mutliply type effects.
    /// </summary>
    Average }

[CreateAssetMenu(fileName = "SpellDefinition", menuName = "ScriptableObjects/SpellDefinition", order = 1)]
public class SpellDefinition : ScriptableObject {

    [Header("Text")]
    public SpellId spellId;
    public string spellName; // Invigorate
    [TextArea(1, 2)]
    public string spellPurpose; // "Invigorates and heals target unit";
    [TextArea(1,5)]
    public string spellDescription; // Will heal 50% and tire 50% less this turn. 


    [Header("General parameters")]
    public BuffType buffType;
    public BuffTrigger buffTrigger;
    public SpellSpreadType spread;

    [Header("Effects")]
    // duration is set at cast time

    // Invigorate
    [Tooltip("In percent of wounded that are healed each turn, -1 for ignore")]
    public int altHealingRate = -1;
    [Tooltip("Multiplier of total tiredness, from 0-2, -1 for ignore.")]
    public float tirednessMultiplier = -1;
    [Tooltip("Multiplier of tiredness added this turn, from 0-2, -1 for ignore.")]
    public float tirednessDeltaMultiplier = -1;

    // Harmonize
    [Tooltip("Morale added/removed from unit each turn if morale < 100, 0 for ignore")]
    public int moraleBoost = 0;
    [Tooltip("Morale added/removed from unit each turn if morale >= 100, 0 for ignore")]
    public int moraleBoostWhenOver100 = 0;

    [Header("Negative Effects for directed spells")]
    // Invigorate Negative 
    [Tooltip("In percent of wounded that are healed each turn, -1 for ignore")]
    public int altHealingRateNeg = -1;
    [Tooltip("Multiplier of total tiredness, from 0-2, -1 for ignore.")]
    public float tirednessMultiplierNeg = -1;
    [Tooltip("Multiplier of tiredness added this turn, from 0-2, -1 for ignore.")]
    public float trednessDeltaMultiplierNeg = -1;

    // Harmonize negative
    [Tooltip("Morale added/removed from unit each turn if morale < 100, 0 for ignore")]
    public int moraleBoostNeg = 0;
    [Tooltip("Morale added/removed from unit each turn if morale >= 100, 0 for ignore")]
    public int moraleBoostWhenOver100Neg = 0;

    // Directed spells use heal setting for alignment

    [Header("Resources")]
    [Tooltip("Per tile or unit, for level 1, for duration 1")]
    public int manaCost;

    [Header("Display")]
    public Sprite buffSprite;
    public AnimatorOverrideController animatorOverrideController;
    public BuffAnimType animType;

    [Tooltip("Animation played when unit moves across the wind, positive effect")]
    public Animation applyWallEffectPositive;
    [Tooltip("Animation played when unit moves across the wind, negative effect")]
    public Animation applWallEffectNegative;
    //public Image icon;

    public string GetDissipateTooltip(Buff b)
    {
        return "\nWill dissipate after " + ((b.duration - b.age) > 1 ? b.duration - b.age + " turns." : " this turn."); 
    }

    /// <summary>
    /// Tooltip for specific buff cast on scene
    /// </summary>
    public string GetBuffTooltip(Buff b)
    {
        string ret = "";
        switch (b.spell.spellId)
        {
            case SpellId.Invigorate:
                ret = spellPurpose;
                ret += "\n" + spellDescription;
                ret += "\n" + GetDissipateTooltip(b);
                break;
        }
        return ret;
    }

    //TODO descriptions and tooltips for every spell type and level selected
}

