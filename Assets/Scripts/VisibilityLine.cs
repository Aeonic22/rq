﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to be replaced by tilemap covering the area of any shape, capable of showing visibility for SINGLE friendly unit
/// Currently implemented with one line renderer per tile seen.
/// </summary>
public class VisibilityLine : MonoBehaviour {

    // state
    //bool needsUpdate = true;
    public Vector2Int coords = new Vector2Int(0, 0); // tile coords
    public Vector2 pos = new Vector2(0f,0f);    // position of lower left corner of self tile
    public float tileSize = 1;
    private bool[] visibility;  // for each neighbour and self, is army visible there

    // objects
    public LineRenderer lineRenderer;   // template for one tile
    private List<LineRenderer> renderers;   // one for each neighbour
    private GameMain gameMain;

    // misc
    bool inited = false;

	// Use this for initialization
	void Awake () {
        Init();
    }

    // move template to 0,0, create rectangle on it, make instances for neighbours, disable template
    // creation must follow enum Neighbour
    public void Init()
    {
        if (inited) return;
        MyHelpers.Log(this, "VisibilityLine init " + gameObject.name);
        inited = true;
        gameMain = FindObjectOfType<GameMain>();
        float len = gameMain.tileLen;
        float z = transform.position.z;

        visibility = new bool[9];
        renderers = new List<LineRenderer>();

        // instantiate template, link all to parent 
        LineRenderer lr = Instantiate(lineRenderer, new Vector3(0, 0, z), Quaternion.identity, transform);
        lr.positionCount = 5;
        lr.SetPosition(0, new Vector3(0f, 0f, 0));
        lr.SetPosition(1, new Vector3(len, 0f, 0));
        lr.SetPosition(2, new Vector3(len, len, 0));
        lr.SetPosition(3, new Vector3(0f, len, 0));
        lr.SetPosition(4, new Vector3(0f, 0f, 0));
        lr.name = "LineRenderer  Self";
        renderers.Add(lr);
        // left
        lr = Instantiate(lr, new Vector3(-len, 0, z), Quaternion.identity, transform);
        lr.name = "LineRenderer  Left";
        renderers.Add(lr);
        // right
        lr = Instantiate(lr, new Vector3(len, 0, z), Quaternion.identity, transform);
        lr.name = "LineRenderer  Right";
        renderers.Add(lr);
        // top 
        lr = Instantiate(lr, new Vector3(0, len, z), Quaternion.identity, transform);
        lr.name = "LineRenderer  Top";
        renderers.Add(lr);
        // bottom
        lr = Instantiate(lr, new Vector3(0, -len, z), Quaternion.identity, transform);
        lr.name = "LineRenderer  Bottom";
        renderers.Add(lr);
        // top left
        lr = Instantiate(lr, new Vector3(-len, len, z), Quaternion.identity, transform);
        lr.name = "LineRenderer  TopLeft";
        renderers.Add(lr);
        // top right
        lr = Instantiate(lr, new Vector3(len, len, z), Quaternion.identity, transform);
        lr.name = "LineRenderer  TopRight";
        renderers.Add(lr);
        // bottom left
        lr = Instantiate(lr, new Vector3(-len, -len, z), Quaternion.identity, transform);
        lr.name = "LineRenderer  BottomLeft";
        renderers.Add(lr);
        // bottom right
        lr = Instantiate(lr, new Vector3(len, -len, z), Quaternion.identity, transform);
        lr.name = "LineRenderer  BottomRight";
        renderers.Add(lr);
    }

    // just add all neighbours that you want visible to params
    public void SetVisibilityAll(Vector2Int selfPos)
    {
        List<NeighbourTile> ntl = new List<NeighbourTile>();
        ntl.Add(NeighbourTile.Self);
        ntl.Add(NeighbourTile.Top);
        ntl.Add(NeighbourTile.Bottom);
        ntl.Add(NeighbourTile.Left);
        ntl.Add(NeighbourTile.Right);
        ntl.Add(NeighbourTile.TopLeft);
        ntl.Add(NeighbourTile.BottomLeft);
        ntl.Add(NeighbourTile.TopRight);
        ntl.Add(NeighbourTile.BottomRight);
        SetVisibility(selfPos, ntl);
    }

    // just add all neighbours that you want visible to params
    public void SetVisibility(Vector2Int selfPos, List<NeighbourTile> visibleNeighbours)
    {
        if (!inited) Init();
        foreach (NeighbourTile nt in System.Enum.GetValues(typeof (NeighbourTile)))
        {
            visibility[(int)nt] = false;
        }

        foreach (NeighbourTile n in visibleNeighbours)
        {
            visibility[(int)n] = true;
        }

        for (int i = 0; i < 9; i++)
        {
            renderers[i].enabled = visibility[i];
        }

        // move me and children in wrld space
        coords = selfPos;
        Vector3 newPos = gameMain.GetWorldCoords(coords.x, coords.y, transform.position.z);
        transform.position = newPos;
    }
	
	// Update is called once per frame
	void Update () {

    }

    private void OnDestroy()
    {
        if (renderers != null)
        foreach (LineRenderer lr in renderers)
        {
            Destroy(lr);
        }
    }
}
