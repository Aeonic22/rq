﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Has name, side, type and AI (optional).
/// Can process turn.
/// Local player processes turn by doing nothing, just waiting, UI actions are handled elsewhere.
/// </summary>
public class PlayerMB : MonoBehaviour, IPlayerMB
{
    public static PlayerMBDummy playerMBDummy;

    public Player me;

    public Color playerColor;
    public Color GetPlayerColor() { return playerColor; }
    public string GetPlayerColorAsHex() { return "#" + ColorUtility.ToHtmlStringRGBA(GetPlayerColor()); }

    public Sprite playerFlag;

    // cached
    GameMain gm;

    static PlayerMB()
    {
        playerMBDummy = new PlayerMBDummy();
    }

    /// <summary>
    /// Objects created at runtime so all references are ok.
    /// </summary>
    void Awake()
    {
    }
    void Start()
    {
        gm = FindObjectOfType<GameMain>();
        PlayerAI playerAI = GetComponent<PlayerAI>();
        me.Init(this, playerAI, gm.ls);
    }

    // Update is called once per frame
    void Update()
    {

    }


    public Sprite GetFlag()
    {
        return playerFlag;
    }

}

public interface IPlayerMB {
    string GetPlayerColorAsHex();
    Color GetPlayerColor();
    Sprite GetFlag();
}
public class PlayerMBDummy : IPlayerMB {
    public string GetPlayerColorAsHex() { return ""; }
    public Color GetPlayerColor() { return new Color(); }
    public Sprite GetFlag() { return null; }
}

