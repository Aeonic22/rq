﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

[System.Serializable]
public class SpellIcon
{
    public SpellId spell;
    public GameObject icon;
}

/// <summary>
/// For displaying what tiles can be targeted and at what energy cost
/// </summary>
/*public class SpellTargetInfo
{
    public TerrainTile tile;
    public int manaCost;
}*/

public class SpellMB : MonoBehaviour
{

    public Spell me;

    /// <summary>
    /// Global list of spells in game. Order not relevant here.
    /// </summary>
    public List<SpellDefinition> spells;
    /// <summary>
    /// Mapping of spells to icons in the UI, also order not relevant
    /// </summary>
    public List<SpellIcon> spellIcons;

    [HideInInspector] public GameObject tilesFolder;
    [HideInInspector] public GameObject textFolder;
    [HideInInspector] public List<SpellTargetTile> tiles;
    private int xTiles, yTiles;
    public static GameMain gm;

    public GameObject template;
    public GameObject textTemplate;

    public SpellTargetTile selectedTile;
    [HideInInspector] List<TerrainTile> targetedTiles;





    //public int duration = 1;

    public GameObject spellDurationPanel;
    public bool isOverSpellCard;

    /// <summary>
    /// Creates tiles used for display of targeting in a folder "Runtime objects/Targeting"
    /// </summary>
    void Start()
    {
        gm = FindObjectOfType<GameMain>();
        xTiles = gm.xTiles;
        yTiles = gm.yTiles;
        tiles = new List<SpellTargetTile>(xTiles * yTiles);
        tilesFolder = GameObject.Find(Defines.targetingFolderName);
        textFolder = GameObject.Find(Defines.targetingTextFolderName);
        me = null;
        spellDurationPanel = GameObject.Find(Defines.spellParametersPanelName);
        spellDurationPanel.SetActive(false);

        var camera = FindObjectOfType<Camera>();

        SpellTargetTile.spell = this;
        for (int i = 0; i < xTiles; i++)
        {
            for (int j = 0; j < yTiles; j++)
            {
                // tile
                Vector3 pos = gm.GetWorldCoords(i, j, Defines.def.miscGameSettings.zPosTargeting);
                //GameObject go = Instantiate(tileTemplateSprite, pos, Quaternion.identity);
                GameObject go = Instantiate(template, pos, Quaternion.identity, tilesFolder.transform);
                go.name = "TargetingTile " + i + ", " + j;
                go.transform.localScale = new Vector3(gm.tileScale, gm.tileScale, 1);
                SpellTargetTile stt = go.GetComponent<SpellTargetTile>();
                //stt.x = i;
                //stt.y = j;
                stt.tile = gm.ls.tiles[i, j];
                stt.Init();
                tiles.Add(stt);

                // text
                Vector3 screenpos = camera.WorldToScreenPoint(pos + new Vector3(0.5f,0.25f,0)); // put it on center down part of the tile
                go = Instantiate(textTemplate, screenpos, Quaternion.identity, textFolder.transform);
                go.name = "TargetingTileText " + i + ", " + j;
                stt.costText = go;
            }
        }
        DeactivateAllTiles();

    }

    /// <summary>
    /// Call anytime you begin targeting with a new spell or unit
    /// </summary>
    public void BeginTargeting(Unit caster, SpellDefinition spellDef, int duration)
    {
        me = new Spell();
        me.Init(caster, spellDef, duration);

        gm.TileEntered += OnTileEnter;
        gm.TileExited += OnTileExit;
        gm.TileClicked += OnTileClick;
    }

    public void EndTargeting()
    {
        DeactivateAllTiles();
        gm.TileEntered -= OnTileEnter;
        gm.TileExited -= OnTileExit;
        gm.TileClicked -= OnTileClick;
        me = null;
    }

    public SpellTargetTile this[int i, int j]
    {
        get
        {
            return tiles[i * yTiles + j];
        }
        set
        {
            tiles[i * yTiles + j] = value;
        }
    }

    public void DeactivateAllTiles()
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            tiles[i].gameObject.SetActive(false);
            tiles[i].costText.SetActive(false);
        }
    }

    /// <summary>
    /// To ba called after all tiles have been deactivated
    /// </summary>
    public void DisplayValidTiles()
    {
        foreach (TerrainTile t in me.GetValidTiles())
        {
            SpellTargetTile tile = this[t.x, t.y];
            tile.gameObject.SetActive(true);
            tile.SetValid();

            tile.costText.SetActive(true);
            tile.costText.GetComponent<Text>().text = t.tempManaCost.ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool IsCasting()
    {
        return me != null;
    }

    /// <summary>
    /// If valid tile, it becomes targeted. Also other tiles are marked targeted if AOE.
    /// </summary>
    public void OnTileEnter(TerrainTile tile)
    {
        SpellTargetTile stt = this[tile.x, tile.y];
        if (stt.valid)
        {
            stt.SetSelected();
            selectedTile = stt;
            targetedTiles = me.GetTargetedTiles(tile);
            if (targetedTiles.Count > 0)
            {
                foreach (TerrainTile t in targetedTiles)
                {
                    this[t.x, t.y].SetTarget();
                }
            }
        }

    }

    /// <summary>
    /// All possible targeted tiles and selected tiles reset to default state, just showing if valid targets
    /// </summary>
    public void OnTileExit(TerrainTile tile)
    {
        SpellTargetTile stt = this[tile.x, tile.y];
        stt.ResetToValid();
        selectedTile = null;
        if (targetedTiles != null && targetedTiles.Count > 0)
        {
            foreach (TerrainTile t in targetedTiles)
            {
                this[t.x, t.y].ResetToValid();
            }
        }
    }

    /// <summary>
    /// If tile valid target, cast spell
    /// </summary>
    public void OnTileClick(TerrainTile t)
    {
        SpellTargetTile stt = this[t.x, t.y];
        if (stt.valid)
        {
            gm.CastSpellUI();
            EndTargeting();
            MyHelpers.Log(this, "CLICK " + t.x + ", " + t.y);
        }
    }

    public TerrainTile GetSelecetedTile()
    {
        //return gm.ls.tiles[selectedTile.x, selectedTile.y];
        return selectedTile.tile;
    }

    public void SetHoverOverSpellCard(bool b)
    {
        isOverSpellCard = b;
    }

    public void HoverAwayFromSpellIcon()
    {
        if (isOverSpellCard) return;
        spellDurationPanel.SetActive(false);
    }

}





