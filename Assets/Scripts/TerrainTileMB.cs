﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TerrainTileMB : MonoBehaviour, ITerrainTileMB
{
    public static TerrainTileMBDummy terrainTileMBDummy;

    [Header("Core object")]
    public TerrainTile me;

    // cached
    //SpriteRenderer sr;
    private GameMain gm;
    //TileMapper tileMapper;
    Tilemap tileMapHatch;

    // misc
    bool needsUpdate = true;    // to set sprite initially

    static TerrainTileMB()
    {
        terrainTileMBDummy = new TerrainTileMBDummy();
    }

    /// <summary>
    /// Tiles are created at runtime after game and level are set up so Awake is ok to use for init.
    /// </summary>
    void Awake()
    {
        me = new TerrainTile();
        me.mb = this;

        //if (sr == null) sr = GetComponent<SpriteRenderer>();
        if (gm == null) gm = FindObjectOfType<GameMain>();
        me.ls = gm.ls;
        //if (tileMapper == null) tileMapper = FindObjectOfType<TileMapper>();
        if (tileMapHatch == null) tileMapHatch = GameObject.Find(Defines.tilemapHatchName).GetComponent<Tilemap>();
    }
    /// <summary>
    /// Use awake to set up units since they will be created at run time when all other objects are set up.
    /// Start will run after current objects Update has finished, and it's too late.
    /// </summary>
    void Start()
    {
    }

    public void NeedsUpdate()
    {
        needsUpdate = true;
    }

    #region unityIntegration

    /// <summary>
    /// Notify GameMain.
    /// </summary>
    private void OnMouseUp()
    {
        MyHelpers.Log(this, "OnMouseUp");
        if (gm.gui.IsPointerOverUIObject()) return;
        //MyHelpers.MyHelpers.Log(this, "clicked tile " + x + ", " + y);
        if (!gm.spellmb.IsCasting()) gm.ls.TileActionUI(me);
        gm.OnTileClick(me);
    }

    /// <summary>
    /// Set sprite.
    /// </summary>
    void Update()
    {
        if (!needsUpdate) return;
        /*needsUpdate = false;
        Color tint;
        //HI sr.sprite = tileMapper.GetSpriteForTile(type, revealMap[this], out tint);
        sr.sprite = tileMapper.GetSpriteForTile(me.type, out tint);
        sr.color = tint;
        //HI SetHatchOnVisibleTiles();*/
    }

    private void OnMouseEnter()
    {
        gm.gui.SetTileInfoText(me.GetInfoText());
        if (me.settlement) gm.gui.SetSettlementInfoText(me.settlement.GetInfoText());
        if (me.AnyLiveUnits()) gm.gui.SetUnitInfoText(me.GetUnit().GetInfoText());
        gm.OnTileEnter(me);
    }

    public void OnMouseExit()
    {
        gm.gui.SetTileInfoText(null);
        gm.gui.SetSettlementInfoText(null);
        gm.gui.SetUnitInfoText(null);
        gm.OnTileExit(me);
    }

    /// <summary>
    /// Don't cancel text if we hover over unit on same tile
    /// </summary>
    /*private void OnMouseExit()
    {
        gm.ClearTileInfoText();
    }*/

    #endregion


}


public interface ITerrainTileMB
{
    void NeedsUpdate();
}

public class TerrainTileMBDummy
{
    public void NeedsUpdate() { }
}
